'use strict';
// @flow
const angular = require('angular');

interface User {
  name: string;
  email: string;
  password: string;
}

export default class SignupController {
  user: User = {
    name: '',
    email: '',
    password: ''
  };
  errors = {};
  submitted = false;
  Auth;
  $location;
  $http;
  data = "";
  $scope;
  catpcha;
  toastr;
  Rkey;
  _id;
  /*@ngInject*/
  constructor(Auth, $location, $scope, Stripe, $http, toastr, vcRecaptchaService) {
    this.Auth = Auth;
    this.$location = $location;
    this.$http = $http;
    this.$scope = $scope;
    this.toastr = toastr;


    $http.get('/api/settings').then(response => {
      for(var a in response.data){
          if(response.data[a].name == "Recaptcha"){
            $scope.Rkey = (response.data[a].publicToken).toString();
            this._id = response.data[a]._id;
          };
    }

    });

  }

  register(form) {
    this.submitted = true;
    if(form.$valid) {
      return this.Auth.createUser({
        name: this.user.name,
        email: this.user.email,
        password: this.user.password,
        role: 'nad'
      })
      .then((data) => {
        // Account created, redirect to home

        this.$location.path('/confirm');              })
      .catch(err => {
        err = err.data;
        this.errors = {};
        // Update validity of form fields that match the mongoose errors
        angular.forEach(err.errors, (error, field) => {
          form[field].$setValidity('mongoose', false);
          this.errors[field] = error.message;
        });

      });
    }else{
    //  this.toastr.error('Something missing!', 'Error');
      this.$scope.error = "error-log"
    }
  }

  registerFree(form) {
    this.submitted = true;
    if(form.$valid) {
      return this.Auth.createUser({
        name: this.user.name,
        email: this.user.email,
        password: this.user.password,
        role: 'nad',
        freeCoupon: 'free'
      })
      .then((data) => {
        // Account created, redirect to home

        this.$location.path('/confirm');              })
      .catch(err => {
        err = err.data;
        this.errors = {};
        // Update validity of form fields that match the mongoose errors
        angular.forEach(err.errors, (error, field) => {
          form[field].$setValidity('mongoose', false);
          this.errors[field] = error.message;
        });

      });
    }else{
    //  this.toastr.error('Something missing!', 'Error');
      this.$scope.error = "error-log"
    }
  }


}
