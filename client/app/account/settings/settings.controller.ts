'use strict';
// @flow
interface User {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  name: string;
  lastname: string;
  email: string;
}

export default class SettingsController {
  user: User = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
    name: '',
    lastname: '',
    email: ''
  };
  errors = {other: undefined};
  message = '';
  submitted = false;
  Auth;
  usuario;
  avatar = [];
  $scope;
  $http;

  /*@ngInject*/
  constructor(Auth, $scope, $translate, appConfig, $http, Upload, $timeout, $rootScope, socket) {
    this.Auth = Auth;
    this.usuario = Auth.getCurrentUserSync();
    this.$scope = $scope;
    this.$http = $http;

    var lan = this.usuario.language;
    var id = "";

    $http.get('/api/users/me')
    .then(response => {
      if(response.data.img){
       $scope.picFile = true;
       $scope.profilPic = true;
     }
     this.avatar = [response.data];
     socket.syncUpdates('user', this.avatar);
    });

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.img)
         $scope.picFile = true;
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

    $scope.changeLanguage = function (langKey) {

      if(langKey === "es")
       $rootScope.spanish = true;
       else
       $rootScope.spanish = false;

       $translate.use(langKey);
       $http.get('/api/users/me')
       .then(response => {

          id = response.data._id;
            $http.post('/api/users/changelan' , {lan: langKey, _id: id})
            .then(response => {
              console.log(response.data)
            });
       });

     };

     $scope.submit = function() {
       if ($scope.form.file.$valid && $scope.file) {
         $scope.upload($scope.file);
       }
     };

     // upload on file select or drop
     $scope.upload = function (file) {
          var fd = new FormData();
        fd.append('file', file);
        fd.append('_id', Auth.getCurrentUserSync()._id);
          $http.post('/api/users/avatar' , fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
          .then(response => {
            this.avatar = [response.data];
          });
     };
  }

  changePassword(form) {
    this.submitted = true;

    if(form.$valid) {
      this.Auth.changePassword(this.user.oldPassword, this.user.newPassword, this.user.name, this.user.lastname, this.user.email)
        .then(() => {
          this.message = 'Password successfully changed.';
        })
        .catch(() => {
          form.password.$setValidity('mongoose', false);
          this.errors.other = 'Incorrect password';
          this.message = '';
        });
    }
  }

  update(street, city, state, pc, country, phone, business_name, tax_id){
    var _id =  this.usuario._id;

    this.$http.put('/api/users/billing' , {_id: _id, address: street, city: city, state: state, postal_code: pc, country: country, phone: phone, business_name: business_name, tax_id: tax_id })
    .then( response => {

    });
  }


  delete(){
    this.$http.post('/api/users/deleteavatar', {_id : this.Auth.getCurrentUserSync()._id})
    .then(response => {
        if(response.data.img == null){
          this.$scope.picFile = false;
           this.$scope.profilPic = false;
        }

    });
  }

}
