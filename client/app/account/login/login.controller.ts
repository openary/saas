'use strict';
// @flow
interface User {
  name: string;
  email: string;
  password: string;
}

export default class LoginController {
  user: User = {
    name: '',
    email: '',
    password: ''
  };
  errors = {login: undefined};
  submitted = false;
  Auth;
  $location;
  usuario;
  $http;
  $scope;
  $rootScope;
  /*@ngInject*/
  constructor(Auth, $location, $translate, $scope, $http, $rootScope) {
    this.Auth = Auth;
    this.$location = $location;
    this.usuario = Auth.getCurrentUserSync();
    this.$http = $http;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
  }


  login(form) {

    this.$rootScope = this.$rootScope.$new(true);
    this.$scope = this.$scope.$new(true);

    this.submitted = true;

    if (form.$valid) {
      this.Auth.login({
        email: this.user.email,
        password: this.user.password
      })
      .then((data) => {
        if(data.confirmedEmail || data.confirmedEmail == undefined)
        this.$location.path('/');
        else if(data.confirmedEmail == false)
        this.$location.path('/confirm');
      })
      .catch(err => {
        this.errors.login = err.message;
        this.$scope.error = "error-log"
      });
    }
  }
}
