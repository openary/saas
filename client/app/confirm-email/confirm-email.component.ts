'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './confirm-email.routes';

export class ConfirmEmailComponent {
  /*@ngInject*/

  constructor(Stripe, $http) {

  }

}

export default angular.module('saasApp.confirm-email', [ngRoute])
  .config(routes)
  .component('confirmEmail', {
    template: require('./confirm-email.html'),
    controller: ConfirmEmailComponent,
    controllerAs: 'confCtrl'
  })
  .name;
