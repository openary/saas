'use strict';

describe('Component: ConfirmEmailComponent', function() {
  // load the controller's module
  beforeEach(module('saasApp.confirm-email'));

  var ConfirmEmailComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    ConfirmEmailComponent = $componentController('confirm-email', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
