'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/confirm', {
      template: '<confirm-email></confirm-email>',
      authenticate: 'nad'
    });
}
