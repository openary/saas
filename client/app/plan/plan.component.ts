'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './plan.routes';

export class PlanComponent {
  /*@ngInject*/
  Plan;
  $http;
  $scope;

  constructor($http, $scope, $rootScope) {
    this.$http = $http;
    this.$scope = $scope;
    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });
  }

  $onInit(){
    this.$http.get('/api/plan/')
    .then( response => {
      if(response.data)
      this.$scope.data = response.data;
    });
  }

  addPlan(form){
    var plan = this.Plan;
    console.log(plan);
    this.$http.post('/api/plan', {plan})
    .then( response => {
      this.$scope.message = response.data;
    });
  }

}

export default angular.module('saasApp.plan', [ngRoute])
  .config(routes)
  .component('plan', {
    template: require('./plan.html'),
    controller: PlanComponent,
    controllerAs: 'planCtrl'
  })
  .name;
