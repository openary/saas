'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/support', {
      template: '<support></support>'
    });
}
