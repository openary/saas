'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './support.routes';

export class SupportComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('saasApp.support', [ngRoute])
  .config(routes)
  .component('support', {
    template: require('./support.html'),
    controller: SupportComponent,
    controllerAs: 'supportCtrl'
  })
  .name;
