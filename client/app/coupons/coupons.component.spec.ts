'use strict';

describe('Component: CouponsComponent', function() {
  // load the controller's module
  beforeEach(module('saasApp.coupons'));

  var CouponsComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    CouponsComponent = $componentController('coupons', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
