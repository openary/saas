'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/coupons', {
      template: '<coupons></coupons>',
      authenticate: 'admin',
      title: 'Coupons',
      titleS: 'Cupones'
    });
}
