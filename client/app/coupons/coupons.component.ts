'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './coupons.routes';

export class CouponsComponent {
  /*@ngInject*/
  Stripe;
  $scope;
  $http;
  coupon;
  data;

  constructor(Stripe, $scope, $http, $rootScope) {
      this.Stripe = Stripe;
      this.$scope = $scope;
      this.$http = $http;
      
      $scope.$on('$routeChangeSuccess', function (event, data) {
        $http.get('/api/users/me')
        .then(response => {
          if(response.data.language === "es")
           $rootScope.subTitle = data.titleS;
           else
            $rootScope.subTitle = data.title;
        });
       });
  }

    billing(){
        var self = this;
              this.Stripe.card.createToken({
                  number: this.$scope.card,
                  exp_month: this.$scope.mm,
                  exp_year: this.$scope.year,
                  cvc: this.$scope.cvc
                }, function(status, response) {

                       self.$http.post('/api/payments', {token: response.id})
                        .then(response => {
                          this.data = response.data;
                        });

                });
    }


    $onInit(){
      this.$http.get('/api/cupons').then(response => {
        this.data = response.data;
      });
    }

    create(form){
      var f = this.coupon;
      f.discount = f.porcent - f.discount;
      f.porcent = undefined;

      this.$http.post('/api/cupons', {f})
       .then(response => {
         this.data.push({name: f.name , code: f.code, discount: f.discount, duration: f.duration, expiration: f.expiration, limit: f.limit, creation: Date.now()});
       });
    }

}

export default angular.module('saasApp.coupons', [ngRoute])
  .config(routes)
  .component('coupons', {
    template: require('./coupons.html'),
    controller: CouponsComponent,
    controllerAs: 'couponsCtrl'
  })
  .name;
