'use strict';

describe('Component: BillingInfoComponent', function() {
  // load the controller's module
  beforeEach(module('saasApp.billing-info'));

  var BillingInfoComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    BillingInfoComponent = $componentController('billing-info', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
