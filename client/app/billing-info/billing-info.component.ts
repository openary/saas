'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './billing-info.routes';

export class BillingInfoComponent {
  /*@ngInject*/
  $http;
  Stripe;
  billing;
  Auth;
  usuario;
  data;
  $window;
  $location;

  constructor(Auth ,Stripe, $http, $window, $location) {
    this.Stripe = Stripe;
    this.$http = $http;
    this.Auth = Auth;
    this.$window = $window;
    this.$location = $location;
    this.usuario = Auth.getCurrentUserSync();
  }

    billingCard(form){

      var billing = this.billing;

      console.log(this.billing);
      this.Stripe.card.createToken({
          number: this.billing.card,
          exp_month: this.billing.mm,
          exp_year: this.billing.year,
          cvc: this.billing.cvc
        }, (status, response) => {
          // response.id is the card token.

                this.$http.post('/api/payments', {token: response.id, email: this.usuario.email})
                .then(response => {
                  if(response.data.role == "paid"){
                    this.$http.put('/api/users/update', {_id : this.usuario._id, billing: billing})
                    .then( response => {
                      console.log(response.data);
                    });
                  }
                  //  this.$location.path('/overview');
                });

       });

    }

    bitcoin(form){
        this.$http.post('/api/payments/bitcoin', {email: this.usuario.email})
            .then(response => {
             //  self.data = response.data;
             console.log(response.data);
        });
    }

    paypal(){
      this.$http.post('/api/payments/paypal', {email: this.usuario.email})
          .then(response => {
             this.data = response.data;
             if(this.data)
              this.$window.location.href = this.data.links[1].href;
      });
    }

}

export default angular.module('saasApp.billing-info', [ngRoute])
  .config(routes)
  .component('billingInfo', {
    template: require('./billing-info.html'),
    controller: BillingInfoComponent,
    controllerAs: 'billingInfoCtrl'
  })
  .name;
