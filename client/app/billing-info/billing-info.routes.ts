'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/billinginfo', {
      template: '<billing-info></billing-info>',
      authenticate: 'free'
    });
}
