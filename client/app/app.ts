'use strict';

const angular = require('angular');
const ngAnimate = require('angular-animate');
const ngCookies = require('angular-cookies');
const ngResource = require('angular-resource');
const ngSanitize = require('angular-sanitize');
const aria = require('angular-aria');
import 'angular-socket-io';
const ngRoute = require('angular-route');
import 'bootstrap';
import 'bootstrap-material-design';

const uiBootstrap = require('angular-ui-bootstrap');
// const ngMessages = require('angular-messages');
// import ngValidationMatch from 'angular-validation-match';


//import filters
import countFilter from './admin/countAccounts/countAccounts.filter'
import timeFilter from './admin/timeFilter/timeFilter.filter'
import ceroFilter from './filters/cero/cero.filter.ts';
import uknown from './filters/uknown/uknown.filter';
import breadcrum from './filters/breadcrum/breadcrum.filter';

import {routeConfig} from './app.config';

import _Auth from '../components/auth/auth.module';
import account from './account';
import admin from './admin';
import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import socket from '../components/socket/socket.service';
import reports from './reports/reports.component';
import data from './data/data.component';
import dashboard from './dashboard/dashboard.component';
import blog from './blog/blog.component';
import coupons from './coupons/coupons.component';
import confirmEmail from './confirm-email/confirm-email.component';
import calendar from './calendar/calendar.component';
import billinginfo from './billing-info/billing-info.component';
import accounts from './accounts/accounts.component';
import indexController from './index/index.controller';
import notifications from './notifications/notifications.component';
import plan from './plan/plan.component';
import setting from './setting/setting.component';

import './app.css';
const stripeAngular = require('angular-stripe');
const toastr = require('angular-toastr');
import 'trumbowyg';
const translate =  require('angular-translate');
import 'angular-recaptcha';
import 'angular-material-event-calendar';
import 'angular-material';
import 'ng-file-upload';

//directives
import searchFilter from './accounts/search/search.directive';

angular.module('saasApp', [
  ngCookies,
  ngResource,
  ngSanitize,
  ngAnimate,
  aria,

  'btford.socket-io',
  'ngMaterial',

  indexController,
  ngRoute,
  uiBootstrap,
  translate,
  _Auth,
  account,
  admin,
  navbar,
  footer,
  main,
  constants,
  socket,
  util,
  reports,
  dashboard,
  data,
  blog,
  coupons,
  confirmEmail,
  calendar,
  billinginfo,
  accounts,
  plan,
  setting,
  notifications,
  stripeAngular,
  toastr,

  //filters
  countFilter,
  timeFilter,
  ceroFilter,
  uknown,
  breadcrum,
  //directives
  searchFilter,

  'vcRecaptcha',
  'material.components.eventCalendar',
  'ngFileUpload'

])
  .factory('callHttp', function($http){
    return {
    settings:  function(vcRecaptchaServiceProvider){return $http.get('/api/settings');}
    }
  })
  .config(routeConfig)
  .run(function($rootScope, $location, Auth, $http) {
    'ngInject';


    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function(event, next) {
      Auth.isLoggedIn(function(loggedIn) {
        if(next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  });

angular
  .element(document)
  .ready(() => {
    angular.bootstrap(document, ['saasApp'], {
      strictDi: true
    });
  });
