'use strict';

describe('Component: SettingComponent', function() {
  // load the controller's module
  beforeEach(module('saasApp.setting'));

  var SettingComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    SettingComponent = $componentController('setting', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
