'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/settings', {
      template: '<setting></setting>',
      authenticate: 'admin',
      title: 'Settings',
      titleS: 'Configuraciones'
    });
}
