'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './setting.routes';

export class SettingComponent {
  /*@ngInject*/
  $http;
  $scope;
  tl = [];
  socket;

  constructor($http, $scope, socket, $rootScope) {
    this.$http = $http;
    this.$scope = $scope;
    this.socket = socket;

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

    $scope.upload = function (file, id) {
         var fd = new FormData();
       fd.append('file', file);
         //fd.append('_id', Auth.getCurrentUserSync()._id);
         $http.post('/api/tl/logo' , fd, {
           transformRequest: angular.identity,
           headers: {'Content-Type': undefined}
       })
         .then(response => {
           $scope.mensaje =  response.data;
           console.log(response.data)
         });
    };


  }

  $onInit(){
    this.$http.get('/api/tl')
    .then(response => {
      this.tl = response.data;
      this.socket.syncUpdates('tl', this.tl);
      this.$scope.message = response.data;

    });

  }

  addTitle(title){
    this.$http.post('/api/tl' , {name : title})
    .then( response => {
      this.$scope.message = response.data;
    });

  }

  modifyTitle(t, i){
    this.$http.patch('/api/tl/' + i , {name : t})
    .then( response => {
    });
  }

}

export default angular.module('saasApp.setting', [ngRoute])
  .config(routes)
  .component('setting', {
    template: require('./setting.html'),
    controller: SettingComponent,
    controllerAs: 'settingCtrl'
  })
  .name;
