'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './accounts.routes';

export class AccountsComponent {
  /*@ngInject*/
  $scope;
  Auth;
  $http;
  toastr;
  usuario;
  socket;
  user;
  $window;
  things = [];

  constructor($http, $scope, Auth, toastr, socket, $window, $routeParams, $rootScope) {
    this.$scope = $scope;
    this.$http = $http;
    this.Auth = Auth;
    this.toastr = toastr;
    this.usuario = Auth.getCurrentUserSync();
    this.socket = socket;
    this.$window= $window;

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

    if($routeParams.a == true){
          this.$http.post('/api/networks/adsensetoken', {token: $routeParams.code + "#"}).then(response => {
            this.$http.post('/api/users/addToken', {_id: this.Auth.getCurrentUserSync()._id, token: response.data.access_token }).then(response=>{
                console.log(response.data);
            });
          });
    }
    if($routeParams.p == true){
          this.$http.post('/api/networks/paypaltoken', {token: $routeParams.code}).then(response => {

          });
    }
  }

  $onInit(){
    this.usuario = this.Auth.getCurrentUserSync();
    this.$http.get('/api/users/me')
    .then(response => {
      this.things = response.data.networks;
      if(this.things.length == 0)
        this.$scope.networks = false;
      else
        this.$scope.networks = true;

        this.socket.syncUpdates('networks', this.things);
    });

  }


    paypal(username, pass, signature, tag ){
      this.$http.post('/api/users/networks', { _id: this.Auth.getCurrentUserSync()._id , name:"Paypal", auth: "Credentials" , APIKey: pass, NetworkID: username, signature: signature, tag: tag }).then(response=>{

        });
        this.$onInit();
        this.usuario.networks = 1;
    }


  addNetwork(key, id, tag){

    this.$scope.apiKey = "";
    this.$scope.netID = "";
    this.$scope.tagHO = "";
    this.$http.post('/api/users/networks', {APIKey: key, NetworkID: id, _id: this.Auth.getCurrentUserSync()._id , name:"hasOffers", tag: tag, auth: "Api Key & Network ID"})
    .then(response=>{
        if(response.data == "error")
        this.toastr.error("Network already added", "");
        else
        this.toastr.success("Network added", "");
    });

    this.$onInit();
    this.usuario.networks = 1;
  }

  addNetworkAmazon(key, id, tag){

    this.$http.post('/api/users/networks', {APIKey: key, NetworkID: id, _id: this.Auth.getCurrentUserSync()._id , name:"Amazon"})
    .then(response=>{
      if(response.data == "error")
      this.toastr.error("Network already added", "");
      else
      this.toastr.success("Network added", "");
          this.usuario.networks.push({ name:"Amazon" , "NetworkID": id, "APIKey": key, "_id": this.Auth.getCurrentUserSync()._id});
    });

    this.$onInit();
    this.usuario.networks = 1;
  }

  addNetworkAdsense(id, tag){

   this.$http.post('/api/users/networks', {APIKey: id, NetworkID: "", _id: this.Auth.getCurrentUserSync()._id , name:"Adsense", auth: "Account ID", tag: tag}).then(response=>{

          this.toastr.success("Network added", "");
          this.$http.post('/api/networks/adsense', {})
          .then(response=>{
              this.$window.location.href = response.data;
              //  this.usuario.networks.push({ name:"Adsense" , "NetworkID": "", "APIKey": id, "_id": this.Auth.getCurrentUserSync()._id});
          });
    });

    this.$onInit();
    this.usuario.networks = 1;

  }

  callAmazon(){



    var _id = this.usuario._id;

    this.$http.post('/api/networks/amazon', {_id: _id}).then(response=>{
          console.log(response.data);
    });

    this.$onInit();
    this.usuario.networks = 1;
  }

  addNetworkGetCake(key, id, website, tag){

    this.$scope.apiKey = "";
    this.$scope.netID = "";
    this.$scope.tagGetCake = "";
    this.$scope.website = "";
    var w = website.replace(/^(https|ftp|http):\/\//i , '');

    this.$http.post('/api/users/networks', {APIKey: key, NetworkID: id, _id: this.Auth.getCurrentUserSync()._id , name:"getCake", tag: tag, auth: "Api Key & Network ID", website: website})
    .then(response=>{
      if(response.data == "error")
      this.toastr.error("Network already added", "");
      else
      this.toastr.success("Network added", "");
    });

    this.$onInit();
    this.usuario.networks = 1;
  }


  delete(tag){
    this.$http.put('/api/users/network', {_id: this.Auth.getCurrentUserSync()._id, tag: tag}).then(response=>{
        this.toastr.success("Network deleted", "");
    });
    this.$onInit();


  }

  testpaypal(){
    this.$http.post('/api/networks/testpaypal', {}).then(response=>{
        this.toastr.success("Network added", "");
    });

  }

}

export default angular.module('saasApp.accounts', [ngRoute])
  .config(routes)
  .component('accounts', {
    template: require('./accounts.html'),
    controller: AccountsComponent,
    controllerAs: 'accountsCtrl'
  })
  .name;
