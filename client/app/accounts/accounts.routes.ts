'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/accounts', {
      template: '<accounts></accounts>',
      authenticate: 'paid',
      title: 'Accounts',
      titleS: 'Cuentas'
    });
}
