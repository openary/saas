'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './terms.routes';

export class TermsComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('saasApp.terms', [ngRoute])
  .config(routes)
  .component('terms', {
    template: require('./terms.html'),
    controller: TermsComponent,
    controllerAs: 'termsCtrl'
  })
  .name;
