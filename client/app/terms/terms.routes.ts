'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/terms', {
      template: '<terms></terms>'
    });
}
