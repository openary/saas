'use strict';

export default function routes($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/reports', {
      template: '<reports></reports>',
      authenticate: 'beta',
      authent: 'paid',
      title: 'Reports',
      titleS: 'Reportes'
    });
}
