'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');
const Chart = require('chart.js');
const moment = require('moment');

export default class ReportsRanges {


    constructor(){

    }

    newDate(days) {
       return moment().add(days, 'd');
    }

    today(datos, breakdown){

      var neg = 0;

      if(datos < 0){
        neg = datos;
        datos = null;
      }
      else
         neg = null;

              var config = {
                type: 'line',
                data: {
                  labels: [this.newDate(0)],
                  datasets: [{
                    label: 'Income',
                    borderColor: 'rgba(92,131,239,0.9)',
                    backgroundColor: 'rgba(92,131,239,0.2)',
                    data: [datos],
                  },
                  {
                    label: 'Expenses',
                    borderColor: 'rgba(234, 108, 86, 0.9)',
                    backgroundColor: 'rgba(234, 108, 86, 0.2)',
                    data: [neg],
                  }
                ]
                },
                options: {
                  legend: {
                          display: false,
                          position: 'bottom',
                          labels: {
                                pointStyle: true
                            }
                          },
                  scales: {
                    xAxes: [{
                        display : true,
                        position: 'bottom',
                        type: 'time',
                        time: {
                        format: "HH:mm",
                        unit: breakdown,
                        unitStepSize: 1,
                        displayFormats: {
                          'minute': 'HH:mm',
                          'hour': 'HH:mm',
                          'day' : 'll',

                          },
                        }
                      }],
                  },
            }
          };
        return config;
    }

    yesterday(datos, breakdown){

              var config = {
                type: 'line',
                data: {
                  labels: [this.newDate(0)],
                  datasets: [{
                    label: 'Income',
                    borderColor: 'rgba(92,131,239,0.9)',
                    backgroundColor: 'rgba(92,131,239,0.2)',
                    data: [datos],
                  }]
                },
                options: {
                  legend: {
                          display: false,
                          position: 'bottom',
                          labels: {
                                pointStyle: true
                            }
                          },
                  scales: {
                    xAxes: [{
                        display : true,
                        position: 'bottom',
                        type: 'time',
                        time: {
                        format: "HH:mm",
                        unit: breakdown,
                        unitStepSize: 1,
                        displayFormats: {
                          'minute': 'HH:mm',
                          'hour': 'HH:mm',
                          'day' : 'll'
                          },
                        }
                      }],
                  },
            }
          };
        return config;
    }

    sevenDays(datos, breakdown){

              var config = {
                type: 'line',
                data: {
                  labels: [],
                  datasets: [{
                    label: 'Income',
                    borderColor: 'rgba(92,131,239,0.9)',
                    backgroundColor: 'rgba(92,131,239,0.2)',
                    data: [{
                      x : this.newDate(-6),
                      y : datos[0]
                    }, {
                      x : this.newDate(-5),
                      y : datos[1]
                    }, {
                      x : this.newDate(-4),
                      y : datos[2]
                    }, {
                      x : this.newDate(-3),
                      y : datos[3]
                    }, {
                      x : this.newDate(-2),
                      y : datos[4]
                    }, {
                      x : this.newDate(-1),
                      y : datos[5]
                    }, {
                      x : this.newDate(0),
                      y : datos[6]
                    }],
                  }]
                },
                options: {
                  legend: {
                          display: false,
                          position: 'bottom',
                          labels: {
                                pointStyle: true
                            }
                          },
                  scales: {
                    xAxes: [{
                        display : true,
                        position: 'bottom',
                        type: 'time',
                        time: {
                        format: "HH:mm",
                        unit: breakdown,
                        unitStepSize: 1,
                        displayFormats: {
                          'minute': 'HH:mm',
                          'hour': 'HH:mm',
                          'day' : 'll',
                          'month': 'MMM YYYY',
                          },
                        }
                      }],
                  },
            }
          };
        return config;
    }

    thirty(datos, breakdown){

                var config = {
                  type: 'line',
                  data: {
                    labels: [this.newDate(0)],
                    datasets: [{
                      label: 'Income',
                      borderColor: 'rgba(92,131,239,0.9)',
                      backgroundColor: 'rgba(92,131,239,0.2)',
                      data: [{
                        x : this.newDate(-30),
                        y : datos[0]
                      }, {
                        x : this.newDate(-29),
                        y : datos[1]
                      }, {
                        x : this.newDate(-28),
                        y : datos[2]
                      }, {
                        x : this.newDate(-27),
                        y : datos[3]
                      }, {
                        x : this.newDate(-26),
                        y : datos[4]
                      }, {
                        x : this.newDate(-25),
                        y : datos[5]
                      }, {
                        x : this.newDate(-24),
                        y : datos[6]
                      },{
                        x : this.newDate(-23),
                        y : datos[7]
                      }, {
                        x : this.newDate(-22),
                        y : datos[8]
                      }, {
                        x : this.newDate(-21),
                        y : datos[9]
                      }, {
                        x : this.newDate(-20),
                        y : datos[10]
                      }, {
                        x : this.newDate(-19),
                        y : datos[11]
                      }, {
                        x : this.newDate(-18),
                        y : datos[12]
                      }, {
                        x : this.newDate(-17),
                        y : datos[13]
                      },{
                        x : this.newDate(-16),
                        y : datos[14]
                      }, {
                        x : this.newDate(-15),
                        y : datos[15]
                      }, {
                        x : this.newDate(-14),
                        y : datos[16]
                      }, {
                        x : this.newDate(-13),
                        y : datos[17]
                      }, {
                        x : this.newDate(-12),
                        y : datos[18]
                      }, {
                        x : this.newDate(-11),
                        y : datos[19]
                      }, {
                        x : this.newDate(-10),
                        y : datos[20]
                      },{
                        x : this.newDate(-9),
                        y : datos[21]
                      }, {
                        x : this.newDate(-8),
                        y : datos[22]
                      }, {
                        x : this.newDate(-7),
                        y : datos[23]
                      }, {
                        x : this.newDate(-6),
                        y : datos[24]
                      }, {
                        x : this.newDate(-5),
                        y : datos[25]
                      }, {
                        x : this.newDate(-4),
                        y : datos[26]
                      }, {
                        x : this.newDate(-3),
                        y : datos[27]
                      },{
                        x : this.newDate(-2),
                        y : datos[28]
                      }, {
                        x : this.newDate(-1),
                        y : datos[29]
                      }
                    ],
                    }]
                  },
                  options: {
                    legend: {
                            display: false,
                            position: 'bottom',
                            labels: {
                                  pointStyle: true
                              }
                            },
                    scales: {
                      xAxes: [{
                          display : true,
                          position: 'bottom',
                          type: 'time',
                          time: {
                          format: "HH:mm",
                          unit: breakdown,
                          unitStepSize: 1,
                          displayFormats: {
                            'minute': 'HH:mm',
                            'hour': 'HH:mm',
                            'day' : 'll'
                            },
                          }
                        }],
                    },
              }
            };
          return config;

    }

    custom(begin, end, datos){

                        var config = {
                          type: 'line',
                          data: {
                            labels: [this.newDate(-7), this.newDate(-6), this.newDate(-5), this.newDate(-4), this.newDate(-3), this.newDate(-2), this.newDate(-1)],
                            datasets: [{
                              label: 'Income',
                              borderColor: 'rgba(92,131,239,0.9)',
                              backgroundColor: 'rgba(92,131,239,0.2)',
                              data: [{x: begin.getTime() , y : datos}],
                            }]
                          },
                          options: {
                            legend: {
                                    display: false,
                                    position: 'bottom',
                                    labels: {
                                          pointStyle: true
                                      }
                                    },
                            scales: {
                              xAxes: [{
                                  display : true,
                                  position: 'bottom',
                                  type: 'time',
                                  time: {
                                  format: "HH:mm",
                                  unit: "month",
                                  unitStepSize: 1,
                                  displayFormats: {
                                    'minute': 'HH:mm',
                                    'hour': 'HH:mm',
                                    'day' : 'll'
                                    },
                                  }
                                }],
                            },
                      }
                    };
              return config;
    }


    //For ADSENSE ------------------------------------------


    sevenDaysAdsense(datos, breakdown){

              var config = {
                type: 'line',
                data: {
                  labels: [this.newDate(0)],
                  datasets: [{
                    label: 'Income',
                    borderColor: 'rgba(92,131,239,0.9)',
                    backgroundColor: 'rgba(92,131,239,0.2)',
                    data: [{
                      x : this.newDate(-7),
                      y : datos[0][1]
                    }, {
                      x : this.newDate(-6),
                      y : datos[1][1]
                    }, {
                      x : this.newDate(-5),
                      y : datos[2][1]
                    }, {
                      x : this.newDate(-4),
                      y : datos[3][1]
                    }, {
                      x : this.newDate(-3),
                      y : datos[4][1]
                    }, {
                      x : this.newDate(-2),
                      y : datos[5][1]
                    }, {
                      x : this.newDate(-1),
                      y : datos[6][1]
                    }],
                  }]
                },
                options: {
                  legend: {
                          display: false,
                          position: 'bottom',
                          labels: {
                                pointStyle: true
                            }
                          },
                  scales: {
                    xAxes: [{
                        display : true,
                        position: 'bottom',
                        type: 'time',
                        time: {
                        format: "HH:mm",
                        unit: breakdown,
                        unitStepSize: 1,
                        displayFormats: {
                          'minute': 'HH:mm',
                          'hour': 'HH:mm',
                          'day' : 'll'
                          },
                        }
                      }],
                  },
            }
          };
        return config;
    }

    thirtyAdsense(datos, breakdown){

                var config = {
                  type: 'line',
                  data: {
                    labels: [this.newDate(0)],
                    datasets: [{
                      label: 'Income',
                      borderColor: 'rgba(92,131,239,0.9)',
                      backgroundColor: 'rgba(92,131,239,0.2)',
                      data: [{
                        x : this.newDate(-27),
                        y : datos[1][1]
                      }, {
                        x : this.newDate(-26),
                        y : datos[2][1]
                      }, {
                        x : this.newDate(-25),
                        y : datos[3][1]
                      }, {
                        x : this.newDate(-24),
                        y : datos[4][1]
                      }, {
                        x : this.newDate(-22),
                        y : datos[6][1]
                      },{
                        x : this.newDate(-21),
                        y : datos[7][1]
                      }, {
                        x : this.newDate(-20),
                        y : datos[8][1]
                      }, {
                        x : this.newDate(-19),
                        y : datos[9][1]
                      }, {
                        x : this.newDate(-18),
                        y : datos[10][1]
                      }, {
                        x : this.newDate(-17),
                        y : datos[11][1]
                      }, {
                        x : this.newDate(-16),
                        y : datos[12][1]
                      }, {
                        x : this.newDate(-15),
                        y : datos[13][1]
                      },{
                        x : this.newDate(-14),
                        y : datos[14][1]
                      }, {
                        x : this.newDate(-13),
                        y : datos[15][1]
                      }, {
                        x : this.newDate(-12),
                        y : datos[16][1]
                      }, {
                        x : this.newDate(-11),
                        y : datos[17][1]
                      }, {
                        x : this.newDate(-10),
                        y : datos[18][1]
                      }, {
                        x : this.newDate(-9),
                        y : datos[19][1]
                      }, {
                        x : this.newDate(-8),
                        y : datos[20][1]
                      },{
                        x : this.newDate(-7),
                        y : datos[21][1]
                      }, {
                        x : this.newDate(-6),
                        y : datos[22][1]
                      }, {
                        x : this.newDate(-5),
                        y : datos[23][1]
                      }, {
                        x : this.newDate(-4),
                        y : datos[24][1]
                      }, {
                        x : this.newDate(-3),
                        y : datos[25][1]
                      }, {
                        x : this.newDate(-2),
                        y : datos[26][1]
                      }, {
                        x : this.newDate(-1),
                        y : datos[27][1]
                      },{
                        x : this.newDate(-0),
                        y : datos[28][1]
                      }, {
                        x : this.newDate(0),
                        y : datos[29]
                      }, {
                        x : this.newDate(0),
                        y : datos[30]
                      }
                    ],
                    }]
                  },
                  options: {
                    legend: {
                            display: false,
                            position: 'bottom',
                            labels: {
                                  pointStyle: true
                              }
                            },
                    scales: {
                      xAxes: [{
                          display : true,
                          position: 'bottom',
                          type: 'time',
                          time: {
                          format: "HH:mm",
                          unit: breakdown,
                          unitStepSize: 1,
                          displayFormats: {
                            'minute': 'HH:mm',
                            'hour': 'HH:mm',
                            'day' : 'll'
                            },
                          }
                        }],
                    },
              }
            };
          return config;

    }

}
