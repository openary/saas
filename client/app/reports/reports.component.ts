'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');
var Chart = require('chart.js');
var moment = require('moment');
import routes from './reports.routes';
import ReportsRanges from './ReportsRanges';
var rr = new ReportsRanges();

var convert = require('xml-to-json-promise');

/*
var _id = this.usuario._id;

this.$http.post('/api/networks/amazon', {_id: _id}).then(response=>{
      console.log(response.data);
});

*/
export class ReportsComponent {
  /*@ngInject*/
  Auth;
  usuario;
  $http;
  $scope;
  reports = {};
  tags = [];
  source = [];
  json = {};

  constructor($http, $scope, Auth, $timeout, $rootScope) {
    this.usuario = Auth.getCurrentUserSync();
    this.$http = $http;
    this.$scope = $scope;
    this.Auth = Auth;

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

    var a = "hasOffers";
    $scope.user = null;
    $scope.users = null;

    $scope._breakdown =  $scope._breakdown  || [
      { id: 1, name: 'Day' },
      { id: 2, name: 'Week' },
      { id: 3, name: 'Month' },

    ];
    $scope.time_frame =  $scope.time_frame  || [
      { id: 1, name: 'Today' },
      { id: 2, name: 'Yesterday' },
      { id: 3, name: 'Last 7 days' },
      { id: 4, name: 'Last 30 days'}

    ];

    $scope.type_report =  $scope.type_report  || [
      { id: 1, name: 'Days' },
      { id: 2, name: 'Country' },
      { id: 3, name: 'Websites' },
    ];

    $scope.tag = (source) => {
      this.tags = [];
      for(var x in this.usuario.networks){
        if(this.usuario.networks[x].name == source)
        this.tags.push(this.usuario.networks[x].tag);
      }
    }

    $http.get('/api/users/me')
    .then(response => {
      for(var i in response.data.networks){
       this.source.indexOf( response.data.networks[i].name ) === -1 ? this.source.push(response.data.networks[i].name) : void 0 ;
      }
    });


  /*  $http.post('/api/users/getToken', {_id: Auth.getCurrentUserSync()._id }).then(response=>{
        console.log(response.data);

        $http.get('https://www.googleapis.com/adsense/v1.4/reports?startDate=2016-12-01&endDate=2016-12-15&accountId=pub-1100469234563518&dimension=DOMAIN_NAME&metric=EARNINGS&access_token=' + response.data.token).then(response=>{
            $scope.adsense = response.data;
        });
    });*/


    interface ${
      modal():void;
    }

    $scope.customRange = () => {
      if($scope.frame == 'Custom')
      (<any>$('#datePicker')).modal('show');

    }
    $scope.phframe ="Time frame";

    $scope.custom = function(a , b){
      var d = a.getDate();
      var m = a.getMonth();
      var y = a.getFullYear();
      var d2 = b.getDate();
      var m2 = b.getMonth();
      var y2 = b.getFullYear();

      $scope.phframe = d+"/"+m+"/"+y+" - "+d2+"/"+m2+"/"+y2;
      $scope.begin = a;
      $scope.end = b;
    }



          var data = {
              type: 'line',
              data: {
                  datasets: [{
                      label: 'Income',
                      borderColor: 'rgba(92,131,239,0.9)',
                      backgroundColor: 'rgba(92,131,239,0.2)',
                      data: []
                  },
                  {
                      label: 'Expenses',
                      borderColor: 'rgba(207,107,88,0.9)',
                      backgroundColor: 'rgba(207,107,88,0.2)',
                      data: []
                  },
                  {
                      label: 'Profit',
                      borderColor: 'rgba(115,203,112,0.9)',
                      backgroundColor: 'rgba(115,203,112,0.2)',
                      data: []
                  }]
              },
              options: {

                legend: {
                            display: false,
                            position: 'bottom',
                            labels: {
                                pointStyle: true
                            }
                        },
                  scales: {
                      xAxes: [{
                          display : true,
                          position: 'bottom',
                          type: 'time',
                          time: {
                          format: "HH:mm",
                          unit: 'day',
                          unitStepSize: 1,
                          displayFormats: {
                            'minute': 'HH:mm',
                            'hour': 'HH:mm',
                            min: '2016-10-10',
                            max: '2016-10-20'
                            },
                          }
                        }],
                      yAxes: [{
                          display: true
                        }]
                  }
              }
          }

          setTimeout(()=>{
            var ctx = document.getElementById("chartReports");
            var chart = new Chart(ctx, data);
          }, 500);



  }

  $onInit(){
    this.$http.get('/api/users/me')
    .then(response =>{
      this.usuario = response.data;
      //this.socket.syncUpdates('notifications', this.things);
    });

  }


        newDate(days) {
           return moment().add(days, 'd');
        }

        xmltojson(xml){
            convert.xmlDataToJSON(xml).then(j => {
                this.json = j;
              });
              return this.json;
        }


          today(source, tag, breakdown){

            var today = new Date();
            var dd = ("0" + (today.getDate()).toString().slice(-2));
            var mm = ("0" + (today.getMonth()+1).toString().slice(-2));
            var yyyy = today.getFullYear().toString();
            var datos = 0;
            var todayString = yyyy + "-" + mm +'-'+ dd;
            var todayStringCake = mm + "/" + dd + "/" + yyyy;
            var apiKey = "";
            var netID = "";
            var todayPaypal = yyyy + "-" + mm + "-" + dd + 'T00:00:00.681Z';
            var todayPaypal2 = yyyy + "-" + mm + "-" + dd + 'T24:59:59.681Z';
            switch(source){
                case 'Paypal' :
              for(var a in this.usuario.networks){
                if(this.usuario.networks[a].tag == tag){
                  this.$http.post('/api/networks/paypal',{_id: this.usuario._id, tag: tag, date: todayPaypal, date2: todayPaypal2})
                    .then(response =>{
                      for(var i in response.data.objects){
                        datos += response.data.objects[i].NETAMT;
                      }
                      var ctx = document.getElementById("chartReports");
                      new Chart(ctx, rr.today(datos, breakdown));
                    });
                }
              }
              break;

              case 'Adsense' :
                    for(var a in this.usuario.networks){
                      if(this.usuario.networks[a].name == "Adsense"){
                          apiKey = this.usuario.networks[a].APIKey;
                          netID = this.usuario.networks[a].NetworkID;

                          this.$http.post('/api/users/getToken', {_id: this.Auth.getCurrentUserSync()._id }).then(response=>{
                               console.log(response.data);

                              this.$http.get('https://www.googleapis.com/adsense/v1.4/reports?startDate='+ todayString +'&endDate=' + todayString + '&accountId=' + apiKey + '&dimension=DOMAIN_NAME&metric=EARNINGS&access_token=' + response.data.token).then(response=>{
                                   this.$scope.adsense = response.data.totals[1];

                                   datos = response.data.totals[1];
                                   var ctx = document.getElementById("chartReports");
                                   new Chart(ctx, rr.today(datos, breakdown));
                             });
                        });
                    }
                  }
              break;

              case "hasOffers" :
                    for(var a in this.usuario.networks){
                      if(this.usuario.networks[a].name == "hasOffers"){
                          apiKey = this.usuario.networks[a].APIKey;
                          netID = this.usuario.networks[a].NetworkID;

                          this.$http.get('https://api.hasoffers.com/Apiv3/json?NetworkId=' + netID + '&Target=Affiliate_AffiliateBilling&Method=getPayoutTotals&api_key='+ apiKey +'&timeframes%5B0%5D%5Bstart_date%5D=' + todayString + '&timeframes%5B0%5D%5Bend_date%5D=' + todayString +'&timeframes%5B0%5D%5Blabel%5D=today')
                          .then(response => {
                            this.$scope.mensaje = response.data;
                            if(response.data)
                              datos = response.data.response.data.today.total_payout;
                          });

                          var ctx = document.getElementById("chartReports");
                          new Chart(ctx, rr.today(datos, breakdown));

                        }
                    }
              break;
              case "Amazon":
              break;
              case "getCake":

              for(var a in this.usuario.networks){
                if(this.usuario.networks[a].name == "getCake"){
                    apiKey = this.usuario.networks[a].APIKey;
                    netID = this.usuario.networks[a].NetworkID;
                    var website = this.usuario.networks[a].website;

                    this.$http.post('/api/networks/getCake' , {website: website, apiKey: apiKey, netID: netID, date: todayStringCake})
                      .then(response => {
                        var jsondata = {};

                        convert.xmlDataToJSON(response.data).then(j => {
                            this.json = j;
                            this.$scope.messaje = j;
                            datos = j.performance_summary_response.periods[0].period[0].current_revenue[0];
                            console.log(datos)
                            var ctx = document.getElementById("chartReports");
                            new Chart(ctx, rr.today(datos, breakdown));

                          });

                    });

                  };
              }

              break;
              default: break;
            }

          }

          yesterday(source, tag, breakdown){
                let yesterday = ( d => new Date(d.setDate(d.getDate()-1)) )(new Date);
                var dd = ("0" + (yesterday.getDate()).toString().slice(-2));
                var mm = ("0" + (yesterday.getMonth()+1).toString().slice(-2));
                var yyyy = yesterday.getFullYear().toString();
                var datos = 0;

                var todayStringCake = mm + "/" + dd + "/" + yyyy;
                var yesterdayString = yyyy + "-" + mm +'-'+ dd;
                var apiKey = "";
                var netID = "";
                var yesterdayPaypal = yyyy + "-" + mm + "-" + dd + 'T00:00:00.681Z';
                var yesterdayPaypal2 = yyyy + "-" + mm + "-" + dd + 'T24:59:59.681Z';

                switch(source){
                case 'Paypal' :
                  for(var a in this.usuario.networks){
                    if(this.usuario.networks[a].tag == tag){
                      this.$http.post('/api/networks/paypal',{_id: this.usuario._id, tag: tag, date: yesterdayPaypal, date2: yesterdayPaypal2})
                        .then(response =>{
                          for(var i in response.data.objects){
                            datos += response.data.objects[i].NETAMT;
                          }
                          var ctx = document.getElementById("chartReports");
                          new Chart(ctx, rr.yesterday(datos, breakdown));
                        });
                    }
                  }
                  break;

                  case "Adsense" :
                      for(var a in this.usuario.networks){
                        if(this.usuario.networks[a].name == "Adsense"){
                            apiKey = this.usuario.networks[a].APIKey;
                            netID = this.usuario.networks[a].NetworkID;

                            this.$http.post('/api/users/getToken', {_id: this.Auth.getCurrentUserSync()._id }).then(response=>{
                                 console.log(response.data);

                                this.$http.get('https://www.googleapis.com/adsense/v1.4/reports?startDate='+ yesterdayString +'&endDate=' + yesterdayString + '&accountId=' + apiKey + '&dimension=DATE&metric=EARNINGS&access_token=' + response.data.token).then(response=>{
                                     this.$scope.adsense = response.data.rows;
                                     datos = response.data.rows[0];
                                     var ctx = document.getElementById("chartReports");
                                     new Chart(ctx, rr.yesterday(datos, breakdown));
                               });

                          });
                      }
                    }
                  break;

                  case "hasOffers" :
                      for(var a in this.usuario.networks){
                        if(this.usuario.networks[a].name == "hasOffers"){
                            apiKey = this.usuario.networks[a].APIKey;
                            netID = this.usuario.networks[a].NetworkID;

                            this.$http.get('https://api.hasoffers.com/Apiv3/json?NetworkId=' + netID + '&Target=Affiliate_AffiliateBilling&Method=getPayoutTotals&api_key='+ apiKey +'&timeframes%5B0%5D%5Bstart_date%5D=' + yesterdayString + '&timeframes%5B0%5D%5Bend_date%5D=' + yesterdayString +'&timeframes%5B0%5D%5Blabel%5D=yesterday')
                            .then(response => {
                              this.$scope.mensaje = response.data;
                              console.log(response.data.response.data.yesterday.total_payout)
                              if(response.data)
                                datos = response.data.response.data.yesterday.total_payout;
                            });

                            var ctx = document.getElementById("chartReports");
                            new Chart(ctx, rr.yesterday(datos, breakdown));

                          }
                      }
                  break;
                  case "Amazon":
                  break;
                  case "getCake":

                  for(var a in this.usuario.networks){
                    if(this.usuario.networks[a].name == "getCake"){
                        apiKey = this.usuario.networks[a].APIKey;
                        netID = this.usuario.networks[a].NetworkID;
                        var website = this.usuario.networks[a].website;

                        this.$http.post('/api/networks/getCake' , {website: website, apiKey: apiKey, netID: netID, date: todayStringCake})
                          .then(response => {
                            var jsondata = {};

                            convert.xmlDataToJSON(response.data).then(j => {
                                this.json = j;
                                this.$scope.messaje = j;
                                datos = j.performance_summary_response.periods[0].period[0].current_revenue[0];
                                console.log(datos)
                                var ctx = document.getElementById("chartReports");
                                new Chart(ctx, rr.today(datos, breakdown));

                              });

                        });

                      };
                  }

                  break;
                  default: break;
                }



          }


          sevenDays(source, tag, breakdown){


            let today = ( d => new Date(d.setDate(d.getDate()-1)) )(new Date);
            var dd = ("0" + (today.getDate()).toString().slice(-2));
            var mm = ("0" + (today.getMonth()+1).toString().slice(-2));
            var yyyy = today.getFullYear().toString();
            var datos = [];
            var todayString = yyyy + "-" + mm +'-'+ dd;
            var todayStringCake = mm + "/" + dd + "/" + yyyy;
            var d = ("0" + (today.getDate()-1)).slice(-2);
            var yesterdayString = yyyy + "-" + mm +'-'+ d;
            var apiKey = "";
            var netID = "";


            var curr = new Date();
            var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
            var last = first - 6; // last day is the first day + 6
            var endDate = new Date(curr.setDate(last));
            var previousWeek = endDate.getFullYear() + "-" +  ("0" + (endDate.getMonth() + 1)).slice(-2) + "-" + ("0" + endDate.getDate()).slice(-2);
            var endPaypal = yyyy + "-" + mm + "-" + dd + 'T24:59:59.681Z';


            switch(source){

              case 'Paypal' :
                for(var a in this.usuario.networks){
                  if(this.usuario.networks[a].tag == tag){
                    for(var i = 1 ; i <= 6; i++){
                    let today = ( d => new Date(d.setDate(d.getDate()-i)) )(new Date);
                    var pri = today.getDate() - today.getDay();
                    var p = pri - 3; // last day is the first day + 6
                    var paypalDate = new Date(today.setDate(p));
                    var startPaypal = paypalDate.getFullYear().toString() + "-" + ("0" + (paypalDate.getMonth() + 1)).toString().slice(-2) + "-" + ("0" + paypalDate.getDate()).toString().slice(-2) + 'T00:00:00.681Z';

                    alert(startPaypal)
                    this.$http.post('/api/networks/paypal',{_id: this.usuario._id, tag: tag, date: startPaypal, date2: endPaypal})
                      .then(response =>{
                        for(var i in response.data.objects){
                          datos += response.data.objects[i].NETAMT;
                        }
                        var ctx = document.getElementById("chartReports");
                        new Chart(ctx, rr.yesterday(datos, breakdown));
                      });

                    }

                  }
                }
                break;

              case "Adsense" :
                    for(var a in this.usuario.networks){
                      if(this.usuario.networks[a].name == "Adsense"){
                          apiKey = this.usuario.networks[a].APIKey;
                          netID = this.usuario.networks[a].NetworkID;

                          this.$http.post('/api/users/getToken', {_id: this.Auth.getCurrentUserSync()._id }).then(response=>{
                               console.log(response.data);


                                  this.$http.get('https://www.googleapis.com/adsense/v1.4/reports?startDate=' + previousWeek + '&endDate=' + todayString + '&accountId=' + apiKey + '&dimension=DATE&metric=EARNINGS&access_token=' + response.data.token).then(response=>{
                                        datos.push(response.data.rows);
                                       var ctx = document.getElementById("chartReports");
                                       new Chart(ctx, rr.sevenDaysAdsense(datos[0], breakdown));
                                 });
                          });
                    }
                  }
              break;

              case "hasOffers" :
                      for(var a in this.usuario.networks){
                          if(this.usuario.networks[a].name == "hasOffers"){
                              apiKey = this.usuario.networks[a].APIKey;
                              netID = this.usuario.networks[a].NetworkID;

                          var i = 0;
                          var limit = 6;

                           var f = () =>{
                             if(limit < 0){
                               var ctx = document.getElementById("chartReports");
                               new Chart(ctx, rr.sevenDays(datos, breakdown));
                               return;
                             }


                            var endDate = new Date(today.setDate(limit));
                            var d2 = ("0" + endDate.getDate()).slice(-2);
                            var changeDay = yyyy + "-" + ("0" + endDate.getMonth()+1).slice(-2) +'-'+ d2;

                            this.$http.get('https://api.hasoffers.com/Apiv3/json?NetworkId=' + netID + '&Target=Affiliate_AffiliateBilling&Method=getPayoutTotals&api_key='+ apiKey +'&timeframes%5B0%5D%5Bstart_date%5D=' + changeDay + '&timeframes%5B0%5D%5Bend_date%5D=' + changeDay +'&timeframes%5B0%5D%5Blabel%5D=sday')
                            .then(response => {

                              datos.push(response.data.response.data.sday.total_payout);


                            });

                            if(i <= limit){
                              setTimeout(f  , 500);
                            }


                              limit--;
                          }
                          //execute function
                          f();

                        }
                    }
              break;
              case "Amazon":
              break;
              case "getCake":

              for(var a in this.usuario.networks){
                if(this.usuario.networks[a].name == "getCake"){
                    apiKey = this.usuario.networks[a].APIKey;
                    netID = this.usuario.networks[a].NetworkID;
                    var website = this.usuario.networks[a].website;

                    for(var i = 6; i >= 0; i--){
                        this.$http.post('/api/networks/getCake' , {website: website, apiKey: apiKey, netID: netID, date: todayStringCake})
                          .then(response => {
                            var jsondata = {};

                            convert.xmlDataToJSON(response.data).then(j => {

                                datos.push(j.performance_summary_response.periods[0].period[0].current_revenue[0]);
                                if(i = 6){
                                  var ctx = document.getElementById("chartReports");
                                  new Chart(ctx, rr.sevenDays(datos, breakdown));
                                }

                              });

                        });
                      }
                  };
              }

              break;
              default: break;
            }

          }

          thirty(source, tag, breakdown){

                var today = new Date();
                var dd = ("0" + today.getDate()).slice(-2);
                var mm = ("0" + today.getMonth()+1).slice(-2);
                var yyyy = today.getFullYear();
                var datos = [];
                var todayString = yyyy + "-" + mm +'-'+ dd;
                var todayStringCake = mm + "/" + dd + "/" + yyyy;
                var pm = new Date(today.setMonth(today.getMonth()-1));
                var previousMonth = ("0" + (pm.getMonth()+1)).slice(-2);
                var previousDate = pm.getFullYear() + "-" + previousMonth +'-'+ dd;
              var apiKey = "";
              var netID = "";

              switch(source){
                case "Adsense" :
                      for(var a in this.usuario.networks){
                        if(this.usuario.networks[a].name == "Adsense"){
                            apiKey = this.usuario.networks[a].APIKey;
                            netID = this.usuario.networks[a].NetworkID;

                            this.$http.post('/api/users/getToken', {_id: this.Auth.getCurrentUserSync()._id }).then(response=>{
                                 console.log(response.data);

                                    this.$http.get('https://www.googleapis.com/adsense/v1.4/reports?startDate=' + previousDate + '&endDate=' + todayString + '&accountId=' + apiKey + '&dimension=DATE&metric=EARNINGS&access_token=' + response.data.token).then(response=>{

                                        datos.push(response.data.rows);
                                         var ctx = document.getElementById("chartReports");
                                         new Chart(ctx, rr.thirtyAdsense(datos[0] , breakdown));
                                   });

                            });
                      }
                    }
                break;

                case "hasOffers" :
                var d = ("0" + (today.getDate()-1)).slice(-2);
                var yesterdayString = yyyy + "-" + mm +'-'+ d;

                        for(var a in this.usuario.networks){
                            if(this.usuario.networks[a].name == "hasOffers"){
                                apiKey = this.usuario.networks[a].APIKey;
                                netID = this.usuario.networks[a].NetworkID;

                                var i = 0;
                                var limit = 30;

                                 var f = () =>{
                                   if(limit < 0){
                                     var ctx = document.getElementById("chartReports");
                                     new Chart(ctx, rr.thirty(datos, breakdown));
                                     return;
                                   }


                                  var endDate = new Date(today.setDate(limit));
                                  var d2 = ("0" + endDate.getDate()).slice(-2);
                                  var changeDay = yyyy + "-" + ("0" + endDate.getMonth()+1).slice(-2) +'-'+ d2;

                                  this.$http.get('https://api.hasoffers.com/Apiv3/json?NetworkId=' + netID + '&Target=Affiliate_AffiliateBilling&Method=getPayoutTotals&api_key='+ apiKey +'&timeframes%5B0%5D%5Bstart_date%5D=' + changeDay + '&timeframes%5B0%5D%5Bend_date%5D=' + changeDay +'&timeframes%5B0%5D%5Blabel%5D=sday')
                                  .then(response => {

                                    datos.push(response.data.response.data.sday.total_payout);


                                  });

                                  if(i <= limit){
                                    setTimeout(f  , 400);
                                  }


                                    limit--;
                                }
                                //execute function
                                f();

                          }
                      }
                break;

                case "Amazon":
                break;
                case "getCake":

                for(var a in this.usuario.networks){
                  if(this.usuario.networks[a].name == "getCake"){
                      apiKey = this.usuario.networks[a].APIKey;
                      netID = this.usuario.networks[a].NetworkID;
                      var website = this.usuario.networks[a].website;



                      for(var i = 30; i >= 0; i--){
                        var endDate = new Date(today.setDate(i));
                        var d2 = ("0" + endDate.getDate()).slice(-2);
                        var changeDay =  ("0" + endDate.getMonth()+1).slice(-2) + "/" + d2 +'/' + yyyy;

                          this.$http.post('/api/networks/getCake' , {website: website, apiKey: apiKey, netID: netID, date: changeDay})
                            .then(response => {
                              var jsondata = {};
                              convert.xmlDataToJSON(response.data).then(j => {

                                  datos.push(j.performance_summary_response.periods[0].period[0].current_revenue[0]);
                                  if(i=1){
                                    var ctx = document.getElementById("chartReports");
                                    new Chart(ctx, rr.thirty(datos, breakdown));
                                  }


                                });

                          });
                        }
                    };
                }

                break;
                default: break;
              }



          }

      filter(source, tag, type, frame, breakdown){
//console.log(source, tag, type, frame, breakdown)
  /*          var dia =  ('0' + this.$scope.begin.getDate()).slice(-2);
            var mes =  ('0'+ (this.$scope.begin.getMonth()+1)).slice(-2);
            var anio = this.$scope.begin.getFullYear();
            var dia2 =  ('0' + this.$scope.end.getDate()).slice(-2);
            var mes2 =  ('0'+ (this.$scope.end.getMonth()+1)).slice(-2);
            var anio2 = this.$scope.end.getFullYear();
            var fechaInicial = anio + "-" +  mes + "-" + dia;
            var fechaFinal = anio2 + "-" +  mes2 + "-" + dia2;
                var datos = "";
*/

                switch(frame.name){
                    case "Today": this.today(source, tag, breakdown.name);
                    break;
                    case "Yesterday": this.yesterday(source, tag, breakdown.name);
                    break;
                    case "Last 7 days": this.sevenDays(source, tag, breakdown.name);
                    break;
                    case "Last 30 days": this.thirty(source, tag, breakdown.name);
                    break
                }

        /*        this.$http.get('https://api.hasoffers.com/Apiv3/json?NetworkId=factorads&Target=Affiliate_AffiliateBilling&Method=getPayoutTotals&api_key=43af466cfd33fee5c6c01f1ee07ad3f87df201cb69b659179c80a2133dd263f6&timeframes%5B0%5D%5Bstart_date%5D=' + fechaInicial + '&timeframes%5B0%5D%5Bend_date%5D=' + fechaFinal +'&timeframes%5B0%5D%5Blabel%5D=sdays')
                .then(response => {
                  this.$scope.mensaje = response.data;
                  if(response.data)
                    datos = response.data.response.data.sdays.total_payout;
                });
                          var ctx = document.getElementById("chartReports");
                          new Chart(ctx, rr.custom(this.$scope.begin, this.$scope.end, datos));
*/
      }


  cancel(){
    var config = {
      type: 'line',
      data: {
        labels: [this.newDate(-7), this.newDate(-6), this.newDate(-5), this.newDate(-4), this.newDate(-3), this.newDate(-2), this.newDate(-1)],
        datasets: [{
          label: 'Income',
          borderColor: 'rgba(92,131,239,0.9)',
          backgroundColor: 'rgba(92,131,239,0.2)',
          data: [],
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
                display: false,
                position: 'bottom',
                labels: {
                      pointStyle: true
                  }
                },
        scales: {
          xAxes: [{
              display : true,
              position: 'bottom',
              type: 'time',
              time: {
              format: "HH:mm",
              unit: 'day',
              unitStepSize: 1,
              displayFormats: {
                'minute': 'HH:mm',
                'hour': 'HH:mm',
                'day' : 'll'
                },
              }
            }],
        },
  }
};
    var ctx = document.getElementById("chartReports");
    new Chart(ctx, config);
  }

}

export default angular.module('saasApp.reports', [ngRoute])
  .config(routes)
  .component('reports', {
    template: require('./reports.html'),
    controller: ReportsComponent,
    controllerAs: 'reportsCtrl'
  })
  .name;
