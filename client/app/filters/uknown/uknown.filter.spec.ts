'use strict';

describe('Filter: uknown', function() {
  // load the filter's module
  beforeEach(module('saasApp.uknown'));

  // initialize a new instance of the filter before each test
  var uknown;
  beforeEach(inject(function($filter) {
    uknown = $filter('uknown');
  }));

  it('should return the input prefixed with "uknown filter:"', function() {
    var text = 'angularjs';
    expect(uknown(text)).to.equal('uknown filter: ' + text);
  });
});
