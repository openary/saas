'use strict';
const angular = require('angular');

/*@ngInject*/
export function uknownFilter() {
  return function(input) {
    if(input == undefined || input == null || input == "")
      input = "Unknown";
    return input;
  };
}


export default angular.module('saasApp.uknown', [])
  .filter('uknown', uknownFilter)
  .name;
