'use strict';
const angular = require('angular');

/*@ngInject*/
export function ceroFilter() {
  return function(input) {
    if(input == null || input == undefined || input == '')
    return `0`;
  };
}


export default angular.module('saasApp.cero', [])
  .filter('cero', ceroFilter)
  .name;
