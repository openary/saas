'use strict';

describe('Filter: cero', function() {
  // load the filter's module
  beforeEach(module('saasApp.cero'));

  // initialize a new instance of the filter before each test
  var cero;
  beforeEach(inject(function($filter) {
    cero = $filter('cero');
  }));

  it('should return the input prefixed with "cero filter:"', function() {
    var text = 'angularjs';
    expect(cero(text)).to.equal('cero filter: ' + text);
  });
});
