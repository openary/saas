'use strict';

describe('Filter: breadcrum', function() {
  // load the filter's module
  beforeEach(module('saasApp.breadcrum'));

  // initialize a new instance of the filter before each test
  var breadcrum;
  beforeEach(inject(function($filter) {
    breadcrum = $filter('breadcrum');
  }));

  it('should return the input prefixed with "breadcrum filter:"', function() {
    var text = 'angularjs';
    expect(breadcrum(text)).to.equal('breadcrum filter: ' + text);
  });
});
