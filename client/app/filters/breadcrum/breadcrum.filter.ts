'use strict';
const angular = require('angular');

/*@ngInject*/
export function breadcrumFilter() {
  return function(input, filter, bol) {
  
    if(filter.spanish){
    if(input == "Data")
      return "Datos";
    else if(input == "Users")
      return "Usuarios";
    else if(input == "Notifications")
      return "Notificaciones";
    else if(input == "Coupons")
      return "Cupones";
    else if(input == "Settings")
      return "Configuraciones";
    else if(input == "Dashboard")
      return "General";
    else if(input == "Reports")
      return "Reportes";
    else if(input == "Accounts")
      return "Cuentas";
    else if(input == "Calendar")
      return "Calendario";
    else if(input == "Profile")
      return "Perfil";
    } else {
      return input;
    }
  };
}


export default angular.module('saasApp.breadcrum', [])
  .filter('breadcrum', breadcrumFilter)
  .name;
