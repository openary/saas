'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/notifications', {
      template: '<notifications></notifications>',
      authenticate: 'admin',
      title: 'Notifications',
      titleS: 'Notificaciones'
    });
}
