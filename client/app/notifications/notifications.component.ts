'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './notifications.routes';

export class NotificationsComponent {
  /*@ngInject*/
  socket;
  $http;
  $scope;
  things = [];
  not;

  constructor(socket, $scope, $http) {
    this.socket = socket;
    this.$scope = $scope;
    this.$http = $http;

  }

  $onInit(){

    this.$http.get('/api/notifications')
    .then(response =>{
      this.things = response.data;
      this.socket.syncUpdates('notifications', this.things);
    });

  }

  create(form){
    var f = this.not;
    this.$http.post('/api/notifications' , {f})
    .then(response =>{
      //this.things = response.data;
    });
  }

}

export default angular.module('saasApp.notifications', [ngRoute])
  .config(routes)
  .component('notifications', {
    template: require('./notifications.html'),
    controller: NotificationsComponent,
    controllerAs: 'notificationsCtrl'
  })
  .name;
