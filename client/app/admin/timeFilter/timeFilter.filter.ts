'use strict';
const angular = require('angular');

/*@ngInject*/
export function timeFilterFilter() {
  return function(input) {
    var d, h, m, s;
    var timeArray = input.split('/')
    var time1 = Date.parse(timeArray[0]);
    var time2 = Date.parse(timeArray[1]);


    return Math.floor((time1 - time2) / (86400000) ) + " days";
  };
}

//Math.floor(Date.parse(input) / (1000*60*60*60*24)) + " days";


export default angular.module('saasApp.timeFilter', [])
  .filter('timeFilter', timeFilterFilter)
  .name;
