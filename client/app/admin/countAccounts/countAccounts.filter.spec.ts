'use strict';

describe('Filter: countAccounts', function() {
  // load the filter's module
  beforeEach(module('saasApp.countAccounts'));

  // initialize a new instance of the filter before each test
  var countAccounts;
  beforeEach(inject(function($filter) {
    countAccounts = $filter('countAccounts');
  }));

  it('should return the input prefixed with "countAccounts filter:"', function() {
    var text = 'angularjs';
    expect(countAccounts(text)).to.equal('countAccounts filter: ' + text);
  });
});
