'use strict';
const angular = require('angular');

/*@ngInject*/
export function countAccountsFilter() {
  return function(input) {
    return (input.length);
  };
}


export default angular.module('saasApp.countAccounts', [])
  .filter('countAccounts', countAccountsFilter)
  .name;
