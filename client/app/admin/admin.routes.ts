'use strict';

export default function routes($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/users', {
      template: require('./users.html'),
      controller: 'AdminController',
      controllerAs: 'admin',
      authenticate: 'admin',
      title: 'Users',
      titleS: 'Usuarios'
    })
    .when('/admin/settings', {
      template: require('./sections/settings.html'),
      controller: 'AdminController',
      controllerAs: 'admin',
      authenticate: 'admin',
      title: 'Settings',
      titleS: 'Configuraciones'
    })
    .when('/apis', {
      template: require('./sections/apikeys.html'),
      controller: 'AdminController',
      controllerAs: 'admin',
      authenticate: 'admin',
      title: 'Apis',
      titleS: 'Apis'
    });
};
