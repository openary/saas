'use strict';

export default class AdminController {
  users: Object[];
  $http;
  data = "";
  toastr;
  $scope;
  socket;
  publicToken;
  secretToken;
  _id;
  Auth;
  Rkey;
  client_id;
  secret;
  client_id_adsense;
  secret_adsense;
  key_id_amazon;
  secret_amazon;
  /*@ngInject*/
  constructor(Auth, User, $http, $scope, toastr, appConfig, $rootScope, socket) {
    // Use the User $resource to fetch all users
    this.users = User.query();
    this.$http = $http;
    this.toastr = toastr;
    this.Auth = Auth;
    this.$scope = $scope;
    this.socket = socket;

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

    $scope.sortReverse  = false;  // set the default sort order
    $scope.searchFish   = '';

    $scope.roles = appConfig.userRoles;
    $http.get('/api/users')
    .then((response) => {
      $scope.users = response.data;
    });

    $scope.changeRole = (role, users) => {
          angular.forEach(users, u => {
            if(u.role != 'admin')
              if(u.select == true)
                this.changeRole(role, u);
          });
    }

    $scope.selectAll = function(){
      if($scope.select){
        angular.forEach($scope.users, (user) => {
          user.select = false;
        });
      }
      else {
        angular.forEach($scope.users, (user) => {
          user.select = true;
        });
      }
    }

    this.$http.get('/api/settings')
    .then(response => {
        for(var a in response.data){
            if(response.data[a].name == "Stripe"){
              this.publicToken = response.data[a].publicToken;
              this.secretToken = response.data[a].secretToken;
              this._id = response.data[a]._id;
            };

            if(response.data[a].name == "Recaptcha"){
              this.Rkey = response.data[a].publicToken;
              this._id = response.data[a]._id;
            };

            if(response.data[a].name == "Paypal"){
              this.client_id = response.data[a].publicToken;
              this.secret = response.data[a].secretToken;
              this._id = response.data[a]._id;
            };

            if(response.data[a].name == "Adsense"){
              this.client_id_adsense = response.data[a].publicToken;
              this.secret_adsense = response.data[a].secretToken;
              this._id = response.data[a]._id;
            };

            if(response.data[a].name == "Amazon"){
              this.key_id_amazon = response.data[a].publicToken;
              this.secret_amazon = response.data[a].secretToken;
              this._id = response.data[a]._id;
            };
      }

    });


  }

  call(){
    this.$http.post('/api/status/call' , {})
    .then(response => {

    });
  }

  delete(user) {
    if(user.role != "admin"){
      this.$http.delete('/api/users/'+ user._id)
      .then(response => {
        this.users.splice(this.users.indexOf(user), 1);
        this.toastr.success('Deleted!', 'Success');
      });
   }
  }

  create(){
      this.$http.post('/api/settings', {name:"sergio", info:"admin", active:true})
      .then(response => {
        this.data = response.data;
      });
  }

  block(){

  }

  changeRole(role, user){
    this.Auth.changeRole(role, user._id)
      .then((response) => {

        this.data = 'Rol changed';
        this.toastr.success('Changed!', 'Success');
      })
      .catch(() => {
      //  this.errors.other = 'Incorrect role';
        this.data = '';
      });
  }

  addApiKeys(){

    this.$http.post('/api/settings/', {publicToken: this.publicToken, secretToken: this.secretToken, name: "Stripe"})
    .then(response => {
    });

    this.$http.post('/api/settings/', {publicToken: this.Rkey, name: "Recaptcha"})
    .then(response => {

    });

    this.$http.post('/api/settings/', {publicToken: this.client_id , secretToken: this.secret , name: "Paypal"})
    .then(response => {

    });

    this.$http.post('/api/settings/', {publicToken: this.client_id_adsense , secretToken: this.secret_adsense , name: "Adsense"})
    .then(response => {
    });

    this.$http.post('/api/settings/', {publicToken: this.key_id_amazon , secretToken: this.secret_amazon , name: "Amazon"})
    .then(response => {
    });

      this.toastr.success('Keys added!', 'Success');

  }

}
