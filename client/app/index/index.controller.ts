'use strict';
const angular = require('angular');

/*@ngInject*/
export function indexController($scope, $location, Auth, $http, socket) {
  $scope.$location = $location;
  $scope.isLoggedIn = Auth.isLoggedInSync;
  $scope.isAdmin = Auth.isAdminSync;
  $scope.getCurrentUser = Auth.getCurrentUserSync;
  $scope.usuario = Auth.getCurrentUserSync();

  var arr = [];

  $http.get('/api/tl')
  .then(response => {
    $scope.title = response.data[0].name;
    $scope.tl = response.data;
    socket.syncUpdates('tl', $scope.tl);
  });

}

export default angular.module('saasApp.index', [])
  .controller('IndexController', indexController)
  .name;
