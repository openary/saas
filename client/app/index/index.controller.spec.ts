'use strict';

describe('Controller: IndexCtrl', function() {
  // load the controller's module
  beforeEach(module('saasApp.index'));

  var IndexCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    IndexCtrl = $controller('IndexCtrl', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
