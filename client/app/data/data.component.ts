'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');
var Chart = require('chart.js');
import DataCharts from './dataCharts';
var dc = new DataCharts();

import routes from './data.routes';

export class DataComponent {
  /*@ngInject*/
  users: Object[];
  $http;
  $timeout;
  data = "";
  toastr;
  publicToken;
  secretToken;
  _id;
  Auth;
  Rkey;
  client_id;
  secret;
  breakdown;

  constructor(Auth, User, $http, $scope, toastr, appConfig, $timeout, $rootScope) {
    this.users = User.query();
    this.$http = $http;
    this.toastr = toastr;
    this.Auth = Auth;
    this.$timeout = $timeout;


    $scope.time =  $scope.time  || [
      { id: 1, name: 'Day' },
      { id: 2, name: 'Week' },
      { id: 3, name: 'Month' },
    ];

    $scope.timeOthers =  $scope.timeOthers  || [
    //  { id: 1, name: 'Day' },
      { id: 2, name: 'Last 7 days' },
      { id: 3, name: 'Month' },
    ];

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });



    $scope.userstats = time => {
      switch(time.name){
        case "Day":
        today( u => {
          var chartUserData = document.getElementById("myUserChart");
          var chartUser = new Chart(chartUserData, dc.today(u));
        });
        break;
        case "Week":
          week( u => {
            var chartUserData = document.getElementById("myUserChart");
            var chartUser = new Chart(chartUserData, dc.week(u));
          });
        break;
        case "Month":
          month( u => {
            var chartUserData = document.getElementById("myUserChart");
            var chartUser = new Chart(chartUserData, dc.month(u));
          });
        break;
      }
    }


    let today = (callback) => {
      $http.get('/api/users')
      .then(response => {
        var users = response.data;
        var dateUsers = [];
        var todayUsers = [];
        var todayTypeUsers = [];
        var now = new Date();
        var today = new Date(now);
        today.setDate(today.getDate());
        var hoy = ( today.getDate() + '-' + today.getMonth() + '-' + today.getFullYear() );
        //day by day var ['nad', 'free', 'beta', 'paid', 'admin']
        var dbd = new Date(now);
        var countUsers: number = 0;
        var activeUsers: number = 0;
        var freeUsers: number = 0;
        var betaUsers: number = 0;
        var paidUsers: number = 0;
        var finalUsers = [];
        $scope.freeTotal = 0;
        $scope.paidTotal = 0;
        $scope.usersTotal = 0;
        $scope.betaTotal = 0;
        $scope.activeUsers = 0;

          for(var i = 0; i < users.length; i++){
              dateUsers[i] = users[i].registered;
              dateUsers[i] = new Date(users[i].registered);
              dateUsers[i].toISOString();

              dateUsers[i] = ( dateUsers[i].getDate() + '-' + dateUsers[i].getMonth() + '-' + dateUsers[i].getFullYear() );

              if( hoy == dateUsers[i] ){
                 todayTypeUsers.push( users[i].role );
                 countUsers++;

                 $scope.usersTotal++;
                   if(todayTypeUsers[i] == 'free'){
                     freeUsers++;
                      $scope.freeTotal++;
                   }
                   if(todayTypeUsers[i] == 'beta'){
                     betaUsers++;
                      $scope.betaTotal++;
                   }
                   if(todayTypeUsers[i] == 'paid'){
                     paidUsers++;
                      $scope.paidTotal++;
                   }
               }

            }

            if(freeUsers == undefined)
               freeUsers = 0;
            if(betaUsers == undefined)
               betaUsers = 0;
            if(paidUsers == undefined)
               paidUsers = 0;
            if(countUsers == undefined)
               countUsers = 0;

            finalUsers.push({ total: countUsers, free: freeUsers, beta: betaUsers, paid: paidUsers});
            callback(finalUsers);
      });
    }
    //final today stats


      let week = (callback) => {
        $http.get('/api/users')
        .then(response => {
          var users = response.data;
          var dateUsers = [];
          var weekUsers = [];
          var weekTypeUsers = [];
          var now = new Date();

          var prevWeek = new Date(now);
          prevWeek.setDate(prevWeek.getDate() - 7);

            for(var i = 0; i < users.length; i++){
                dateUsers[i] = users[i].registered;
                dateUsers[i] = new Date(users[i].registered);
                dateUsers[i].toISOString();
                if(dateUsers[i] >= prevWeek && dateUsers[i] <= now){
                   weekUsers.push( dateUsers[i].getDate() + "-" + (dateUsers[i].getMonth()+1) + "-" + dateUsers[i].getFullYear() );
                   weekTypeUsers.push( users[i].role );
                 }
              }
              var a: number = 0;
              var b: number = 0;
              var c: number = 0;
              var d: number = 0;
              //day by day var ['nad', 'free', 'beta', 'paid', 'admin']
              var dbd = new Date(now);
              var countUsers = [];
              var activeUsers = [];
              var freeUsers = [];
              var betaUsers = [];
              var paidUsers = [];
              var finalUsers = [];
              $scope.freeTotal = 0;
              $scope.paidTotal = 0;
              $scope.usersTotal = 0;
              $scope.betaTotal = 0;
              $scope.activeUsers = 0;

              for(var i = 0; i < 7; i++){
              dbd = ( d => new Date(d.setDate(d.getDate()-i)) )(new Date);
              var fecha = dbd.getDate() + "-" + (dbd.getMonth()+1) + "-" + dbd.getFullYear();
              var dia = dbd.getDate();
                  for(var j = 0; j < weekUsers.length; j++){

                    if(weekUsers[j] == fecha){
                      d++;
                      countUsers[i] = d;
                      $scope.usersTotal++;

                        if(weekTypeUsers[j] == 'free'){
                          a++;
                          freeUsers[i] = a;
                          $scope.freeTotal++;
                        }
                        else if(weekTypeUsers[j] == 'beta'){
                          b++;
                          betaUsers[i] = b;
                          $scope.betaTotal++;
                        }
                        else if(weekTypeUsers[j] == 'paid'){
                          c++;
                          paidUsers[i] = c;
                          $scope.paidTotal++;
                        }
                      }


                    }

                    if(freeUsers[i] == undefined)
                       freeUsers[i] = 0;
                    if(betaUsers[i] == undefined)
                       betaUsers[i] = 0;
                    if(paidUsers[i] == undefined)
                       paidUsers[i] = 0;
                    if(countUsers[i] == undefined)
                       countUsers[i] = 0;

                finalUsers.push({ total: countUsers[i], free: freeUsers[i], beta: betaUsers[i], paid: paidUsers[i] , date: dia});
              }

              callback(finalUsers);
        });
      }
      //final week stats

      let month= (callback) => {
        $http.get('/api/users')
        .then(response => {
          var users = response.data;
          var dateUsers = [];
          var monthUsers = [];
          var monthTypeUsers = [];
          var now = new Date();

          var prevMonth = new Date(now);
          prevMonth.setDate(prevMonth.getDate() - 30);

            for(var i = 0; i < users.length; i++){
                dateUsers[i] = users[i].registered;
                dateUsers[i] = new Date(users[i].registered);
                dateUsers[i].toISOString();
                if(dateUsers[i] >= prevMonth && dateUsers[i] <= now){
                   monthUsers.push( dateUsers[i].getDate() + "-" + (dateUsers[i].getMonth()+1) + "-" + dateUsers[i].getFullYear() );
                   monthTypeUsers.push( users[i].role );

                 }
              }
              var a: number = 0;
              var b: number = 0;
              var c: number = 0;
              var d: number = 0;
              //day by day var ['nad', 'free', 'beta', 'paid', 'admin']
              var dbd = new Date(now);
              var countUsers = [];
              var activeUsers = [];
              var freeUsers = [];
              var betaUsers = [];
              var paidUsers = [];
              var finalUsers = [];
              $scope.freeTotal = 0;
              $scope.paidTotal = 0;
              $scope.usersTotal = 0;
              $scope.betaTotal = 0;
              $scope.activeUsers = 0;

              for(var i = 0; i < 31; i++){
              dbd = ( d => new Date(d.setDate(d.getDate()-i)) )(new Date);
              var fecha = dbd.getDate() + "-" + (dbd.getMonth()+1) + "-" + dbd.getFullYear();
              var dia = dbd.getDate();
                  for(var j = 0; j < monthUsers.length; j++){

                    if(monthUsers[j] == fecha){
                      d++;
                      countUsers[i] = d;
                      $scope.usersTotal++;

                        if(monthTypeUsers[j] == 'free'){
                          a++;
                          freeUsers[i] = a;
                          $scope.freeTotal++;
                        }
                        if(monthTypeUsers[j] == 'beta'){
                          b++;
                          betaUsers[i] = b;
                          $scope.betaTotal++;
                        }
                        if(monthTypeUsers[j] == 'paid'){
                          c++;
                          paidUsers[i] = c;
                          $scope.paidTotal++;
                        }
                      }


                    }

                    if(freeUsers[i] == undefined)
                       freeUsers[i] = 0;
                    if(betaUsers[i] == undefined)
                       betaUsers[i] = 0;
                    if(paidUsers[i] == undefined)
                       paidUsers[i] = 0;
                    if(countUsers[i] == undefined)
                       countUsers[i] = 0;

                finalUsers.push({ total: countUsers[i], free: freeUsers[i], beta: betaUsers[i], paid: paidUsers[i] , date: dia});
              }

              callback(finalUsers);
        });
      }
      //final month stats

      week( u => {
        var chartUserData = document.getElementById("myUserChart");
        var chartUser = new Chart(chartUserData, dc.week(u));
      });


      //others
      $scope.userStatsOthers = time => {


        let today = ( d => new Date(d.setDate(d.getDate())) )(new Date);
        var dd = ("0" + (today.getDate()).toString().slice(-2));
        var mm = ("0" + (today.getMonth()+1).toString().slice(-2));
        var yyyy = today.getFullYear().toString();
        var todayString = yyyy + "-" + mm + "-" + dd.slice(-2) + "T06:00:00.000Z";

        switch(time.name){

          case "Day":
            $http.get('/api/statistics').then( response => {

              for(var i in response.data){
                  if(response.data[i].date == todayString){
                    $scope.freeToPaid = response.data[i].freeToPaid;
                    $scope.visitorToSign = response.data[i].visitorToSign;
                    $scope.bitcoinUsers = response.data[i].bitcoinUsers;
                    $scope.cardUsers = response.data[i].cardUsers;
                    $scope.payPalUsers = response.data[i].payPalUsers;
                  }
               }
               if($scope.freeToPaid == undefined)
                  $scope.freeToPaid = 0;
               if($scope.visitorToSign == undefined)
                  $scope.visitorToSign = 0;
               if($scope.bitcoinUsers == undefined)
                  $scope.bitcoinUsers = 0;
               if($scope.payPalUsers == undefined)
                  $scope.payPalUsers = 0;
               if($scope.cardUsers == undefined)
                  $scope.cardUsers = 0;

            });
            break;

          case "Last 7 days":
             $http.get('/api/statistics').then( response => {
               var now = new Date();
               var dbd = new Date(now);
               var ftp: number = 0;
               var vts: number = 0;
               var btu: number = 0;
               var cdu: number = 0;
               var plu: number = 0;
               var ftp2: number = 0;
               var vts2: number = 0;
               var btu2: number = 0;
               var cdu2: number = 0;
               var plu2: number = 0;
               for(var i = 0; i < 7; i++){
               dbd = ( d => new Date(d.setDate(d.getDate()-i)) )(new Date);
               var dd = ("0" + (dbd.getDate()).toString().slice(-2));
               var mm = ("0" + (dbd.getMonth()+1).toString().slice(-2));
               var fecha = dbd.getFullYear() + "-" + mm + "-" + dd.slice(-2) + "T06:00:00.000Z";
               var dia = dbd.getDate();

                for(var j in response.data){
                    if(response.data[j].date == fecha){
                      ftp += response.data[j].freeToPaid != undefined ? response.data[j].freeToPaid : 0;
                      vts += response.data[j].visitorToSign != undefined  ? response.data[j].visitorToSign : 0;
                      btu += response.data[j].bitcoinUsers != undefined ? response.data[j].bitcoinUsers : 0;
                      cdu += response.data[j].cardUsers != undefined ? response.data[j].cardUsers : 0;
                      plu += response.data[j].payPalUsers != undefined ? response.data[j].payPalUsers : 0;
                    }//end if
                 }//end for j
               }//end for i
               for(var i = 7; i < 14; i++){
               dbd = ( d => new Date(d.setDate(d.getDate()-i)) )(new Date);
               var dd = ("0" + (dbd.getDate()).toString().slice(-2));
               var mm = ("0" + (dbd.getMonth()+1).toString().slice(-2));
               var fecha = dbd.getFullYear() + "-" + mm + "-" + dd.slice(-2) + "T06:00:00.000Z";
               var dia = dbd.getDate();

                for(var j in response.data){
                    if(response.data[j].date == fecha){
                      ftp2 += response.data[j].freeToPaid != undefined ? response.data[j].freeToPaid : 0;
                      vts2 += response.data[j].visitorToSign != undefined  ? response.data[j].visitorToSign : 0;
                      btu2 += response.data[j].bitcoinUsers != undefined ? response.data[j].bitcoinUsers : 0;
                      cdu2 += response.data[j].cardUsers != undefined ? response.data[j].cardUsers : 0;
                      plu2 += response.data[j].payPalUsers != undefined ? response.data[j].payPalUsers : 0;
                    }//end if
                 }//end for j
               }//end for i

               if(ftp == undefined)
                  ftp = 0;
               if(vts == undefined)
                  vts = 0;
               if(btu == undefined)
                  btu = 0;
               if(plu == undefined)
                  plu = 0;
               if(cdu == undefined)
                  cdu = 0;

                  $scope.freeToPaid = ftp;
                  $scope.visitorToSign = vts;
                  $scope.bitcoinUsers = btu;
                  $scope.cardUsers = cdu;
                  $scope.payPalUsers = plu;

                  if(ftp2 == undefined)
                     ftp2 = 0;
                  if(vts2 == undefined)
                     vts2 = 0;
                  if(btu2 == undefined)
                     btu2 = 0;
                  if(plu2 == undefined)
                     plu2 = 0;
                  if(cdu2 == undefined)
                     cdu2 = 0;

                  $scope.freeToPaidRange = ( (ftp - ftp2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))(ftp , ftp2) ) * 100;
                  $scope.visitorToSignRange = ( (vts - vts2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))(vts , vts2) ) * 100;
                  $scope.bitcoinUsersRange = ( (btu - btu2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))(btu , btu2) ) * 100;
                  $scope.cardUsersRange = ( (cdu - cdu2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))(cdu , cdu2) ) * 100;
                  $scope.payPalUsersRange = ( (plu - plu2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))(plu , plu2) ) * 100;

            });
            break;

          case "Month":
              $http.get('/api/statistics').then( response => {
                var now = new Date();
                var dbd = new Date(now);
                for(var i = 0; i < 31; i++){
                dbd = ( d => new Date(d.setDate(d.getDate()-i)) )(new Date);
                var dd = ("0" + (dbd.getDate()).toString().slice(-2));
                var mm = ("0" + (dbd.getMonth()+1).toString().slice(-2));
                var fecha = dbd.getFullYear() + "-" + mm + "-" + dd.slice(-2) + "T06:00:00.000Z";
                var dia = dbd.getDate();

                 for(var j in response.data){
                     if(response.data[j].date == fecha){
                       $scope.freeToPaid = $scope.freeToPaid + response.data[j].freeToPaid != undefined ? response.data[j].freeToPaid : 0;
                       $scope.visitorToSign = $scope.visitorToSign + response.data[j].visitorToSign != undefined  ? response.data[j].visitorToSign : 0;
                       $scope.bitcoinUsers = $scope.bitcoinUsers + response.data[j].bitcoinUsers != undefined ? response.data[j].bitcoinUsers : 0;
                       $scope.cardUsers = $scope.cardUsers + response.data[j].cardUsers != undefined ? response.data[j].cardUsers : 0;
                       $scope.payPalUsers = $scope.payPalUsers + response.data[j].payPalUsers != undefined ? response.data[j].payPalUsers : 0;
                     }//end if
                  }//end for j
                }//end for i

                for(var i = 31; i < 62; i++){
                dbd = ( d => new Date(d.setDate(d.getDate()-i)) )(new Date);
                var dd = ("0" + (dbd.getDate()).toString().slice(-2));
                var mm = ("0" + (dbd.getMonth()+1).toString().slice(-2));
                var fecha = dbd.getFullYear() + "-" + mm + "-" + dd.slice(-2) + "T06:00:00.000Z";
                var dia = dbd.getDate();

                 for(var j in response.data){
                     if(response.data[j].date == fecha){
                       $scope.freeToPaid = $scope.freeToPaid + response.data[j].freeToPaid != undefined ? response.data[j].freeToPaid : 0;
                       $scope.visitorToSign = $scope.visitorToSign + response.data[j].visitorToSign != undefined  ? response.data[j].visitorToSign : 0;
                       $scope.bitcoinUsers = $scope.bitcoinUsers + response.data[j].bitcoinUsers != undefined ? response.data[j].bitcoinUsers : 0;
                       $scope.cardUsers = $scope.cardUsers + response.data[j].cardUsers != undefined ? response.data[j].cardUsers : 0;
                       $scope.payPalUsers = $scope.payPalUsers + response.data[j].payPalUsers != undefined ? response.data[j].payPalUsers : 0;
                     }//end if
                  }//end for j
                }//end for i

                if($scope.freeToPaid == undefined)
                   $scope.freeToPaid = 0;
                if($scope.visitorToSign == undefined)
                   $scope.visitorToSign = 0;
                if($scope.bitcoinUsers == undefined)
                   $scope.bitcoinUsers = 0;
                if($scope.payPalUsers == undefined)
                   $scope.payPalUsers = 0;
                if($scope.cardUsers == undefined)
                   $scope.cardUsers = 0;

                if($scope.freeToPaid2 == undefined)
                   $scope.freeToPaid2 = 0;
                if($scope.visitorToSign2 == undefined)
                   $scope.visitorToSign2 = 0;
                if($scope.bitcoinUsers2 == undefined)
                   $scope.bitcoinUsers2 = 0;
                if($scope.payPalUsers2 == undefined)
                   $scope.payPalUsers2 = 0;
                if($scope.cardUsers2 == undefined)
                   $scope.cardUsers2 = 0;

                      $scope.freeToPaidRange = ( ($scope.freeToPaid - $scope.freeToPaid2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))($scope.freeToPaid , $scope.freeToPaid2) ) * 100;
                      $scope.visitorToSignRange = ( ($scope.visitorToSign - $scope.visitorToSign2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))($scope.visitorToSign , $scope.visitorToSign2) ) * 100;
                      $scope.bitcoinUsersRange = ( ($scope.bitcoinUsers - $scope.bitcoinUsers2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))($scope.bitcoinUsers , $scope.bitcoinUsers2) ) * 100;
                      $scope.cardUsersRange = ( ($scope.cardUsers - $scope.cardUsers2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))($scope.cardUsers , $scope.cardUsers2) ) * 100;
                      $scope.payPalUsersRange = ( ($scope.payPalUsers - $scope.payPalUsers2) / ( (a,b)=> a>b?(b==0?1:b):(a==0?1:a))($scope.payPalUsers , $scope.payPalUsers2) ) * 100;

             });
            break;

          }
      }

      setTimeout( ()=>{
        var time = { name : "Last 7 days"}
        $scope.userStatsOthers(time);
      } , 1000);

      setTimeout( ()=>{
        $scope.chartUser();
      } , 1000);

      $scope.chartUser = function(){

      var ua = [{"total":0,"free":0,"beta":0,"paid":0,"date":0},{"total":0,"free":0,"beta":0,"paid":0,"date":0},{"total":0,"free":0,"beta":0,"paid":0,"date":25},{"total":0,"free":0,"beta":0,"paid":0,"date":24},{"total":0,"free":0,"beta":0,"paid":0,"date":23},{"total":0,"free":0,"beta":0,"paid":0,"date":22},{"total":0,"free":0,"beta":0,"paid":0,"date":21}];
      var chartUserData = document.getElementById("myChartRevenue");
      var chartUser = new Chart(chartUserData, dc.weekRevenue(ua));
      }


      $scope.clickazo = function(){
        $http.post('/api/payments/listStripe')
        .then((response)=>{
          console.log(response.data)
        });
      }

  }

  $onInit(){

  }



}

export default angular.module('saasApp.data', [ngRoute])
  .config(routes)
  .component('data', {
    template: require('./data.html'),
    controller: DataComponent,
    controllerAs: 'dataCtrl'
  })
  .name;
