'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');
const Chart = require('chart.js');
const moment = require('moment');

export default class DataCharts {


    constructor(){

    }

    newDate(days) {
       return moment().add(days, 'd');
    }

    today(datos){

            var config = {
              type: 'line',
              data: {
                labels: [],
                datasets: [
                  {
                  label: 'Total Users',
                  borderColor: 'rgba(5,203,106,0.9)',
                  backgroundColor: 'rgba(5,203,106,0.2)',
                  data: [{
                    x : this.newDate(0),
                    y : datos[0].total
                  }],
                },
                {
                label: 'Active Users',
                borderColor: 'rgba(66,132,240,0.9)',
                backgroundColor: 'rgba(66,132,240,0.2)',
                data: [{
                  x : this.newDate(0),
                  y : datos[0].active
                }],
              },
                {
                  label: 'Paid Users',
                  borderColor: 'rgba(208,125,205,0.9)',
                  backgroundColor: 'rgba(208,125,205,0.2)',
                  data: [{
                    x : this.newDate(0),
                    y : datos[0].paid
                  }],
                },
                {
                  label: 'Free Users',
                  borderColor: 'rgba(234,108,86,0.9)',
                  backgroundColor: 'rgba(234,108,86,0.2)',
                  data: [{
                    x : this.newDate(0),
                    y : datos[0].free
                  }],
                },
                {
                  label: 'Beta Users',
                  borderColor: 'rgba(50,50,50,0.9)',
                  backgroundColor: 'rgba(50,50,50,0.2)',
                  data: [{
                    x : this.newDate(0),
                    y : datos[0].beta
                  }],
                }]
              },
              options: {
                legend: {
                        display: false,
                        position: 'bottom',
                        labels: {
                              pointStyle: true
                          }
                        },
                scales: {
                  xAxes: [{
                      display : true,
                      position: 'bottom',
                      type: 'time',
                      time: {
                      format: "HH:mm",
                      unit: "day",
                      unitStepSize: 1,
                      displayFormats: {
                        'minute': 'HH:mm',
                        'hour': 'HH:mm',
                        'day' : 'll',
                        'month': 'MMM YYYY',
                        },
                      }
                    }],
                },
          }
        };
      return config;

    }  //final today

    week(datos){
            var config = {
              type: 'line',
              data: {
                labels: [],
                datasets: [
                  {
                  label: 'Total Users',
                  borderColor: 'rgba(5,203,106,0.9)',
                  backgroundColor: 'rgba(5,203,106,0.2)',
                  data: [{
                    x : this.newDate(-6),
                    y : datos[6].total
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].total
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].total
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].total
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].total
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].total
                  }, {
                    x : this.newDate(0),
                    y : datos[0].total
                  }],
                },
                {
                label: 'Active Users',
                borderColor: 'rgba(66,132,240,0.9)',
                backgroundColor: 'rgba(66,132,240,0.2)',
                data: [{
                  x : this.newDate(-6),
                  y : datos[6].active
                }, {
                  x : this.newDate(-5),
                  y : datos[5].active
                }, {
                  x : this.newDate(-4),
                  y : datos[4].active
                }, {
                  x : this.newDate(-3),
                  y : datos[3].active
                }, {
                  x : this.newDate(-2),
                  y : datos[2].active
                }, {
                  x : this.newDate(-1),
                  y : datos[1].active
                }, {
                  x : this.newDate(0),
                  y : datos[0].active
                }],
              },
                {
                  label: 'Paid Users',
                  borderColor: 'rgba(208,125,205,0.9)',
                  backgroundColor: 'rgba(208,125,205,0.2)',
                  data: [{
                    x : this.newDate(-6),
                    y : datos[6].paid
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].paid
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].paid
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].paid
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].paid
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].paid
                  }, {
                    x : this.newDate(0),
                    y : datos[0].paid
                  }],
                },
                {
                  label: 'Free Users',
                  borderColor: 'rgba(234,108,86,0.9)',
                  backgroundColor: 'rgba(234,108,86,0.2)',
                  data: [{
                    x : this.newDate(-6),
                    y : datos[6].free
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].free
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].free
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].free
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].free
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].free
                  }, {
                    x : this.newDate(0),
                    y : datos[0].free
                  }],
                },
                {
                  label: 'Beta Users',
                  borderColor: 'rgba(50,50,50,0.9)',
                  backgroundColor: 'rgba(50,50,50,0.2)',
                  data: [{
                    x : this.newDate(-6),
                    y : datos[6].beta
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].beta
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].beta
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].beta
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].beta
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].beta
                  }, {
                    x : this.newDate(0),
                    y : datos[0].beta
                  }],
                }]
              },
              options: {
                legend: {
                        display: false,
                        position: 'bottom',
                        labels: {
                              pointStyle: true
                          }
                        },
                scales: {
                  xAxes: [{
                      display : true,
                      position: 'bottom',
                      type: 'time',
                      time: {
                      format: "HH:mm",
                      unit: "day",
                      unitStepSize: 1,
                      displayFormats: {
                        'minute': 'HH:mm',
                        'hour': 'HH:mm',
                        'day' : 'll',
                        'month': 'MMM YYYY',
                        },
                      }
                    }],
                },
          }
        };
      return config;

    }//final week



    month(datos){

            var config = {
              type: 'line',
              data: {
                labels: [],
                datasets: [
                  {
                  label: 'Total Users',
                  borderColor: 'rgba(5,203,106,0.9)',
                  backgroundColor: 'rgba(5,203,106,0.2)',
                  data: [{
                    x : this.newDate(-30),
                    y : datos[30].total
                  }, {
                    x : this.newDate(-29),
                    y : datos[29].total
                  }, {
                    x : this.newDate(-28),
                    y : datos[28].total
                  }, {
                    x : this.newDate(-27),
                    y : datos[27].total
                  }, {
                    x : this.newDate(-26),
                    y : datos[26].total
                  }, {
                    x : this.newDate(-25),
                    y : datos[25].total
                  }, {
                    x : this.newDate(-24),
                    y : datos[24].total
                  },{
                    x : this.newDate(-23),
                    y : datos[23].total
                  }, {
                    x : this.newDate(-22),
                    y : datos[22].total
                  }, {
                    x : this.newDate(-21),
                    y : datos[21].total
                  }, {
                    x : this.newDate(-20),
                    y : datos[20].total
                  }, {
                    x : this.newDate(-19),
                    y : datos[19].total
                  }, {
                    x : this.newDate(-18),
                    y : datos[18].total
                  }, {
                    x : this.newDate(-17),
                    y : datos[17].total
                  },{
                    x : this.newDate(-16),
                    y : datos[16].total
                  }, {
                    x : this.newDate(-15),
                    y : datos[15].total
                  }, {
                    x : this.newDate(-14),
                    y : datos[14].total
                  }, {
                    x : this.newDate(-13),
                    y : datos[13].total
                  }, {
                    x : this.newDate(-12),
                    y : datos[12].total
                  }, {
                    x : this.newDate(-11),
                    y : datos[11].total
                  }, {
                    x : this.newDate(-10),
                    y : datos[10].total
                  },{
                    x : this.newDate(-9),
                    y : datos[9].total
                  }, {
                    x : this.newDate(-8),
                    y : datos[8].total
                  }, {
                    x : this.newDate(-7),
                    y : datos[7].total
                  }, {
                    x : this.newDate(-6),
                    y : datos[6].total
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].total
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].total
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].total
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].total
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].total
                  }, {
                    x : this.newDate(0),
                    y : datos[0].total
                  }],
                },
                {
                label: 'Active Users',
                borderColor: 'rgba(66,132,240,0.9)',
                backgroundColor: 'rgba(66,132,240,0.2)',
                data: [{
                  x : this.newDate(-30),
                  y : datos[30].active
                }, {
                  x : this.newDate(-29),
                  y : datos[29].active
                }, {
                  x : this.newDate(-28),
                  y : datos[28].active
                }, {
                  x : this.newDate(-27),
                  y : datos[27].active
                }, {
                  x : this.newDate(-26),
                  y : datos[26].active
                }, {
                  x : this.newDate(-25),
                  y : datos[25].active
                }, {
                  x : this.newDate(-24),
                  y : datos[24].active
                },{
                  x : this.newDate(-23),
                  y : datos[23].active
                }, {
                  x : this.newDate(-22),
                  y : datos[22].active
                }, {
                  x : this.newDate(-21),
                  y : datos[21].active
                }, {
                  x : this.newDate(-20),
                  y : datos[20].active
                }, {
                  x : this.newDate(-19),
                  y : datos[19].active
                }, {
                  x : this.newDate(-18),
                  y : datos[18].active
                },{
                  x : this.newDate(-17),
                  y : datos[17].active
                }, {
                  x : this.newDate(-16),
                  y : datos[16].active
                }, {
                  x : this.newDate(-15),
                  y : datos[15].active
                }, {
                  x : this.newDate(-14),
                  y : datos[14].active
                }, {
                  x : this.newDate(-13),
                  y : datos[13].active
                }, {
                  x : this.newDate(-12),
                  y : datos[12].active
                },{
                  x : this.newDate(-11),
                  y : datos[11].active
                }, {
                  x : this.newDate(-10),
                  y : datos[10].active
                }, {
                  x : this.newDate(-9),
                  y : datos[9].active
                }, {
                  x : this.newDate(-8),
                  y : datos[8].active
                }, {
                  x : this.newDate(-7),
                  y : datos[7].active
                }, {
                  x : this.newDate(-6),
                  y : datos[6].active
                },{
                  x : this.newDate(-5),
                  y : datos[5].active
                }, {
                  x : this.newDate(-4),
                  y : datos[4].active
                }, {
                  x : this.newDate(-3),
                  y : datos[3].active
                }, {
                  x : this.newDate(-2),
                  y : datos[2].active
                }, {
                  x : this.newDate(-1),
                  y : datos[1].active
                }, {
                  x : this.newDate(0),
                  y : datos[0].active
                },],
              },
                {
                  label: 'Paid Users',
                  borderColor: 'rgba(208,125,205,0.9)',
                  backgroundColor: 'rgba(208,125,205,0.2)',
                  data: [{
                    x : this.newDate(-30),
                    y : datos[30].paid
                  }, {
                    x : this.newDate(-29),
                    y : datos[29].paid
                  }, {
                    x : this.newDate(-28),
                    y : datos[28].paid
                  }, {
                    x : this.newDate(-27),
                    y : datos[27].paid
                  }, {
                    x : this.newDate(-26),
                    y : datos[26].paid
                  }, {
                    x : this.newDate(-25),
                    y : datos[25].paid
                  }, {
                    x : this.newDate(-24),
                    y : datos[24].paid
                  },{
                    x : this.newDate(-23),
                    y : datos[23].paid
                  }, {
                    x : this.newDate(-22),
                    y : datos[22].paid
                  }, {
                    x : this.newDate(-21),
                    y : datos[21].paid
                  }, {
                    x : this.newDate(-20),
                    y : datos[20].paid
                  }, {
                    x : this.newDate(-19),
                    y : datos[19].paid
                  }, {
                    x : this.newDate(-18),
                    y : datos[18].paid
                  },{
                    x : this.newDate(-17),
                    y : datos[17].paid
                  }, {
                    x : this.newDate(-16),
                    y : datos[16].paid
                  }, {
                    x : this.newDate(-15),
                    y : datos[15].paid
                  }, {
                    x : this.newDate(-14),
                    y : datos[14].paid
                  }, {
                    x : this.newDate(-13),
                    y : datos[13].paid
                  }, {
                    x : this.newDate(-12),
                    y : datos[12].paid
                  },{
                    x : this.newDate(-11),
                    y : datos[11].paid
                  }, {
                    x : this.newDate(-10),
                    y : datos[10].paid
                  }, {
                    x : this.newDate(-9),
                    y : datos[9].paid
                  }, {
                    x : this.newDate(-8),
                    y : datos[8].paid
                  }, {
                    x : this.newDate(-7),
                    y : datos[7].paid
                  }, {
                    x : this.newDate(-6),
                    y : datos[6].paid
                  },{
                    x : this.newDate(-5),
                    y : datos[5].paid
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].paid
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].paid
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].paid
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].paid
                  }, {
                    x : this.newDate(0),
                    y : datos[0].paid
                  },],
                },
                {
                  label: 'Free Users',
                  borderColor: 'rgba(234,108,86,0.9)',
                  backgroundColor: 'rgba(234,108,86,0.2)',
                  data: [{
                    x : this.newDate(-30),
                    y : datos[30].free
                  }, {
                    x : this.newDate(-29),
                    y : datos[29].free
                  }, {
                    x : this.newDate(-28),
                    y : datos[28].free
                  }, {
                    x : this.newDate(-27),
                    y : datos[27].free
                  }, {
                    x : this.newDate(-26),
                    y : datos[26].free
                  }, {
                    x : this.newDate(-25),
                    y : datos[25].free
                  }, {
                    x : this.newDate(-24),
                    y : datos[24].free
                  },{
                    x : this.newDate(-23),
                    y : datos[23].free
                  }, {
                    x : this.newDate(-22),
                    y : datos[22].free
                  }, {
                    x : this.newDate(-21),
                    y : datos[21].free
                  }, {
                    x : this.newDate(-20),
                    y : datos[20].free
                  }, {
                    x : this.newDate(-19),
                    y : datos[19].free
                  }, {
                    x : this.newDate(-18),
                    y : datos[18].free
                  },{
                    x : this.newDate(-17),
                    y : datos[17].free
                  }, {
                    x : this.newDate(-16),
                    y : datos[16].free
                  }, {
                    x : this.newDate(-15),
                    y : datos[15].free
                  }, {
                    x : this.newDate(-14),
                    y : datos[14].free
                  }, {
                    x : this.newDate(-13),
                    y : datos[13].free
                  }, {
                    x : this.newDate(-12),
                    y : datos[12].free
                  },{
                    x : this.newDate(-11),
                    y : datos[11].free
                  }, {
                    x : this.newDate(-10),
                    y : datos[10].free
                  }, {
                    x : this.newDate(-9),
                    y : datos[9].free
                  }, {
                    x : this.newDate(-8),
                    y : datos[8].free
                  }, {
                    x : this.newDate(-7),
                    y : datos[7].free
                  }, {
                    x : this.newDate(-6),
                    y : datos[6].free
                  },{
                    x : this.newDate(-5),
                    y : datos[5].free
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].free
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].free
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].free
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].free
                  }, {
                    x : this.newDate(0),
                    y : datos[0].free
                  },],
                },
                {
                  label: 'Beta Users',
                  borderColor: 'rgba(50,50,50,0.9)',
                  backgroundColor: 'rgba(50,50,50,0.2)',
                  data: [{
                    x : this.newDate(-30),
                    y : datos[30].beta
                  }, {
                    x : this.newDate(-29),
                    y : datos[29].beta
                  }, {
                    x : this.newDate(-28),
                    y : datos[28].beta
                  }, {
                    x : this.newDate(-27),
                    y : datos[27].beta
                  }, {
                    x : this.newDate(-26),
                    y : datos[26].beta
                  }, {
                    x : this.newDate(-25),
                    y : datos[25].beta
                  }, {
                    x : this.newDate(-24),
                    y : datos[24].beta
                  },{
                    x : this.newDate(-23),
                    y : datos[23].beta
                  }, {
                    x : this.newDate(-22),
                    y : datos[22].beta
                  }, {
                    x : this.newDate(-21),
                    y : datos[21].beta
                  }, {
                    x : this.newDate(-20),
                    y : datos[20].beta
                  }, {
                    x : this.newDate(-19),
                    y : datos[19].beta
                  }, {
                    x : this.newDate(-18),
                    y : datos[18].beta
                  }, {
                    x : this.newDate(-17),
                    y : datos[17].beta
                  },{
                    x : this.newDate(-16),
                    y : datos[16].beta
                  }, {
                    x : this.newDate(-15),
                    y : datos[15].beta
                  }, {
                    x : this.newDate(-14),
                    y : datos[14].beta
                  }, {
                    x : this.newDate(-13),
                    y : datos[13].beta
                  }, {
                    x : this.newDate(-12),
                    y : datos[12].beta
                  }, {
                    x : this.newDate(-11),
                    y : datos[11].beta
                  }, {
                    x : this.newDate(-10),
                    y : datos[10].beta
                  },{
                    x : this.newDate(-9),
                    y : datos[9].beta
                  }, {
                    x : this.newDate(-8),
                    y : datos[8].beta
                  }, {
                    x : this.newDate(-7),
                    y : datos[7].beta
                  }, {
                    x : this.newDate(-6),
                    y : datos[6].beta
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].beta
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].beta
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].beta
                  },{
                    x : this.newDate(-2),
                    y : datos[2].beta
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].beta
                  }, {
                    x : this.newDate(0),
                    y : datos[0].beta
                  }],
                }]
              },
              options: {
                legend: {
                        display: false,
                        position: 'bottom',
                        labels: {
                              pointStyle: true
                          }
                        },
                scales: {
                  xAxes: [{
                      display : true,
                      position: 'bottom',
                      type: 'time',
                      time: {
                      format: "HH:mm",
                      unit: "day",
                      unitStepSize: 1,
                      displayFormats: {
                        'minute': 'HH:mm',
                        'hour': 'HH:mm',
                        'day' : 'll',
                        'month': 'MMM YYYY',
                        },
                      }
                    }],
                },
          }
        };
      return config;

    }//final month


    weekRevenue(datos){

            var config = {
              type: 'line',
              data: {
                labels: [],
                datasets: [
                  {
                  label: 'Total Users',
                  borderColor: 'rgba(5,203,106,0.9)',
                  backgroundColor: 'rgba(5,203,106,0.2)',
                  data: [{
                    x : this.newDate(-6),
                    y : datos[6].total
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].total
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].total
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].total
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].total
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].total
                  }, {
                    x : this.newDate(0),
                    y : datos[0].total
                  }],
                },
                {
                label: 'Active Users',
                borderColor: 'rgba(66,132,240,0.9)',
                backgroundColor: 'rgba(66,132,240,0.2)',
                data: [{
                  x : this.newDate(-6),
                  y : datos[6].active
                }, {
                  x : this.newDate(-5),
                  y : datos[5].active
                }, {
                  x : this.newDate(-4),
                  y : datos[4].active
                }, {
                  x : this.newDate(-3),
                  y : datos[3].active
                }, {
                  x : this.newDate(-2),
                  y : datos[2].active
                }, {
                  x : this.newDate(-1),
                  y : datos[1].active
                }, {
                  x : this.newDate(0),
                  y : datos[0].active
                }],
              },
                {
                  label: 'Paid Users',
                  borderColor: 'rgba(208,125,205,0.9)',
                  backgroundColor: 'rgba(208,125,205,0.2)',
                  data: [{
                    x : this.newDate(-6),
                    y : datos[6].paid
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].paid
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].paid
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].paid
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].paid
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].paid
                  }, {
                    x : this.newDate(0),
                    y : datos[0].paid
                  }],
                },
                {
                  label: 'Free Users',
                  borderColor: 'rgba(234,108,86,0.9)',
                  backgroundColor: 'rgba(234,108,86,0.2)',
                  data: [{
                    x : this.newDate(-6),
                    y : datos[6].free
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].free
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].free
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].free
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].free
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].free
                  }, {
                    x : this.newDate(0),
                    y : datos[0].free
                  }],
                },
                {
                  label: 'Beta Users',
                  borderColor: 'rgba(50,50,50,0.9)',
                  backgroundColor: 'rgba(50,50,50,0.2)',
                  data: [{
                    x : this.newDate(-6),
                    y : datos[6].beta
                  }, {
                    x : this.newDate(-5),
                    y : datos[5].beta
                  }, {
                    x : this.newDate(-4),
                    y : datos[4].beta
                  }, {
                    x : this.newDate(-3),
                    y : datos[3].beta
                  }, {
                    x : this.newDate(-2),
                    y : datos[2].beta
                  }, {
                    x : this.newDate(-1),
                    y : datos[1].beta
                  }, {
                    x : this.newDate(0),
                    y : datos[0].beta
                  }],
                }]
              },
              options: {
                legend: {
                        display: false,
                        position: 'bottom',
                        labels: {
                              pointStyle: true
                          }
                        },
                scales: {
                  xAxes: [{
                      display : true,
                      position: 'bottom',
                      type: 'time',
                      time: {
                      format: "HH:mm",
                      unit: "day",
                      unitStepSize: 1,
                      displayFormats: {
                        'minute': 'HH:mm',
                        'hour': 'HH:mm',
                        'day' : 'll',
                        'month': 'MMM YYYY',
                        },
                      }
                    }],
                },
          }
        };
      return config;

    }//final weekRevenue



  }//final class
