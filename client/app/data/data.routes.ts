'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/data', {
      template: '<data></data>',
      controllerAs: 'admin',
      authenticate: 'admin',
      title: 'Data',
      titleS: 'Datos'
    });
}
