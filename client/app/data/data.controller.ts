'use strict';
var Chart = require('chart.js');

export default class DataController {
  users: Object[];
  $http;
  data = "";
  toastr;
  publicToken;
  secretToken;
  _id;
  Auth;
  Rkey;
  client_id;
  secret;
  breakdown;

  /*@ngInject*/
  constructor(Auth, User, $http, $scope, toastr, appConfig, $rootScope) {
    // Use the User $resource to fetch all users
    this.users = User.query();
    this.$http = $http;
    this.toastr = toastr;
    this.Auth = Auth;

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

    $scope.mensaje = "hola";
    var ctx = document.getElementById("myUserChart");

    var scatterChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                label: '',
                borderColor: 'rgba(92,131,239,0.9)',
                backgroundColor: 'rgba(92,131,239,0.2)',
                data: []
            },
            {
                label: '',
                borderColor: 'rgba(207,107,88,0.9)',
                backgroundColor: 'rgba(207,107,88,0.2)',
                data: []
            },
            {
                label: '',
                borderColor: 'rgba(115,203,112,0.9)',
                backgroundColor: 'rgba(115,203,112,0.2)',
                data: []
            }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: true,
            scales: {
                xAxes: [{
                    type: 'time',
                    position: 'bottom',
                  }]
            }
        }
    });


    var ctx = document.getElementById("myChart2");

    var scatterChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                label: '',
                borderColor: 'rgba(92,131,239,0.9)',
                backgroundColor: 'rgba(92,131,239,0.2)',
                data: []
            },
            {
                label: '',
                borderColor: 'rgba(207,107,88,0.9)',
                backgroundColor: 'rgba(207,107,88,0.2)',
                data: []
            },
            {
                label: '',
                borderColor: 'rgba(115,203,112,0.9)',
                backgroundColor: 'rgba(115,203,112,0.2)',
                data: []
            }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: true,
            scales: {
                xAxes: [{
                    type: 'time',
                    position: 'bottom',
                  }]
            }
        }
    });





  }



}
