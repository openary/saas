'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/blog', {
      template: '<blog></blog>',
      authenticate: 'admin',
      title: 'Blog',
      titleS: 'Blog'
    });
}
