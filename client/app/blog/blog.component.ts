'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');
const trumbowyg = require('trumbowyg');
import 'trumbowyg';

import routes from './blog.routes';

export class BlogComponent {
  /*@ngInject*/
  constructor($rootScope, $scope, $http) {

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

    interface ${
      trumbowyg():void;
    }

      (<any>$('#editor')).trumbowyg({
          svgPath : '/assets/fonts/icons.svg',
          btns: [
              ['viewHTML'],
              ['formatting'],
              'btnGrp-semantic',
              ['superscript', 'subscript'],
              ['link'],
              ['insertImage'],
              'btnGrp-justify',
              'btnGrp-lists',
              ['horizontalRule'],
              ['removeformat'],
              ['fullscreen']
          ]
      });


  }
}

export default angular.module('saasApp.blog', [ngRoute])
  .config(routes)
  .component('blog', {
    template: require('./blog.html'),
    controller: BlogComponent,
    controllerAs: 'blogCtrl'
  })
  .name;
