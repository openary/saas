'use strict';
var en = require('../assets/languages/en');
var es = require('../assets/languages/es');
export function routeConfig($httpProvider, $routeProvider, $locationProvider, stripeProvider, callHttpProvider, $translateProvider, vcRecaptchaServiceProvider) {
  'ngInject';
  $routeProvider
    .otherwise({
      redirectTo: '/'
    });
    $locationProvider.html5Mode(true);


var initInjector = angular.injector(['ng']);
var $http = initInjector.get('$http');


//request for api Stripe
    var tok = "";

    callHttpProvider.$get().settings().then(response => {
          for(var a in response.data){

              if(response.data[a].name == "Stripe"){
                tok = response.data[a].publicToken;
              stripeProvider.setPublishableKey(tok);
              }

        }

    });


$translateProvider.translations('en', en.translations);
$translateProvider.translations('es', es.translations);
$translateProvider.preferredLanguage('en');
$translateProvider.fallbackLanguage('en');

}
