'use strict';

export default function($routeProvider) {
  'ngInject';
  $routeProvider
    .when('/calendar', {
      template: '<calendar></calendar>',
      authenticate: 'beta',
      authent: 'paid',
      title: 'Calendar',
      titleS: 'Calendario'
    });
}
