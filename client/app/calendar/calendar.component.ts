'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');


import routes from './calendar.routes';

export class CalendarComponent {
  /*@ngInject*/
  constructor($scope, $rootScope, $http) {

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

    $scope.events = [
    {
      start: getDate(-6, 10),
      end: getDate(-2, 13),
      title: 'Event 1'
    }

  ];
  $scope.selected = $scope.events[0];

  $scope.eventClicked = function (item) {
    console.log(item);
  };

  $scope.createClicked = function (date) {
    console.log(date);
  };

  function getDate(offsetDays, hour) {
    offsetDays = offsetDays || 0;
    var offset = offsetDays * 24 * 60 * 60 * 1000;
    var date = new Date(new Date().getTime() + offset);
    if (hour) { date.setHours(hour); }
    return date;
  }


  }
}

export default angular.module('saasApp.calendar', [ngRoute])
  .config(routes)
  .component('calendar', {
    template: require('./calendar.html'),
    controller: CalendarComponent,
    controllerAs: 'calendarCtrl'
  })
  .name;
