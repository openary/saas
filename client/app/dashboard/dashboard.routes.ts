'use strict';

export default function routes($routeProvider) {
  'ngInject';


  $routeProvider
    .when('/dashboard', {
      template: '<dashboard></dashboard>',
      authenticate: 'beta',
      authent: 'paid',
      title: 'Overview',
      titleS: 'General'
    });
}
