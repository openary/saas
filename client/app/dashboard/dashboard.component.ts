'use strict';
const angular = require('angular');
const ngRoute = require('angular-route');
var Chart = require('chart.js');
var moment = require('moment');
var convert = require('xml-to-json-promise');

import routes from './dashboard.routes';

export class DashboardComponent {
  /*@ngInject*/

  usuario;
  $http;
  Auth;
  data = [];

  constructor(Auth, $http, $scope, $rootScope) {
    this.usuario = Auth.getCurrentUserSync();
    this.$http = $http;
    this.Auth = Auth;
    this.$onInit();



        $scope.time =  $scope.time  || [
          { id: 1, name: 'Day' },
          { id: 2, name: 'Week' },
          { id: 3, name: 'Month' },
        ];

    $scope.$on('$routeChangeSuccess', function (event, data) {
      $http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         $rootScope.subTitle = data.titleS;
         else
          $rootScope.subTitle = data.title;
      });
     });

        var config = {
          type: 'line',
          data: {
            labels: [],
            datasets: [{
              label: 'Income',
              borderColor: 'rgba(92,131,239,0.9)',
              backgroundColor: 'rgba(92,131,239,0.2)',
              data: [{}],
            }]
          },
          options: {
            legend: {
                    display: false,
                    position: 'bottom',
                    labels: {
                          pointStyle: true
                      }
                    },
            scales: {
              xAxes: [{
                  display : true,
                  position: 'bottom',
                  type: 'time',
                  time: {
                  format: "HH:mm",
                  unit: 'day',
                  unitStepSize: 1,
                  displayFormats: {
                    'minute': 'HH:mm',
                    'hour': 'HH:mm',
                    'day' : 'll'
                    },
                  }
                }],
            },
      }
    };

  setTimeout(()=>{
    var ctx = document.getElementById("myChart");
   new Chart(ctx, config);

 },500)


     $scope.break =  $scope.break  || [
       { id: 1, name: 'Day' },
       { id: 2, name: 'Week' },
       { id: 3, name: 'Month' },

     ];
     $scope.time =  $scope.time  || [
       { id: 1, name: 'Today' },
       { id: 2, name: 'Yesterday' },
       { id: 3, name: 'Last 7 days' },
       { id: 4, name: 'Last 30 days'}

     ];

  }

  newDate(days) {
     return moment().add(days, 'd');
  }

  $onInit(){

    var today = new Date();
    var dd = ("0" + today.getDate()).slice(-2);
    var mm = ("0" + today.getMonth()+1).slice(-2);
    var yyyy = today.getFullYear();
    var datos = [];
    var todayString = yyyy + "-" + mm +'-'+ dd;
    var todayStringCake = mm + "/" + dd + "/" + yyyy;
    var d = ("0" + (today.getDate()-1)).slice(-2);
    var yesterdayString = yyyy + "-" + mm +'-'+ d;
    var apiKey = "";
    var netID = "";

    var curr = new Date();
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first - 7; // last day is the first day + 6
    var endDate = new Date(curr.setDate(last));
    var previousWeek = endDate.getFullYear() + "-" +  ("0" + (endDate.getMonth() + 1)).slice(-2) + "-" + ("0" + endDate.getDate()).slice(-2);

            for(var a in this.usuario.networks){

              if(this.usuario.networks[a].name == "Adsense"){

                  apiKey = this.usuario.networks[a].APIKey;
                  netID = this.usuario.networks[a].NetworkID;

                  this.$http.post('/api/users/getToken', {_id: this.Auth.getCurrentUserSync()._id }).then(response=>{
                       console.log(response.data);


                          this.$http.get('https://www.googleapis.com/adsense/v1.4/reports?startDate=' + previousWeek + '&endDate=' + todayString + '&accountId=' + apiKey + '&dimension=DATE&metric=EARNINGS&access_token=' + response.data.token).then(response=>{
                                this.data.push(response.data.row);

                               var ctx = document.getElementById("myChart");
                              new Chart(ctx, config);
                         });
                  });
              }


              if(this.usuario.networks[a].name == "getCake"){
                  apiKey = this.usuario.networks[a].APIKey;
                  netID = this.usuario.networks[a].NetworkID;
                  var website = this.usuario.networks[a].website;

                  for(var i = 7; i >= 0; i--){
                      this.$http.post('/api/networks/getCake' , {website: website, apiKey: apiKey, netID: netID, date: todayStringCake})
                        .then(response => {
                          var jsondata = {};

                          convert.xmlDataToJSON(response.data).then(j => {

                              this.data[i] +=  j.performance_summary_response.periods[0].period[0].current_revenue[0];
                            //  data.push(j.performance_summary_response.periods[0].period[0].current_revenue[0]);

                            });

                      });


                          for(var a in this.usuario.networks){
                              if(this.usuario.networks[a].name == "hasOffers"){
                                  apiKey = this.usuario.networks[a].APIKey;
                                  netID = this.usuario.networks[a].NetworkID;

                              var i = 0;
                              var limit = 6;

                               var f = () =>{
                                 if(limit < 0){
                                //   var ctx = document.getElementById("chartReports");
                                   //new Chart(ctx, rr.sevenDays(datos, breakdown));
                                   return;
                                 }


                                var endDate = new Date(today.setDate(limit));
                                var d2 = ("0" + endDate.getDate()).slice(-2);
                                var changeDay = yyyy + "-" + ("0" + endDate.getMonth()+1).slice(-2) +'-'+ d2;

                                this.$http.get('https://api.hasoffers.com/Apiv3/json?NetworkId=' + netID + '&Target=Affiliate_AffiliateBilling&Method=getPayoutTotals&api_key='+ apiKey +'&timeframes%5B0%5D%5Bstart_date%5D=' + changeDay + '&timeframes%5B0%5D%5Bend_date%5D=' + changeDay +'&timeframes%5B0%5D%5Blabel%5D=sday')
                                .then(response => {

                                  this.data[i] += response.data.response.data.sday.total_payout;
                                  //data.push(response.data.response.data.sday.total_payout);

                                });

                                if(i <= limit){
                                  setTimeout(f  , 500);
                                }


                                  limit--;
                              }
                              //execute function
                              f();

                            }
                        }


                    }
                }
                var config = {
                  type: 'line',
                  data: {
                    labels: [],
                    datasets: [{
                      label: 'Income',
                      borderColor: 'rgba(92,131,239,0.9)',
                      backgroundColor: 'rgba(92,131,239,0.2)',
                      data: [{
                        x : this.newDate(-7),
                        y : this.data[0]
                      }, {
                        x : this.newDate(-6),
                        y : this.data[1]
                      }, {
                        x : this.newDate(-5),
                        y : this.data[2]
                      }, {
                        x : this.newDate(-4),
                        y : this.data[3]
                      }, {
                        x : this.newDate(-3),
                        y : this.data[4]
                      }, {
                        x : this.newDate(-2),
                        y : this.data[5]
                      }, {
                        x : this.newDate(-1),
                        y : this.data[6]
                      }],
                    }]
                  },
                  options: {
                    legend: {
                            display: false,
                            position: 'bottom',
                            labels: {
                                  pointStyle: true
                              }
                            },
                    scales: {
                      xAxes: [{
                          display : true,
                          position: 'bottom',
                          type: 'time',
                          time: {
                          format: "HH:mm",
                          unit: 'day',
                          unitStepSize: 1,
                          displayFormats: {
                            'minute': 'HH:mm',
                            'hour': 'HH:mm',
                            'day' : 'll'
                            },
                          }
                        }],
                    },
              }
            };

            var ctx = document.getElementById("myChart");
           new Chart(ctx, config);

          }




  }


}

export default angular.module('saasApp.dashboard', [ngRoute])
  .config(routes)
  .component('dashboard', {
    template: require('./dashboard.html'),
    controller: DashboardComponent,
    controllerAs: 'dashboard',
  })
  .name;
