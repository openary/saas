'use strict';
/* eslint no-sync: 0 */
const angular = require('angular');

export class NavbarComponent {
  /*@ngInject*/
  $location;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  isCollapsed = true;
  usuario='';
  things = [];
  avatar = [];
  logo = [];
  socket;
  $http;
  $scope;
  $rootScope;

  constructor($rootScope, socket, $location, Auth, $scope, $translate, $http) {
    'ngInject';
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
    this.usuario = Auth.getCurrentUserSync();
    this.socket = socket;
    this.$http = $http;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$location = $location;


    $("#divSlider").hide();
    $("#divSlider").click( function() {
      $("#wrapper").toggleClass("toggled");
      $( this ).hide();
    });

    $(".second-part-wrapper li").click( function() {
      $("#wrapper").toggleClass("toggled");
      $( "#divSlider" ).hide();
    });

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#divSlider").show();
    });
    $("#menu-toggle2").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#divSlider").hide();
    });

    $http.get('/api/tl')
    .then(response => {
      $scope.title = response.data[0].name;
      $scope.tl = response.data;
      socket.syncUpdates('tl', $scope.tl);
    });

  }

  $onInit(){


    this.$http.get('/api/tl')
    .then(response => {
      this.logo = response.data;
      this.socket.syncUpdates('tl', this.logo);
      this.$scope.logo = response.data[0].logo;
      this.$rootScope.titulo = response.data[0].name;
    });

    this.$scope.$on('$routeChangeSuccess', (event, data) => {

    if(data.$$route.originalPath == '/login' || data.$$route.originalPath == '/signup' || data.$$route.originalPath == '/' || data.$$route.originalPath == '' ){

    }else{
      this.$http.get('/api/users/me')
      .then(response => {
        if(response.data.language === "es")
         this.$scope.sub = data.titleS;
         else
          this.$scope.sub = data.title;
          if(response.data.img)
            this.avatar = [response.data];

          this.avatar = [response.data];
          this.socket.syncUpdates('user', this.avatar);
      });
    }
        this.$scope.namePage = this.$location.path();
        this.$scope.namePage = this.$scope.namePage.replace(/[/]/g,"");
        this.$scope.namePage = this.$scope.namePage.charAt(0).toUpperCase() + this.$scope.namePage.slice(1);
    });

    this.$http.get('/api/notifications')
    .then(response =>{
      this.things = response.data;
      this.socket.syncUpdates('notifications', this.things);
    });
  }

  isActive(route) {
    return route === this.$location.path();
  }

  isHidden(){
    if(this.$location.path() == '/signup')
     return true;
    else if(this.$location.path() == '/login')
     return true;
    else if(this.$location.path() == '/free-trial')
     return true;
    else
     return false;
  }

}

export default angular.module('directives.navbar', [])
  .component('navbar', {
    template: require('./navbar.html'),
    controller: NavbarComponent
  })
  .name;
