var es = {
  NAVBAR : {
    HOME : 'Inicio',
    OPT: "Opciones",
    OPTIONS: {1:'Configuración', 2:'Api Keys', 3:'Blog'},
    DASHBOARD: 'General',
    REPORTS: 'Reportes',
    ACCOUNTS: 'Cuentas',
    CALENDAR: 'Calendario',
    LOGOUT: 'Cerrar Sesión'
  },
  NAVADMIN : {
    DATA : 'Datos',
    USERS: 'Usuarios',
    NOTIFICATIONS: 'Notificaciones',
    COUPONS: 'Cupones',
  },
  USER : {
    SETTINGS : {
      TITLE : 'Cuenta',
      PHOTO : 'Añadir foto de perfil',
      DPHOTO: 'Eliminar foto de perfil',
      BUTTONS : {1: 'Inglés' , 2: 'Español'},
      SUB : 'Suscribir a la Newsletter',
      TITLE2: 'Facturación',
      TITLE3: 'Informes',
      TITLE4: 'Facturas',
      AMOUNT: 'Cantidad',
      DATE: 'Fecha',
      INVOICE: 'Factura'
    },
    OVERVIEW : {
      TITLE2 : 'Desglose por fuente',
      TITLE3 : 'Rendimiento'
    },
    REPORTS : {
      TITLE : 'Filtros',
      TABLE : {D: 'Fecha', I: 'Impresiones', C: 'Clicks', CTR: 'CTR', CPC: 'CPC', IRPM: 'Impresiones RPM', AV: 'Active View', EE: 'Ingresos estimados'}
    },
    ACCOUNTS : {
      TITLE : 'Añade tu primera cuenta',
      SUB : 'y empieza a medir como nunca antes',
      ADDED : 'Cuentas añadidas',
      TITLE2 : 'Añadir una cuenta',
      TABLE : {ID: 'ID', S: 'Fuente', A: 'Cuenta', AID: 'ID Cuenta', NID: 'ID Red', AUTH: 'Autenticación', TAG: 'Tag', ST: 'Estado', AC: 'Acciones'},
      THRESHOLD : {MT: "Mínimo de cobro", PT: "Términos de pago", PM: "Métodos de pago"}
    }
  },
  //ADMIN

  ADMIN : {
    DATA : {
      TITLE : 'Usuarios',
      THUMBS : {TU: 'Usuarios Totales', AU: 'Usuarios Activos', PU: 'Usuarios de Pago', FU: 'Usuarios Gratis', BT: 'Beta Testers'},
      TITLE2 : 'Ingresos',
        THUMBS2: {TR: 'Ingresos totales', MRR: 'Ingresos Recurrentes Mensuales',AMRRU: 'Promedio mensual de ingresos recurrentes por usuario', LV: 'Valor del tiempo de vida' },
      TITLE3 : 'Otros',
      THUMBS3 : {UAC: 'Coste de Adquisición de Usuario', CR: 'Tasa de Churn', FPU: 'Usuarios de Gratis a Pago', ACL: 'Media Vida Útil del Cliente', VSU: 'De Visitante a Registro', PU: 'Usuarios PayPal', CDC: 'Tarjeta crédito/débito' , BU: 'Usuarios Bitcoin'}
    },
    USERS : {
      TABLE : {ID: 'ID', F: 'Nombre', L: 'Apellidos', E: 'Email', SUD: 'Fecha de Registro', T: 'Tipo', SUBS: 'Suscrito', R: 'Ingreso', AC: 'Cuentas', CO: 'País', LAN: 'Idioma', A: 'Acciones'}
    },
    NOTIFICATIONS :  {
      TITLE : 'Crear',
      TITLE2 : 'Enviar',
      TABLE : {ID: 'ID', T: 'Título', B: 'Cuerpo', D: 'Fecha', H: 'Hora', O: 'Aperturas', OR: 'Tasa de Apertura'}
    },
    COUPONS : {
      TITLE : 'Crear',
      TITLE2 : 'Creado',
      TABLE : {ID: 'ID',N: 'Nombre', C: 'Código', D: 'Descuento', CR: 'Creación', E: 'Expiración', DU: 'Duración', U: 'Uso', UL: 'Límite de Uso', S: 'Estado'}
    },
    PLANS : {
      TITLE : 'Plan',
      TITLE2 : 'Creado',
      TABLE : {ID: 'ID', N: 'Nombre', A: 'Cantidad', I: 'Intervalo', C: 'Moneda', F: 'Frecuencia'}
    }
  },

  COMUN: {
    CHARTS : {I: "INGRESO" ,E: "GASTOS", P: "BENEFICIO" },
    BUTTONS : {C: "CANCELAR", F: "FILTRAR", AD: "AÑADIR CUENTA"}
  }

};

exports.translations = es;
