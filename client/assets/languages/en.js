var en = {
  NAVBAR : {
    HOME : 'Home',
    OPT: "Options",
    OPTIONS: {1:'Settings', 2:'Api Keys', 3:'Blog'},
    DASHBOARD: 'Overview',
    REPORTS: 'Reports',
    ACCOUNTS: 'Accounts',
    CALENDAR: 'Calendar',
    LOGOUT: 'Logout'
  },
  NAVADMIN : {
    DATA : 'Data',
    USERS: 'Users',
    NOTIFICATIONS: 'Notifications',
    COUPONS: 'Coupons',
  },
  USER : {
    SETTINGS : {
      TITLE : 'Account',
      PHOTO : 'Add profile photo',
      DPHOTO: 'Delete profile photo',
      BUTTONS : {1: 'English' , 2: 'Spanish'},
      SUB : 'Subscribe to Newsletter',
      TITLE2: 'Billing',
      TITLE3: 'Reporting',
      TITLE4: 'Invoices',
      AMOUNT: 'Amount',
      DATE: 'Date',
      INVOICE: 'Invoice'
    },
    OVERVIEW : {
      TITLE2 : 'Breakdown by source',
      TITLE3 : 'Performance'
    },
    REPORTS : {
      TITLE : 'Filters',
      TABLE : {D: 'Date', I: 'Impressions', C: 'Clicks', CTR: 'CTR', CPC: 'CPC', IRPM: 'Impressions RPM', AV: 'Active View', EE: 'Estimated earnings'}
    },
    ACCOUNTS : {
      TITLE : 'Add your first account',
      SUB : 'and start tracking like never before',
      ADDED : 'Accounts added',
      TITLE2 : 'Add on account',
      TABLE : {ID: 'ID', S: 'Source', A: 'Account', AID: 'Account ID', NID: 'Network ID', AUTH: 'Authentication', TAG: 'Tag', ST: 'Status', AC: 'Actions'},
      THRESHOLD : {MT: "Minimun threshold", PT: "Payment terms", PM: "Payment methods"}
    }
  },
  //ADMIN

  ADMIN : {
    DATA : {
      TITLE : 'Users',
      THUMBS : {TU: 'Total Users', AU: 'Active Users', PU: 'Paid Users', FU: 'Free Users', BT: 'Beta Testers'},
      TITLE2 : 'Revenue',
      THUMBS2: {TR: 'Total Revenue', MRR: 'Monthly Recurring Revenue',AMRRU: 'Average Monthly Recurring Revenue per User', LV: 'Lifetime Value' },
      TITLE3 : 'Others',
      THUMBS3 : {UAC: 'User Acquisition Cost', CR: 'Churn Rate', FPU: 'Free to Paid User', ACL: 'Average Customer Lifetime', VSU: 'Visitor to Sign Up', PU: 'PayPal Users', CDC: 'Credit / Debit Card' , BU: 'Bitcoins Users'}
    },
    USERS : {
      TABLE : {ID: 'ID', F: 'Firstname', L: 'Lastname', E: 'Email', SUD: 'Sign Up Date', T: 'Type', SUBS: 'Subscribed', R: 'Revenue', AC: 'Accounts', CO: 'Country', LAN: 'Language', A: 'Actions'}
    },
    NOTIFICATIONS :  {
      TITLE : 'Create',
      TITLE2 : 'Sent',
      TABLE : {ID: 'ID', T: 'Title', B: 'Body', D: 'Date', H: 'Hour', O: 'Openings', OR: 'Open Rate'}
    },
    COUPONS : {
      TITLE : 'Create',
      TITLE2 : 'Created',
      TABLE : {ID: 'ID',N: 'Name', C: 'Code', D: 'Discount', CR: 'Creation', E: 'Expiration', DU: 'Duration', U: 'Usage', UL: 'Usage Limit', S: 'Status'}
    },
    PLANS : {
      TITLE : 'Plan',
      TITLE2 : 'Created',
      TABLE : {ID: 'ID', N: 'Name', A: 'Amount', I: 'Interval', C: 'Currency', F: 'Frecuency'}
    }
  },

  COMUN: {
    CHARTS : {I: "INCOME" ,E: "EXPENSES", P: "PROFIT" },
    BUTTONS : {C: "CANCEL", F: "FILTER", AD: "ADD ACCOUNT"}
  }

};

exports.translations = en;
