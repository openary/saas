/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/networks              ->  index
 * POST    /api/networks              ->  create
 * GET     /api/networks/:id          ->  show
 * PUT     /api/networks/:id          ->  upsert
 * PATCH   /api/networks/:id          ->  patch
 * DELETE  /api/networks/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Networks from './networks.model';
import Settings from '../settings/settings.model';
import User from '../user/user.model';
var fs = require('fs');
var mkdirp = require('mkdirp');
var query = Settings.find();
var readline = require('readline');

var google = require('googleapis');
var OAuth2Client = google.auth.OAuth2;
var plus = google.plus('v1');
var paypal = require('paypal-rest-sdk');
var openIdConnect = paypal.openIdConnect;



function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

var https = require('https');
var http = require('http');
// Gets a list of Networkss
export function index(req, res) {

  User.findById(req.query._id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
  /*return Networks.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
    */


}

// Gets a single Networks from the DB
export function show(req, res) {
  return Networks.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Networks in the DB
export function create(req, res) {
  return Networks.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Networks in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Networks.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Networks in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Networks.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Networks from the DB
export function destroy(req, res) {
  return Networks.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


export function getuser(req, res){
  return User.findById(req.query._id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}


export function getCake(req, res){

  var options = {
      hostname: req.body.website,
      path: '/affiliates/api/2/reports.asmx/PerformanceSummary?api_key=' + req.body.apiKey + '&affiliate_id=' + req.body.netID + '&date=' + req.body.date
  };

  var gsaReq = http.get(options, function (response) {
      var completeResponse = '';
      response.on('data', function (chunk) {
          completeResponse += chunk;
      });
      response.on('end', function() {
          res.send(completeResponse);
      })
  }).on('error', function (e) {
      console.log('problem with request: ' + e.message);
  });

}

export function adsense(req, res){

  var rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
  });

  Settings.findOne({name : "Adsense"} , function(err, data){
    var CLIENT_ID = data.publicToken;
    var CLIENT_SECRET = data.secretToken;
    var REDIRECT_URL = req.get('origin')+"/accounts?a";
    var oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

    var url = oauth2Client.generateAuthUrl({
      access_type: 'offline', // will return a refresh token
      scope: 'https://www.googleapis.com/auth/adsense.readonly' // can be a space-delimited string or an array of scopes
    });

    res.json(url);
    console.log('Visit the url: ', url);
    rl.question('Enter the code here:', function (code) {
      // request access token

      oauth2Client.getToken(code, function (err, tokens) {
        if (err) {
          return console.log(err);
        }
        // set tokens to the client
        // TODO: tokens should be set by OAuth2 client.
        oauth2Client.setCredentials(tokens);
      //  callback();
      });
    });

  });
}

export function adsensetoken(req, res){

  Settings.findOne({name : "Adsense"} , function(err, data){
    var CLIENT_ID = data.publicToken;
    var CLIENT_SECRET = data.secretToken;
    var REDIRECT_URL = req.get('origin')+"/accounts?a";
    var oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

    var scopes = [
      'https://www.googleapis.com/auth/adsense.readonly'
    ];

    var url = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: 'https://www.googleapis.com/auth/adsense.readonly' // can be a space-delimited string or an array of scopes
    });
console.log(url);

      oauth2Client.getToken(req.body.token, function (err, tokens) {
        if (err) {
          return console.log(err);
        }
        // set tokens to the client
        // TODO: tokens should be set by OAuth2 client.
        res.json(tokens);
        oauth2Client.setCredentials(tokens);

    });

  });
}
const exec = require('child_process').exec;

export function amazon(req, res){

var sleep = false;

if(!sleep){

        mkdirp('./client/assets/users/' + req.body._id, function(err) {

              const phantom = exec(
              'phantomjs --ignore-ssl-errors=yes --local-to-remote-url-access=yes ../../../../Olivaw/olivaw.js sergiomen4@gmail.com Sergio123. MX trends lastSevenDays xml' ,
               {cwd : './client/assets/users/' + req.body._id}
             );

              phantom.stdout.on('data', (data) => {
                console.log(`stdout: ${data}`);
              });

              phantom.stderr.on('data', (data) => {
                console.log(`stderr: ${data}`);
              });

              phantom.on('close', (code) => {
                console.log(`child process exited with code ${code}`);

                fs.readFile('./client/assets/users/' + req.body._id + '/report-MX-orders-today.xml', 'utf8', function(err, data) {
                  if(err)
                  {
                      return console.log(err);
                  }
                  console.log(data);
                  if(data != "Error!  HTTP Status: 0")
                    sleep = true;
                });

              });

         });

      }

  setTimeout(function(){} , );

}


var ci = '';
var st = '';

function getPaypal(callback){
 query.exec((err, api) => {

   for(var a in api){
       if(api[a].name == "Paypal"){
         ci = api[a].publicToken;
         st = api[a].secretToken;
        callback(ci, st);
       };
    }

 });
}

/*getPaypal((a , b) => {

  paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': a,
    'client_secret': b,
    'openid_redirect_uri': 'http://localhost:3000/accounts?p'
  });

});
*/


export function paypal(req, res){



  return User.findById(req.body._id).exec( function(err, user){
    if(err) throw err;


    for(var i in user.networks){

      if(user.networks[i].tag == req.body.tag){

      var credentials = { username: user.networks[i].NetworkID,
                          password: user.networks[i].APIKey,
                          signature: user.networks[i].signature,
                          live: true }; // false for sandbox mode, true for live mode

      var paypal = new PayPal(credentials);

      console.log(req.body.date + "-------" + req.body.date2)
      paypal.call('TransactionSearch',
                  { StartDate: req.body.date ,
                    EndDate: req.body.date2
                 },
                  function (error, transactions) {
        if (error) {
          console.error('API call error: ' + error);
        } else {
          console.log(transactions);
          res.json(transactions);
        }
      });

     }
    }

  });


}
/*
username: 'sergiomen4_api1.gmail.com',
                    password: 'Z9PSJ3LA9442NKM8',
                    signature: 'AFcWxV21C7fd0v3bYYYRCpSSRl31AfnK36IoTolKMXWHPHMeUFBjqMuS'
                    */
