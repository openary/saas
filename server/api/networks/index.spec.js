'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var networksCtrlStub = {
  index: 'networksCtrl.index',
  show: 'networksCtrl.show',
  create: 'networksCtrl.create',
  upsert: 'networksCtrl.upsert',
  patch: 'networksCtrl.patch',
  destroy: 'networksCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var networksIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './networks.controller': networksCtrlStub
});

describe('Networks API Router:', function() {
  it('should return an express router instance', function() {
    expect(networksIndex).to.equal(routerStub);
  });

  describe('GET /api/networks', function() {
    it('should route to networks.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'networksCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/networks/:id', function() {
    it('should route to networks.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'networksCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/networks', function() {
    it('should route to networks.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'networksCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/networks/:id', function() {
    it('should route to networks.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'networksCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/networks/:id', function() {
    it('should route to networks.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'networksCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/networks/:id', function() {
    it('should route to networks.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'networksCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
