'use strict';

var app = require('../..');
import request from 'supertest';

var newNetworks;

describe('Networks API:', function() {
  describe('GET /api/networks', function() {
    var networkss;

    beforeEach(function(done) {
      request(app)
        .get('/api/networks')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          networkss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(networkss).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/networks', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/networks')
        .send({
          name: 'New Networks',
          info: 'This is the brand new networks!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newNetworks = res.body;
          done();
        });
    });

    it('should respond with the newly created networks', function() {
      expect(newNetworks.name).to.equal('New Networks');
      expect(newNetworks.info).to.equal('This is the brand new networks!!!');
    });
  });

  describe('GET /api/networks/:id', function() {
    var networks;

    beforeEach(function(done) {
      request(app)
        .get(`/api/networks/${newNetworks._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          networks = res.body;
          done();
        });
    });

    afterEach(function() {
      networks = {};
    });

    it('should respond with the requested networks', function() {
      expect(networks.name).to.equal('New Networks');
      expect(networks.info).to.equal('This is the brand new networks!!!');
    });
  });

  describe('PUT /api/networks/:id', function() {
    var updatedNetworks;

    beforeEach(function(done) {
      request(app)
        .put(`/api/networks/${newNetworks._id}`)
        .send({
          name: 'Updated Networks',
          info: 'This is the updated networks!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedNetworks = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedNetworks = {};
    });

    it('should respond with the original networks', function() {
      expect(updatedNetworks.name).to.equal('New Networks');
      expect(updatedNetworks.info).to.equal('This is the brand new networks!!!');
    });

    it('should respond with the updated networks on a subsequent GET', function(done) {
      request(app)
        .get(`/api/networks/${newNetworks._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let networks = res.body;

          expect(networks.name).to.equal('Updated Networks');
          expect(networks.info).to.equal('This is the updated networks!!!');

          done();
        });
    });
  });

  describe('PATCH /api/networks/:id', function() {
    var patchedNetworks;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/networks/${newNetworks._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Networks' },
          { op: 'replace', path: '/info', value: 'This is the patched networks!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedNetworks = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedNetworks = {};
    });

    it('should respond with the patched networks', function() {
      expect(patchedNetworks.name).to.equal('Patched Networks');
      expect(patchedNetworks.info).to.equal('This is the patched networks!!!');
    });
  });

  describe('DELETE /api/networks/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/networks/${newNetworks._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when networks does not exist', function(done) {
      request(app)
        .delete(`/api/networks/${newNetworks._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
