'use strict';

import mongoose from 'mongoose';

var NetworksSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model('Networks', NetworksSchema);
