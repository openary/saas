'use strict';

var express = require('express');
var controller = require('./networks.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);
router.post('/getuser', controller.getuser);
router.post('/adsense', controller.adsense);
router.post('/adsensetoken', controller.adsensetoken);
router.post('/amazon' , controller.amazon);
router.post('/getCake', controller.getCake);
router.post('/paypal', controller.paypal);

module.exports = router;
