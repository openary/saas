/**
 * Networks model events
 */

'use strict';

import {EventEmitter} from 'events';
import Networks from './networks.model';
var NetworksEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
NetworksEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Networks.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    NetworksEvents.emit(event + ':' + doc._id, doc);
    NetworksEvents.emit(event, doc);
  };
}

export default NetworksEvents;
