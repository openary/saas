'use strict';

import mongoose from 'mongoose';

var RandomSchema = new mongoose.Schema({
  email: String,
  secretToken: String,
  expireIn: {type: Date, expires: '15s'}
});

export default mongoose.model('RandomToken', RandomSchema);
