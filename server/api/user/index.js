'use strict';

import {Router} from 'express';
import * as controller from './user.controller';
import * as auth from '../../auth/auth.service';
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './client/assets/avatars')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname+ '-' + Date.now()+'.jpg')
    }
});
var upload = multer({ storage: storage });

var router = new Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.post('/changelan' , controller.changelanguage);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.put('/:id/role',  auth.hasRole('admin'), controller.changeRole);
router.put('/update', auth.isAuthenticated(), controller.update);
router.put('/billing', auth.isAuthenticated(), controller.billing);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);
router.post('/networks' , auth.isAuthenticated(), controller.addNetwork);
router.post('/addToken' , auth.isAuthenticated(), controller.addToken);
router.post('/getToken' , auth.isAuthenticated(), controller.getToken);
router.put('/network', auth.isAuthenticated(), controller.deletenetwork);
router.post('/avatar', auth.isAuthenticated(), upload.single('file'), controller.avatar);
router.post('/deleteavatar', auth.isAuthenticated(), controller.deleteavatar);


module.exports = router;
