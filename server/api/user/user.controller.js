'use strict';

import User from './user.model';
import Random from './random.model';
import Tokens from './token.model';
import config from '../../config/environment';
import jwt from 'jsonwebtoken';
import Stats from '../statistics/statistics.model';

var express = require('express');
var app = express();
var nodemailer = require("nodemailer");
var multer = require('multer');
var upload = multer();
var fs = require('fs');

var transporter = nodemailer.createTransport({
    host: 'juegosparapsvita.org',
    port: 587,
    secure: false,
    tls:{
            rejectUnauthorized: false
        },
    auth: {
        user: 'noreply@juegosparapsvita.org',
        pass: 'v2bbDf0gLo'
    }
});


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}


function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    return res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    return res.status(statusCode).send(err);
  };
}

//Upload avatar

export function avatar(req, res){

  var imgPath = req.file.path;
  imgPath = imgPath.replace(/client\//g, "");
    return User.findById(req.body._id).exec()
        .then(user => {
          if(user) {
            user.img = imgPath;

            return user.save()
              .then((e,u) => {
                res.send(user);
              })
              .catch(validationError(res));
          } else {
            return res.status(403).end();
          }
        });
}


//Upload avatar

export function deleteavatar(req, res){

console.log(req.body)

  var imgPath = null;

    return User.findById(req.body._id).exec()
        .then(user => {
          if(user) {
            user.img = imgPath;

            return user.save()
              .then((e,u) => {
                res.send(user);
              })
              .catch(validationError(res));
          } else {
            return res.status(403).end();
          }
      });
}

/**
 * Get list of users
 * restriction: 'admin'
 */
export function index(req, res) {
  return User.find({}, '-salt -password').exec()
    .then(users => {
      res.status(200).json(users);
    })
    .catch(handleError(res));
}

//change language
export function changelanguage(req, res){
  return User.findById(req.body._id).exec()
    .then(user => {
      if(user) {
        user.language = req.body.lan;

        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(validationError(res));
      } else {
        return res.status(403).end();
      }
    });
}

/**
 * Creates a new user
 */
export function create(req, res) {
  var newUser = new User(req.body);
  var expireDate = new Date();
//Here you can make the role with conditional function
  var email = req.body.email;
  var randomToken = Math.floor((Math.random() * 99999999999999999) + 1);

  Random.create({email: req.body.email, secretToken: randomToken, expireIn: new Date(+new Date() +1*1*1*300*1000) }, function(rand){
    console.log('Se ha guardado el token secreto');
  });
var mailOptions = {
    from: 'noreply@juegosparapsvita.org', // sender address
    to: newUser.email, // list of receivers
    subject: 'Please confirm your account', // Subject line
    text: 'Thanks for signing up for Tracking', // plaintext body
    html: req.get('origin') + '/api/confirms/email/' + randomToken + '/' + email + '<br/>Happy Tracking!' // html body
};

let today = ( d => new Date(d.setDate(d.getDate())) )(new Date);
var dd = ("0" + (today.getDate()).toString().slice(-2));
var mm = ("0" + (today.getMonth()+1).toString().slice(-2));
var yyyy = today.getFullYear().toString();

var todayString = mm + "/" + dd + "/" + yyyy;

transporter.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
});

if(req.body.freeCoupon == 'free'){
  if(req.body.role == 'nad'){
    newUser.provider = 'local';
    newUser.confirmedEmail = false;
    newUser.role = 'nad';
     newUser.expireIn = new Date(+new Date() +14*24*60*60*1000);
     newUser.save()
       .then(function(user) {
         var token = jwt.sign({ _id: user._id }, config.secrets.session, {
           expiresIn: 60 * 60 * 5
         });

         res.json({ token });
       })
       .catch(validationError(res));
  }
} else {
  if(req.body.role == 'nad'){
    newUser.provider = 'local';
    newUser.confirmedEmail = false;
    newUser.role = 'nad';
     newUser.expireIn = new Date(+new Date() +29*24*60*60*1000);
     newUser.save()
       .then(function(user) {
         var token = jwt.sign({ _id: user._id }, config.secrets.session, {
           expiresIn: 60 * 60 * 5
         });

         Stats.findOne({date: todayString} , function(err, s){

           if(!s){
               var stats = new Stats({date: todayString, visitorToSign : 1 });
               stats.save( function(err, stat){
                 console.log(stat);
               });
           }else{
             Stats.findOneAndUpdate({date: todayString} , { $inc : { visitorToSign: 1} })
             .exec(function(err, stat){
               console.log(stat);
             });
           }

         });

         res.json({ token });
       })
       .catch(validationError(res));
  }
}


if(req.body.role == 'paid'){
  newUser.provider = 'local';
  newUser.role = 'paid';
  newUser.expireIn = new Date(+new Date() + 29*24*60*60*1000);
  newUser.save()
    .then(function(user) {
      var token = jwt.sign({ _id: user._id }, config.secrets.session, {
        expiresIn: 60 * 60 * 5
      });
      res.json({ token });
    })
    .catch(validationError(res));
  }

  if(req.body.role == 'beta'){
    newUser.provider = 'local';
    newUser.role = 'beta';
    newUser.save()
      .then(function(user) {
        var token = jwt.sign({ _id: user._id }, config.secrets.session, {
          expiresIn: 60 * 60 * 5
        });
        res.json({ token });
      })
      .catch(validationError(res));
    }

}

/**
 * Get a single user
 */
export function show(req, res, next) {
  var userId = req.params.id;

  return User.findById(userId).exec()
    .then(user => {
      if(!user) {
        return res.status(404).end();
      }
      res.json(user.profile);
    })
    .catch(err => next(err));
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
export function destroy(req, res) {
  return User.findByIdAndRemove(req.params.id).exec()
    .then(function() {
      res.status(204).end();
    })
    .catch(handleError(res));
}

/**
 * Change a users password
 */
export function changePassword(req, res) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);
  var name = String(req.body.name);
  var lastname = String(req.body.lastname);
  var email = String(req.body.email);

  return User.findById(userId).exec()
    .then(user => {
      if(user.authenticate(oldPass)) {
        user.password = newPass;
        user.name = name;
        user.lastname = lastname;
        user.email = email;

        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(validationError(res));
      } else {
        return res.status(403).end();
      }
    });


}

export function update(req, res) {
  var userId = req.user._id;

  if(req.user._id)
    var userId = req.user._id;
  else
    var userId = req.body._id;

  return User.findOneAndUpdate({_id : userId} , { $set: { name: req.body.billing.name, lastname: req.body.billing.lastname, address: req.body.billing.street, city: req.body.billing.city, state: req.body.billing.state, postal_code: req.body.billing.pc, country: req.body.billing.country, phone: req.body.phone, business_name: req.body.business_name, tax_id: req.body.tax_id} }).exec()
    .then(user => {
      res.status(204).end();
    })
    .catch(validationError(res));
}


export function billing(req, res) {

  return User.findOneAndUpdate({_id : req.body._id} , { $set: { address: req.body.user.street, city: req.body.user.city, state: req.body.state, postal_code: req.body.pc, country: req.body.country, phone: req.body.phone, business_name: req.body.business_name, tax_id: req.body.tax_id} }).exec()
    .then(user => {
      res.status(204).end();
    })
    .catch(validationError(res));
}


export function changeRole(req, res) {
  var userId = req.body._id;
  var role = String(req.body.role);

  if(req.body.role == 'free'){

     return User.findById(userId).exec()
       .then(user => {
         if(user) {
           user.role = role;
           user.provider = 'local';
           user.role = 'free';
           user.expireIn = new Date(+new Date() +14*24*60*60*1000);
           return user.save()
             .then(() => {
               res.status(204).end();
             })
             .catch(validationError(res));
         } else {
           return res.status(403).end();
         }
       });

  }

  if(req.body.role == 'paid'){

    return User.findById(userId).exec()
      .then(user => {
        if(user) {
          user.role = role;
          user.provider = 'local';
          user.role = 'paid';
          user.expireIn = new Date(+new Date() + 29*24*60*60*1000);
          return user.save()
            .then(() => {
              res.status(204).end();
            })
            .catch(validationError(res));
        } else {
          return res.status(403).end();
        }
      });

    }

    if(req.body.role == 'beta'){

      return User.findById(userId).exec()
        .then(user => {
          if(user) {
            user.role = role;
            user.provider = 'local';
            user.role = 'beta';
            user.expireIn = "";
            return user.save()
              .then(() => {
                res.status(204).end();
              })
              .catch(validationError(res));
          } else {
            return res.status(403).end();
          }
        });

      }

}

/**
 * Get my info
 */
export function me(req, res, next) {
  var userId = req.user._id;

  return User.findOne({ _id: userId }, '-salt -password').exec()
    .then(user => { // don't ever give out the password or salt
      if(!user) {
        return res.status(401).end();
      }
      res.json(user);
    })
    .catch(err => next(err));
}

/**
 * Authentication callback
 */
export function authCallback(req, res) {
  res.redirect('/');
}


export function addNetwork(req , res){

  User.findOne({_id: req.body._id}, function(err, user){
    if(err) throw err;



    if(user.networks.length == 0){
      return User.findOneAndUpdate({_id: req.body._id}, {$push: { "networks" : {APIKey: req.body.APIKey , NetworkID: req.body.NetworkID, name: req.body.name, signature: req.body.signature, tag: req.body.tag, auth: req.body.auth, website: req.body.website, account:1} } }, {upsert: true} ).exec()
        .then(respondWithResult(res))
        .catch(handleError(res));
    }else{
      var a = 0;
      var b = 0;
      var c = 0;
      var d = 0;
      var e = 0;


      for(var i in user.networks){

      if(user.networks[i].tag == req.body.tag || user.networks[i].APIKey == req.body.APIKey){
        res.send("error");
      }else {

        if(user.networks[i].name == "Paypal") {
          a++;
          if(i == user.networks.length - 1 )
           addNetworkCall(a , req, res)
        }
        else if(user.networks[i].name == "hasOffers") {
          b++;
          if(i == user.networks.length - 1 )
           addNetworkCall(b , req, res)
        }
        else if(user.networks[i].name == "getCake") {
          c++;
          if(i == user.networks.length - 1 )
           addNetworkCall(c , req, res)
        }
        else if(user.networks[i].name == "Amazon") {
          d++;
          if(i == user.networks.length - 1 )
           addNetworkCall(d , req, res)
        }
        else if(user.networks[i].name == "Adsense") {
          e++;
          if(i == user.networks.length - 1 )
           addNetworkCall(e , req, res)
        }

      }


    }//end of for

      console.log("a:" + a + " b:" + b + " c:" + c + " d:"+ d + " e:" + e);

    }

  });


}

function addNetworkCall(a, req, res){
  return User.findOneAndUpdate({_id: req.body._id}, {$push: { "networks" : {APIKey: req.body.APIKey , NetworkID: req.body.NetworkID, name: req.body.name,signature: req.body.signature , tag: req.body.tag, auth: req.body.auth, website: req.body.website, account: a} } }, {upsert: true} ).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}



export function addToken(req , res){
  return User
    .findOne({ _id: req.body._id })
    .populate('id_user')
    .exec(function (err, user) {
      if (err) return handleError(err);

      Tokens.create({id_user : user._id , token : req.body.token},function(err, ant){
        if(err) res.send(err);
        res.json(ant);
      });

    });

}

export function getToken(req, res){
  Tokens.findOne({id_user: req.body._id} ).populate('id_user').exec( function(err , user){
          if(err)throw err;

          res.json(user);

      })
}


export function deletenetwork(req, res){
  return User.findOneAndUpdate({_id: req.body._id}, {$pull: { "networks" : { tag: req.body.tag} } } ).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}
