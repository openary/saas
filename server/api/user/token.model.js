'use strict';

import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var TokenSchema = new mongoose.Schema({
  id_user: [{type: Schema.Types.ObjectId, ref : 'User'}],
  token: String
});

export default mongoose.model('Tokens', TokenSchema);
