'use strict';

import mongoose from 'mongoose';

var PlanSchema = new mongoose.Schema({
  amount: Number,
  interval: String,
  name: String,
  currency: String,
  frequency: String,
  id: String
});

export default mongoose.model('Plan', PlanSchema);
