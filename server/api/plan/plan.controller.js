/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/plan              ->  index
 * POST    /api/plan              ->  create
 * GET     /api/plan/:id          ->  show
 * PUT     /api/plan/:id          ->  upsert
 * PATCH   /api/plan/:id          ->  patch
 * DELETE  /api/plan/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Plan from './plan.model';
import Settings from '../settings/settings.model';

var paypal = require('paypal-rest-sdk');
var query = Settings.find();
var tok;
var stripe;
var pt;
var st;

function getStripe(callback){
   query.exec((err, api) => {

     for(var a in api){
         if(api[a].name == "Stripe"){
          tok = api[a].secretToken;
          callback(tok);
         };
      }

   });
}

getStripe( function(t){
   stripe = require('stripe')(t);
});

 stripe = require('stripe')(tok);


 function getPaypal(callback){
    query.exec((err, api) => {

      for(var a in api){
          if(api[a].name == "Paypal"){
           st = api[a].secretToken;
           pt = api[a].publicToken;
           callback( pt , st );
          };
       }

    });
 }



 getPaypal( function( pt , st ){
   paypal.configure({
     'mode': 'sandbox', //sandbox or live
     'client_id': pt,
     'client_secret': st
   });

 });

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Plans
export function index(req, res) {
  return Plan.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Plan from the DB
export function show(req, res) {
  return Plan.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Plan in the DB
export function create(req, res) {

  var stripe = "";

  getStripe((t) => {
     stripe = require('stripe')(t);

       stripe.plans.create({
         amount: req.body.plan.amount,
         interval: (req.body.plan.interval).toLowerCase(),
         name: req.body.plan.name,
      //   frequency: req.body.plan.frequency,
         currency: req.body.plan.currency,
         id: req.body.plan.id
       }, function(err, plan) {
         // asynchronously called
         console.log(err);
         console.log(plan);
       });

  });

    //console.log(paypal);
  getPaypal( function( pt , st ){
    paypal.configure({
      'mode': 'sandbox', //sandbox or live
      'client_id': pt,
      'client_secret': st
    });


      var isoDate = new Date();
      isoDate.setSeconds(isoDate.getSeconds() + 4);
      isoDate.toISOString().slice(0, 19) + 'Z';

      var billingPlanAttributes = {
        name: 'Saas',
        description: 'Monthly plan for getting the t-shirt of the month.',
        type: 'fixed',
        payment_definitions: [{
          name: req.body.plan.name,
          type: 'REGULAR',
          frequency_interval: req.body.plan.frequency,
          frequency: (req.body.plan.interval).toUpperCase(),
          cycles: '11',
          amount: {
            currency: (req.body.plan.currency).toUpperCase(),
            value: req.body.plan.amount
          }
        }],
        merchant_preferences: {
          setup_fee: {
            currency: 'USD',
            value: '1'
          },
          cancel_url: 'http://localhost:3000/cancel',
          return_url: 'http://localhost:3000/processagreement',
          max_fail_attempts: '0',
          auto_bill_amount: 'YES',
          initial_fail_amount_action: 'CONTINUE'
        }
      };

      var billingPlanUpdateAttributes = [
          {
              "op": "replace",
              "path": "/",
              "value": {
                  "state": "ACTIVE"
              }
          }
      ];

      var billingAgreementAttributes = {
          "name": "Fast Speed Agreement",
          "description": "Agreement for Fast Speed Plan",
          "start_date": isoDate,
          "plan": {
              "id": "P-0NJ10521L3680291SOAQIVTQ"
          },
          "payer": {
              "payment_method": "paypal"
          },
          "shipping_address": {
              "line1": "StayBr111idge Suites",
              "line2": "Cro12ok Street",
              "city": "San Jose",
              "state": "CA",
              "postal_code": "95112",
              "country_code": "US"
          }
      };

      // Create the billing plan
      paypal.billingPlan.create(billingPlanAttributes, function (error, billingPlan) {
          if (error) {
              console.log(error);
              throw error;
          } else {
              console.log("Create Billing Plan Response");
              console.log(billingPlan);
          }
      });


  });


  return Plan.create(req.body.plan)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));

}

// Upserts the given Plan in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Plan.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Plan in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Plan.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Plan from the DB
export function destroy(req, res) {
  return Plan.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


export function addPlan(req, res){

  var stripe = "";

  getStripe((t) => {
     stripe = require('stripe')(t);
  });

  Plan.find().exec((err, p) => {
    if(p.length == 1){
       p[0].Plan = req.body.Plan.Plan;

    return Plan.findOneAndUpdate({_id: p[0]._id}, p[0], {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec();

    }else{
      console.log(stripe);
      stripe.plans.create({
          amount: req.body.Plan.Plan,
          interval: "month",
          name: "default",
          currency: "usd",
          id: "default"
        }, function(err, plan) {
          // asynchronously called
          console.log(err);
          console.log(plan)
        });

      return Plan.create(req.body.Plan);
    }
  })
  .then(respondWithResult(res))
  .catch(handleError(res));

}

export function getPlan(req, res){
  Plan.find().exec()
  .then(respondWithResult(res))
  .catch(handleError(res));
}
