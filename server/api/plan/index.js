'use strict';

var express = require('express');
var controller = require('./plan.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);
router.post('/addPlan', auth.hasRole('admin'), controller.addPlan);
router.post('/getPlan', auth.hasRole('admin'), controller.getPlan);

module.exports = router;
