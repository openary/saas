/**
 * Cupons model events
 */

'use strict';

import {EventEmitter} from 'events';
import Cupons from './cupons.model';
var CuponsEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CuponsEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Cupons.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    CuponsEvents.emit(event + ':' + doc._id, doc);
    CuponsEvents.emit(event, doc);
  };
}

export default CuponsEvents;
