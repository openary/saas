'use strict';

import mongoose from 'mongoose';

var PriceSchema = new mongoose.Schema({
  price: Number,
  interval: String,
  name: String,
  currency: String,
  frequency: String,
  id: String
});

export default mongoose.model('Price', PriceSchema);
