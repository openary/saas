'use strict';

import mongoose from 'mongoose';

var CuponsSchema = new mongoose.Schema({
  name: String,
  code: String,
  discount: Number,
  creation: Date,
  expiration: Date,
  duration: Number,
  usage: Number,
  status: String,
  limit: Number,
  def: Boolean

});

export default mongoose.model('Cupons', CuponsSchema);
