'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var cuponsCtrlStub = {
  index: 'cuponsCtrl.index',
  show: 'cuponsCtrl.show',
  create: 'cuponsCtrl.create',
  upsert: 'cuponsCtrl.upsert',
  patch: 'cuponsCtrl.patch',
  destroy: 'cuponsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var cuponsIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './cupons.controller': cuponsCtrlStub
});

describe('Cupons API Router:', function() {
  it('should return an express router instance', function() {
    expect(cuponsIndex).to.equal(routerStub);
  });

  describe('GET /api/cupons', function() {
    it('should route to cupons.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'cuponsCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/cupons/:id', function() {
    it('should route to cupons.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'cuponsCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/cupons', function() {
    it('should route to cupons.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'cuponsCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/cupons/:id', function() {
    it('should route to cupons.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'cuponsCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/cupons/:id', function() {
    it('should route to cupons.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'cuponsCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/cupons/:id', function() {
    it('should route to cupons.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'cuponsCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
