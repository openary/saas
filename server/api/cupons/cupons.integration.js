'use strict';

var app = require('../..');
import request from 'supertest';

var newCupons;

describe('Cupons API:', function() {
  describe('GET /api/cupons', function() {
    var cuponss;

    beforeEach(function(done) {
      request(app)
        .get('/api/cupons')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          cuponss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(cuponss).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/cupons', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/cupons')
        .send({
          name: 'New Cupons',
          info: 'This is the brand new cupons!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newCupons = res.body;
          done();
        });
    });

    it('should respond with the newly created cupons', function() {
      expect(newCupons.name).to.equal('New Cupons');
      expect(newCupons.info).to.equal('This is the brand new cupons!!!');
    });
  });

  describe('GET /api/cupons/:id', function() {
    var cupons;

    beforeEach(function(done) {
      request(app)
        .get(`/api/cupons/${newCupons._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          cupons = res.body;
          done();
        });
    });

    afterEach(function() {
      cupons = {};
    });

    it('should respond with the requested cupons', function() {
      expect(cupons.name).to.equal('New Cupons');
      expect(cupons.info).to.equal('This is the brand new cupons!!!');
    });
  });

  describe('PUT /api/cupons/:id', function() {
    var updatedCupons;

    beforeEach(function(done) {
      request(app)
        .put(`/api/cupons/${newCupons._id}`)
        .send({
          name: 'Updated Cupons',
          info: 'This is the updated cupons!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedCupons = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCupons = {};
    });

    it('should respond with the original cupons', function() {
      expect(updatedCupons.name).to.equal('New Cupons');
      expect(updatedCupons.info).to.equal('This is the brand new cupons!!!');
    });

    it('should respond with the updated cupons on a subsequent GET', function(done) {
      request(app)
        .get(`/api/cupons/${newCupons._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let cupons = res.body;

          expect(cupons.name).to.equal('Updated Cupons');
          expect(cupons.info).to.equal('This is the updated cupons!!!');

          done();
        });
    });
  });

  describe('PATCH /api/cupons/:id', function() {
    var patchedCupons;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/cupons/${newCupons._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Cupons' },
          { op: 'replace', path: '/info', value: 'This is the patched cupons!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedCupons = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedCupons = {};
    });

    it('should respond with the patched cupons', function() {
      expect(patchedCupons.name).to.equal('Patched Cupons');
      expect(patchedCupons.info).to.equal('This is the patched cupons!!!');
    });
  });

  describe('DELETE /api/cupons/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/cupons/${newCupons._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when cupons does not exist', function(done) {
      request(app)
        .delete(`/api/cupons/${newCupons._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
