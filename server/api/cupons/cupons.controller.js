/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/cupons              ->  index
 * POST    /api/cupons              ->  create
 * GET     /api/cupons/:id          ->  show
 * PUT     /api/cupons/:id          ->  upsert
 * PATCH   /api/cupons/:id          ->  patch
 * DELETE  /api/cupons/:id          ->  destroy
 * POST    /api/cupons/validate              ->  validateCupons
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Cupons from './cupons.model';
import Price from './price.model';
import Settings from '../settings/settings.model';

var query = Settings.find();
var tok;

function getStripe(callback){
   query.exec((err, api) => {

     for(var a in api){
         if(api[a].name == "Stripe"){
          tok = api[a].secretToken;
          callback(tok);
         };
      }

   });
}


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Cuponss
export function index(req, res) {
  return Cupons.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Cupons from the DB
export function show(req, res) {
  return Cupons.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Cupons in the DB
export function create(req, res) {
  console.log(req.body);
  req.body.f.creation = Date.now();
  return Cupons.create(req.body.f)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));



}

// Upserts the given Cupons in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Cupons.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Cupons in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Cupons.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Cupons from the DB
export function destroy(req, res) {
  return Cupons.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

// Validate cupon from cupons model
export function validateCupons(req, res) {
  return Cupons.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

export function setDefault(){
  return Cupons.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}
