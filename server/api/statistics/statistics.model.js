'use strict';

import mongoose from 'mongoose';

var StatisticsSchema = new mongoose.Schema({
  date: Date,
  freeToPaid: Number,
  visitorToSign: Number,
  payPalUsers: Number,
  cardUsers: Number,
  bitcoinUsers: Number,
  churnRate: Number,
  averageLifetime: Number
});

export default mongoose.model('Statistics', StatisticsSchema);
