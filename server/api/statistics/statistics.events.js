/**
 * Statistics model events
 */

'use strict';

import {EventEmitter} from 'events';
import Statistics from './statistics.model';
var StatisticsEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
StatisticsEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Statistics.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    StatisticsEvents.emit(event + ':' + doc._id, doc);
    StatisticsEvents.emit(event, doc);
  };
}

export default StatisticsEvents;
