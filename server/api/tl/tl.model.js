'use strict';

import mongoose from 'mongoose';

var TlSchema = new mongoose.Schema({
  name: String,
  logo :  String
});

export default mongoose.model('Tl', TlSchema);
