'use strict';

import mongoose from 'mongoose';

var LogoSchema = new mongoose.Schema({
  logo : String
});

export default mongoose.model('Logo', LogoSchema);
