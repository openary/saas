'use strict';

var app = require('../..');
import request from 'supertest';

var newTl;

describe('Tl API:', function() {
  describe('GET /api/tl', function() {
    var tls;

    beforeEach(function(done) {
      request(app)
        .get('/api/tl')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          tls = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(tls).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/tl', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/tl')
        .send({
          name: 'New Tl',
          info: 'This is the brand new tl!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTl = res.body;
          done();
        });
    });

    it('should respond with the newly created tl', function() {
      expect(newTl.name).to.equal('New Tl');
      expect(newTl.info).to.equal('This is the brand new tl!!!');
    });
  });

  describe('GET /api/tl/:id', function() {
    var tl;

    beforeEach(function(done) {
      request(app)
        .get(`/api/tl/${newTl._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          tl = res.body;
          done();
        });
    });

    afterEach(function() {
      tl = {};
    });

    it('should respond with the requested tl', function() {
      expect(tl.name).to.equal('New Tl');
      expect(tl.info).to.equal('This is the brand new tl!!!');
    });
  });

  describe('PUT /api/tl/:id', function() {
    var updatedTl;

    beforeEach(function(done) {
      request(app)
        .put(`/api/tl/${newTl._id}`)
        .send({
          name: 'Updated Tl',
          info: 'This is the updated tl!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTl = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTl = {};
    });

    it('should respond with the original tl', function() {
      expect(updatedTl.name).to.equal('New Tl');
      expect(updatedTl.info).to.equal('This is the brand new tl!!!');
    });

    it('should respond with the updated tl on a subsequent GET', function(done) {
      request(app)
        .get(`/api/tl/${newTl._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let tl = res.body;

          expect(tl.name).to.equal('Updated Tl');
          expect(tl.info).to.equal('This is the updated tl!!!');

          done();
        });
    });
  });

  describe('PATCH /api/tl/:id', function() {
    var patchedTl;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/tl/${newTl._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Tl' },
          { op: 'replace', path: '/info', value: 'This is the patched tl!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTl = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTl = {};
    });

    it('should respond with the patched tl', function() {
      expect(patchedTl.name).to.equal('Patched Tl');
      expect(patchedTl.info).to.equal('This is the patched tl!!!');
    });
  });

  describe('DELETE /api/tl/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/tl/${newTl._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when tl does not exist', function(done) {
      request(app)
        .delete(`/api/tl/${newTl._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
