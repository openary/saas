'use strict';

var express = require('express');
var controller = require('./tl.controller');
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './client/assets/logo')
    },
    filename: function (req, file, cb) {
        cb(null, 'logo.png')
    }
});
var upload = multer({ storage: storage });

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/logo' , upload.single('file'), controller.logo);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);

module.exports = router;
