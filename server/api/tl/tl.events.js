/**
 * Tl model events
 */

'use strict';

import {EventEmitter} from 'events';
import Tl from './tl.model';
var TlEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TlEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Tl.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    TlEvents.emit(event + ':' + doc._id, doc);
    TlEvents.emit(event, doc);
  };
}

export default TlEvents;
