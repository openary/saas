/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/tl              ->  index
 * POST    /api/tl              ->  create
 * GET     /api/tl/:id          ->  show
 * PUT     /api/tl/:id          ->  upsert
 * PATCH   /api/tl/:id          ->  patch
 * DELETE  /api/tl/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Tl from './tl.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Tls
export function index(req, res) {
  return Tl.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Tl from the DB
export function show(req, res) {
  return Tl.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Tl in the DB
export function create(req, res) {

    req.body.logo = 'assets/logo/logo.png';
    return Tl.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

//Adds logo
export function logo(req, res){
  var imgPath = req.file.path;
   imgPath = imgPath.replace(/client\//g, "");
   req.body.img = imgPath;
   console.log(imgPath);

   Tl.find().exec( (err , tl) => {

  if(tl.length != 0){
   return Tl.findById(tl[0]._id).exec()
       .then(t => {
         console.log(t)
         if(t) {
           t.logo = imgPath;

           return t.save()
             .then(() => {
               res.status(204).end();
             })
             .catch(handleError(res));
         } else {
           return res.status(403).end();
         }

       });
     }
   })

}

// Upserts the given Tl in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Tl.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Tl in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

   Tl.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
    req.body.logo = 'assets/logo/logo.png';
  return Tl.create(req.body);
}

// Deletes a Tl from the DB
export function destroy(req, res) {
  return Tl.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
