/**
 * Broadcast updates to client when the model changes
 */

'use strict';

import TlEvents from './tl.events';

// Model events to emit
var events = ['save', 'remove', 'patch', 'put'];

export function register(socket) {
  // Bind model events to socket events
  for(var i = 0, eventsLength = events.length; i < eventsLength; i++) {
    var event = events[i];
    var listener = createListener(`tl:${event}`, socket);

    TlEvents.on(event, listener);
    socket.on('disconnect', removeListener(event, listener));
  }
}


function createListener(event, socket) {
  return function(doc) {
    socket.emit(event, doc);
  };
}

function removeListener(event, listener) {
  return function() {
    TlEvents.removeListener(event, listener);
  };
}
