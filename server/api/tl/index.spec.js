'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var tlCtrlStub = {
  index: 'tlCtrl.index',
  show: 'tlCtrl.show',
  create: 'tlCtrl.create',
  upsert: 'tlCtrl.upsert',
  patch: 'tlCtrl.patch',
  destroy: 'tlCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var tlIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './tl.controller': tlCtrlStub
});

describe('Tl API Router:', function() {
  it('should return an express router instance', function() {
    expect(tlIndex).to.equal(routerStub);
  });

  describe('GET /api/tl', function() {
    it('should route to tl.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'tlCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/tl/:id', function() {
    it('should route to tl.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'tlCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/tl', function() {
    it('should route to tl.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'tlCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/tl/:id', function() {
    it('should route to tl.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'tlCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/tl/:id', function() {
    it('should route to tl.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'tlCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/tl/:id', function() {
    it('should route to tl.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'tlCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
