'use strict';

var app = require('../..');
import request from 'supertest';

var newNotifications;

describe('Notifications API:', function() {
  describe('GET /api/notifications', function() {
    var notificationss;

    beforeEach(function(done) {
      request(app)
        .get('/api/notifications')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          notificationss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(notificationss).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/notifications', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/notifications')
        .send({
          name: 'New Notifications',
          info: 'This is the brand new notifications!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newNotifications = res.body;
          done();
        });
    });

    it('should respond with the newly created notifications', function() {
      expect(newNotifications.name).to.equal('New Notifications');
      expect(newNotifications.info).to.equal('This is the brand new notifications!!!');
    });
  });

  describe('GET /api/notifications/:id', function() {
    var notifications;

    beforeEach(function(done) {
      request(app)
        .get(`/api/notifications/${newNotifications._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          notifications = res.body;
          done();
        });
    });

    afterEach(function() {
      notifications = {};
    });

    it('should respond with the requested notifications', function() {
      expect(notifications.name).to.equal('New Notifications');
      expect(notifications.info).to.equal('This is the brand new notifications!!!');
    });
  });

  describe('PUT /api/notifications/:id', function() {
    var updatedNotifications;

    beforeEach(function(done) {
      request(app)
        .put(`/api/notifications/${newNotifications._id}`)
        .send({
          name: 'Updated Notifications',
          info: 'This is the updated notifications!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedNotifications = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedNotifications = {};
    });

    it('should respond with the original notifications', function() {
      expect(updatedNotifications.name).to.equal('New Notifications');
      expect(updatedNotifications.info).to.equal('This is the brand new notifications!!!');
    });

    it('should respond with the updated notifications on a subsequent GET', function(done) {
      request(app)
        .get(`/api/notifications/${newNotifications._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let notifications = res.body;

          expect(notifications.name).to.equal('Updated Notifications');
          expect(notifications.info).to.equal('This is the updated notifications!!!');

          done();
        });
    });
  });

  describe('PATCH /api/notifications/:id', function() {
    var patchedNotifications;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/notifications/${newNotifications._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Notifications' },
          { op: 'replace', path: '/info', value: 'This is the patched notifications!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedNotifications = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedNotifications = {};
    });

    it('should respond with the patched notifications', function() {
      expect(patchedNotifications.name).to.equal('Patched Notifications');
      expect(patchedNotifications.info).to.equal('This is the patched notifications!!!');
    });
  });

  describe('DELETE /api/notifications/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/notifications/${newNotifications._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when notifications does not exist', function(done) {
      request(app)
        .delete(`/api/notifications/${newNotifications._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
