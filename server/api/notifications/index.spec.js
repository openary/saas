'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var notificationsCtrlStub = {
  index: 'notificationsCtrl.index',
  show: 'notificationsCtrl.show',
  create: 'notificationsCtrl.create',
  upsert: 'notificationsCtrl.upsert',
  patch: 'notificationsCtrl.patch',
  destroy: 'notificationsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var notificationsIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './notifications.controller': notificationsCtrlStub
});

describe('Notifications API Router:', function() {
  it('should return an express router instance', function() {
    expect(notificationsIndex).to.equal(routerStub);
  });

  describe('GET /api/notifications', function() {
    it('should route to notifications.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'notificationsCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/notifications/:id', function() {
    it('should route to notifications.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'notificationsCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/notifications', function() {
    it('should route to notifications.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'notificationsCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/notifications/:id', function() {
    it('should route to notifications.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'notificationsCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/notifications/:id', function() {
    it('should route to notifications.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'notificationsCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/notifications/:id', function() {
    it('should route to notifications.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'notificationsCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
