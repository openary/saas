'use strict';

import mongoose from 'mongoose';

var NotificationsSchema = new mongoose.Schema({
  title: String,
  body: String,
  date: Date,
  hour: Number,
  openings: Number
});

export default mongoose.model('Notifications', NotificationsSchema);
