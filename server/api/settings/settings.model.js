'use strict';

import mongoose from 'mongoose';

var SettingsSchema = new mongoose.Schema({
  publicToken: String,
  secretToken : String,
  name: String
});

export default mongoose.model('Settings', SettingsSchema);
