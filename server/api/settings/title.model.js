'use strict';

import mongoose from 'mongoose';

var TitleSchema = new mongoose.Schema({
  name : String,
  logo : String
});

export default mongoose.model('Title', TitleSchema);
