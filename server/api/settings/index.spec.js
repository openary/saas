'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var settingsCtrlStub = {
  index: 'settingsCtrl.index',
  show: 'settingsCtrl.show',
  create: 'settingsCtrl.create',
  upsert: 'settingsCtrl.upsert',
  patch: 'settingsCtrl.patch',
  destroy: 'settingsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var settingsIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './settings.controller': settingsCtrlStub
});

describe('Settings API Router:', function() {
  it('should return an express router instance', function() {
    expect(settingsIndex).to.equal(routerStub);
  });

  describe('GET /api/settings', function() {
    it('should route to settings.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'settingsCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/settings/:id', function() {
    it('should route to settings.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'settingsCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/settings', function() {
    it('should route to settings.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'settingsCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/settings/:id', function() {
    it('should route to settings.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'settingsCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/settings/:id', function() {
    it('should route to settings.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'settingsCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/settings/:id', function() {
    it('should route to settings.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'settingsCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
