/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/settings              ->  index
 * POST    /api/settings              ->  create
 * GET     /api/settings/:id          ->  show
 * PUT     /api/settings/:id          ->  upsert
 * PATCH   /api/settings/:id          ->  patch
 * DELETE  /api/settings/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Settings from './settings.model';
import fs from 'fs';
import Title from './title.model';

var tok;


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    return res.status(statusCode).json(err);
  };
}

// Gets a list of Settingss
export function index(req, res) {
  return Settings.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Settings from the DB
export function show(req, res) {
  return Settings.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

var query = Settings.find();
var tok;
var stripe;

// Creates a new Settings in the DB
export function create(req, res) {

  query.exec((err, api) => {
    for(var a in api){
        if(api[a].name == "Stripe"){
         tok = api[a].secretToken;
        };
     }

  });

  stripe = require('stripe')(tok);

  return Settings.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));

}

// Upserts the given Settings in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Settings.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Settings in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Settings.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Settings from the DB
export function destroy(req, res) {
  return Settings.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
