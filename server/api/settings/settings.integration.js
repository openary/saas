'use strict';

var app = require('../..');
import request from 'supertest';

var newSettings;

describe('Settings API:', function() {
  describe('GET /api/settings', function() {
    var settingss;

    beforeEach(function(done) {
      request(app)
        .get('/api/settings')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          settingss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(settingss).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/settings', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/settings')
        .send({
          name: 'New Settings',
          info: 'This is the brand new settings!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newSettings = res.body;
          done();
        });
    });

    it('should respond with the newly created settings', function() {
      expect(newSettings.name).to.equal('New Settings');
      expect(newSettings.info).to.equal('This is the brand new settings!!!');
    });
  });

  describe('GET /api/settings/:id', function() {
    var settings;

    beforeEach(function(done) {
      request(app)
        .get(`/api/settings/${newSettings._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          settings = res.body;
          done();
        });
    });

    afterEach(function() {
      settings = {};
    });

    it('should respond with the requested settings', function() {
      expect(settings.name).to.equal('New Settings');
      expect(settings.info).to.equal('This is the brand new settings!!!');
    });
  });

  describe('PUT /api/settings/:id', function() {
    var updatedSettings;

    beforeEach(function(done) {
      request(app)
        .put(`/api/settings/${newSettings._id}`)
        .send({
          name: 'Updated Settings',
          info: 'This is the updated settings!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedSettings = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSettings = {};
    });

    it('should respond with the original settings', function() {
      expect(updatedSettings.name).to.equal('New Settings');
      expect(updatedSettings.info).to.equal('This is the brand new settings!!!');
    });

    it('should respond with the updated settings on a subsequent GET', function(done) {
      request(app)
        .get(`/api/settings/${newSettings._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let settings = res.body;

          expect(settings.name).to.equal('Updated Settings');
          expect(settings.info).to.equal('This is the updated settings!!!');

          done();
        });
    });
  });

  describe('PATCH /api/settings/:id', function() {
    var patchedSettings;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/settings/${newSettings._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Settings' },
          { op: 'replace', path: '/info', value: 'This is the patched settings!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedSettings = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedSettings = {};
    });

    it('should respond with the patched settings', function() {
      expect(patchedSettings.name).to.equal('Patched Settings');
      expect(patchedSettings.info).to.equal('This is the patched settings!!!');
    });
  });

  describe('DELETE /api/settings/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/settings/${newSettings._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when settings does not exist', function(done) {
      request(app)
        .delete(`/api/settings/${newSettings._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
