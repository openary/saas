/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/payments              ->  index
 * POST    /api/payments              ->  create
 * GET     /api/payments/:id          ->  show
 * PUT     /api/payments/:id          ->  upsert
 * PATCH   /api/payments/:id          ->  patch
 * DELETE  /api/payments/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Payments from './payments.model';
import Settings from '../settings/settings.model';
import User from '../user/user.model';
var paypal = require('paypal-rest-sdk');
var tok;
var stripe;
var query = Settings.find();


function getStripe(callback){
 query.exec((err, api) => {

   for(var a in api){
       if(api[a].name == "Stripe"){
        tok = api[a].secretToken;
        callback(tok);
       };
    }

 });
}

var ci = '';
var st = '';

function getPaypal(callback){
 query.exec((err, api) => {

   for(var a in api){
       if(api[a].name == "Paypal"){
         ci = api[a].publicToken;
         st = api[a].secretToken;
        callback(ci, st);
       };
    }

 });
}

/* getPaypal((a , b) => {

  paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': a,
    'client_secret': b,
    'openid_redirect_uri': 'http://localhost:3000/accounts?p'
  });

});
*/

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Paymentss
export function index(req, res) {
  return Payments.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Payments from the DB
export function show(req, res) {
  return Payments.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Payments in the DB
export function create(req, res) {


  let today = ( d => new Date(d.setDate(d.getDate())) )(new Date);
  var dd = ("0" + (today.getDate()).toString().slice(-2));
  var mm = ("0" + (today.getMonth()+1).toString().slice(-2));
  var yyyy = today.getFullYear().toString();

  var todayString = mm + "/" + dd + "/" + yyyy;

  getStripe((t) => {
     stripe = require('stripe')(t);
  })

  var token = req.body.token;
  stripe.customers.create({
  source: token, // obtained with Stripe.js
  plan: "basic_month",
  email: req.body.email
    }, function(err, customer) {
        if(customer){
          User.findOneAndUpdate({email: req.body.email} , {$set: {role: 'paid'} } , function(er, user){
            if(err) throw err;

            Stats.find({} , function(err, s){
                Stats.findOneAndUpdate({date: todayString} , { $inc : { freeToPaid: 1 , cardUsers: 1 } })
                .exec(function(err, stat){
                  console.log(stat);
                });
            });

            res.json(user);
          });
        }
    });
}

export function bitcoin(req, res){

  let today = ( d => new Date(d.setDate(d.getDate())) )(new Date);
  var dd = ("0" + (today.getDate()).toString().slice(-2));
  var mm = ("0" + (today.getMonth()+1).toString().slice(-2));
  var yyyy = today.getFullYear().toString();

  var todayString = mm + "/" + dd + "/" + yyyy;

  getStripe((t) => {
     stripe = require('stripe')(t);
  })

      stripe.sources.create({
      type: "bitcoin",
      amount: 1000,
      currency: "usd",
      owner: {
        email: req.body.email
          }
        }, function(err, source) {
          // asynchronously called
          console.log(source);
          Stats.find({} , function(err, s){
              Stats.findOneAndUpdate({date: todayString} , { $inc : { bitcoinUsers: 1 } })
              .exec(function(err, stat){
                console.log(stat);
              });
          });

        });
}


function getStripeKey(cb){
 query.exec((err, api) => {

   for(var a in api){
       if(api[a].name == "Stripe"){
        tok = api[a].secretToken;
        cb(tok);
       };
    }

 });
}

//get list of charges stripe
export function listStripe(req, res){

  getStripeKey((t) => {
     stripe = require('stripe')(t);

       stripe.balance.listTransactions({
         limit: 30
        }, function(err, transactions) {
         // asynchronously called
        // res.send(transactions);
        console.log(err + "--------------")
        console.log(transactions)
       });

  });//callback getStripe

}


// Upserts the given Payments in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Payments.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Payments in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Payments.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Payments from the DB
export function destroy(req, res) {
  return Payments.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


export function paypal(req, res){

  var isoDate = new Date();
  isoDate.setSeconds(isoDate.getSeconds() + 4);
  isoDate.toISOString().slice(0, 19) + 'Z';

  var billingPlanAttributes = {
      "description": "Create Plan for Regular",
      "merchant_preferences": {
          "auto_bill_amount": "yes",
          "cancel_url": "http://www.cancel.com",
          "initial_fail_amount_action": "continue",
          "max_fail_attempts": "1",
          "return_url": "http://www.success.com",
          "setup_fee": {
              "currency": "USD",
              "value": "25"
          }
      },
      "name": "Testing1-Regular1",
      "payment_definitions": [
          {
              "amount": {
                  "currency": "USD",
                  "value": "100"
              },
              "charge_models": [
                  {
                      "amount": {
                          "currency": "USD",
                          "value": "10.60"
                      },
                      "type": "SHIPPING"
                  },
                  {
                      "amount": {
                          "currency": "USD",
                          "value": "20"
                      },
                      "type": "TAX"
                  }
              ],
              "cycles": "0",
              "frequency": "MONTH",
              "frequency_interval": "1",
              "name": "Regular 1",
              "type": "REGULAR"
          },
          {
              "amount": {
                  "currency": "USD",
                  "value": "20"
              },
              "charge_models": [
                  {
                      "amount": {
                          "currency": "USD",
                          "value": "10.60"
                      },
                      "type": "SHIPPING"
                  },
                  {
                      "amount": {
                          "currency": "USD",
                          "value": "20"
                      },
                      "type": "TAX"
                  }
              ],
              "cycles": "4",
              "frequency": "MONTH",
              "frequency_interval": "1",
              "name": "Trial 1",
              "type": "TRIAL"
          }
      ],
      "type": "INFINITE"
  };

  var billingPlanUpdateAttributes = [
      {
          "op": "replace",
          "path": "/",
          "value": {
              "state": "ACTIVE"
          }
      }
  ];

  var billingAgreementAttributes = {
      "name": "Fast Speed Agreement",
      "description": "Agreement for Fast Speed Plan",
      "start_date": isoDate,
      "plan": {
          "id": "P-0NJ10521L3680291SOAQIVTQ"
      },
      "payer": {
          "payment_method": "paypal"
      },
      "shipping_address": {
          "line1": "StayBr111idge Suites",
          "line2": "Cro12ok Street",
          "city": "San Jose",
          "state": "CA",
          "postal_code": "95112",
          "country_code": "US"
      }
  };

  // Create the billing plan
  paypal.billingPlan.create(billingPlanAttributes, function (error, billingPlan) {
      if (error) {
          console.log(error);
          throw error;
      } else {
          console.log("Create Billing Plan Response");
          console.log(billingPlan);

          // Activate the plan by changing status to Active
          paypal.billingPlan.update(billingPlan.id, billingPlanUpdateAttributes, function (error, response) {
              if (error) {
                  console.log(error);
                  throw error;
              } else {
                  console.log("Billing Plan state changed to " + billingPlan.state);
                  billingAgreementAttributes.plan.id = billingPlan.id;

                  // Use activated billing plan to create agreement
                  paypal.billingAgreement.create(billingAgreementAttributes, function (error, billingAgreement) {
                      if (error) {
                          console.log(error);
                          throw error;
                      } else {
                          console.log("Create Billing Agreement Response");
                          //console.log(billingAgreement);
                          for (var index = 0; index < billingAgreement.links.length; index++) {
                              if (billingAgreement.links[index].rel === 'approval_url') {
                                  var approval_url = billingAgreement.links[index].href;
                                  console.log("For approving subscription via Paypal, first redirect user to");
                                  console.log(approval_url);

                                  console.log("Payment token is");
                                  console.log(url.parse(approval_url, true).query.token);
                                  // See billing_agreements/execute.js to see example for executing agreement
                                  // after you have payment token
                              }
                          }
                      }
                  });
              }
          });
      }
  });

}
