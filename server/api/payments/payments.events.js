/**
 * Payments model events
 */

'use strict';

import {EventEmitter} from 'events';
import Payments from './payments.model';
var PaymentsEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PaymentsEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Payments.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    PaymentsEvents.emit(event + ':' + doc._id, doc);
    PaymentsEvents.emit(event, doc);
  };
}

export default PaymentsEvents;
