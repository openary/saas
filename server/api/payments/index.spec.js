'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var paymentsCtrlStub = {
  index: 'paymentsCtrl.index',
  show: 'paymentsCtrl.show',
  create: 'paymentsCtrl.create',
  upsert: 'paymentsCtrl.upsert',
  patch: 'paymentsCtrl.patch',
  destroy: 'paymentsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var paymentsIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './payments.controller': paymentsCtrlStub
});

describe('Payments API Router:', function() {
  it('should return an express router instance', function() {
    expect(paymentsIndex).to.equal(routerStub);
  });

  describe('GET /api/payments', function() {
    it('should route to payments.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'paymentsCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/payments/:id', function() {
    it('should route to payments.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'paymentsCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/payments', function() {
    it('should route to payments.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'paymentsCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/payments/:id', function() {
    it('should route to payments.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'paymentsCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/payments/:id', function() {
    it('should route to payments.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'paymentsCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/payments/:id', function() {
    it('should route to payments.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'paymentsCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
