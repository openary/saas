'use strict';

import mongoose from 'mongoose';

var PaymentsSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model('Payments', PaymentsSchema);
