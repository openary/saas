'use strict';

var app = require('../..');
import request from 'supertest';

var newPayments;

describe('Payments API:', function() {
  describe('GET /api/payments', function() {
    var paymentss;

    beforeEach(function(done) {
      request(app)
        .get('/api/payments')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          paymentss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(paymentss).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/payments', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/payments')
        .send({
          name: 'New Payments',
          info: 'This is the brand new payments!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newPayments = res.body;
          done();
        });
    });

    it('should respond with the newly created payments', function() {
      expect(newPayments.name).to.equal('New Payments');
      expect(newPayments.info).to.equal('This is the brand new payments!!!');
    });
  });

  describe('GET /api/payments/:id', function() {
    var payments;

    beforeEach(function(done) {
      request(app)
        .get(`/api/payments/${newPayments._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          payments = res.body;
          done();
        });
    });

    afterEach(function() {
      payments = {};
    });

    it('should respond with the requested payments', function() {
      expect(payments.name).to.equal('New Payments');
      expect(payments.info).to.equal('This is the brand new payments!!!');
    });
  });

  describe('PUT /api/payments/:id', function() {
    var updatedPayments;

    beforeEach(function(done) {
      request(app)
        .put(`/api/payments/${newPayments._id}`)
        .send({
          name: 'Updated Payments',
          info: 'This is the updated payments!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedPayments = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedPayments = {};
    });

    it('should respond with the original payments', function() {
      expect(updatedPayments.name).to.equal('New Payments');
      expect(updatedPayments.info).to.equal('This is the brand new payments!!!');
    });

    it('should respond with the updated payments on a subsequent GET', function(done) {
      request(app)
        .get(`/api/payments/${newPayments._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let payments = res.body;

          expect(payments.name).to.equal('Updated Payments');
          expect(payments.info).to.equal('This is the updated payments!!!');

          done();
        });
    });
  });

  describe('PATCH /api/payments/:id', function() {
    var patchedPayments;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/payments/${newPayments._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Payments' },
          { op: 'replace', path: '/info', value: 'This is the patched payments!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedPayments = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedPayments = {};
    });

    it('should respond with the patched payments', function() {
      expect(patchedPayments.name).to.equal('Patched Payments');
      expect(patchedPayments.info).to.equal('This is the patched payments!!!');
    });
  });

  describe('DELETE /api/payments/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/payments/${newPayments._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when payments does not exist', function(done) {
      request(app)
        .delete(`/api/payments/${newPayments._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
