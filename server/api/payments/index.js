'use strict';

var express = require('express');
var controller = require('./payments.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/listStripe' , controller.listStripe);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);
router.post('/bitcoin', controller.bitcoin);
router.post('/paypal', controller.paypal);

module.exports = router;
