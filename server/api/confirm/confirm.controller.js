/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/confirms              ->  index
 * POST    /api/confirms              ->  create
 * GET     /api/confirms/:id          ->  show
 * PUT     /api/confirms/:id          ->  upsert
 * PATCH   /api/confirms/:id          ->  patch
 * DELETE  /api/confirms/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Confirm from './confirm.model';
import User from '../user/user.model';
import Random from '../user/random.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Confirms
export function index(req, res) {
  return Confirm.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Confirm from the DB
export function show(req, res) {
  return Confirm.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Confirm in the DB
export function create(req, res) {
  return Confirm.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Confirm in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Confirm.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Confirm in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Confirm.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Confirm from the DB
export function destroy(req, res) {
  return Confirm.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}



export function confirm(req, res){

  var email = req.params.email;
  Random.findOne({email: email} , function(err, randToken){

  if(err) return handleError(err);

      User.findOne({email: email}, function(e, user){

        if(e) return handleError(e);

        try {
          if(!user || (randToken == null) || (randToken == undefined) )
            console.log("Wrong email or token");
          else if(randToken.secretToken == req.params.id){
            User.findOneAndUpdate({email: email}, {$set: {confirmedEmail: true , role: 'free'} }, function(err, u){
              res.redirect('/billinginfo');
            });
          }
          else
            console.log('Something went wrong');

        }
        catch(err){
          console.log(err);
        }

      });




  });


}
