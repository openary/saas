'use strict';

import mongoose from 'mongoose';

var ConfirmSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model('Confirm', ConfirmSchema);
