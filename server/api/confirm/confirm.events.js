/**
 * Confirm model events
 */

'use strict';

import {EventEmitter} from 'events';
import Confirm from './confirm.model';
var ConfirmEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ConfirmEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Confirm.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    ConfirmEvents.emit(event + ':' + doc._id, doc);
    ConfirmEvents.emit(event, doc);
  };
}

export default ConfirmEvents;
