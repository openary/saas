'use strict';

var app = require('../..');
import request from 'supertest';

var newConfirm;

describe('Confirm API:', function() {
  describe('GET /api/confirms', function() {
    var confirms;

    beforeEach(function(done) {
      request(app)
        .get('/api/confirms')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          confirms = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(confirms).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/confirms', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/confirms')
        .send({
          name: 'New Confirm',
          info: 'This is the brand new confirm!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newConfirm = res.body;
          done();
        });
    });

    it('should respond with the newly created confirm', function() {
      expect(newConfirm.name).to.equal('New Confirm');
      expect(newConfirm.info).to.equal('This is the brand new confirm!!!');
    });
  });

  describe('GET /api/confirms/:id', function() {
    var confirm;

    beforeEach(function(done) {
      request(app)
        .get(`/api/confirms/${newConfirm._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          confirm = res.body;
          done();
        });
    });

    afterEach(function() {
      confirm = {};
    });

    it('should respond with the requested confirm', function() {
      expect(confirm.name).to.equal('New Confirm');
      expect(confirm.info).to.equal('This is the brand new confirm!!!');
    });
  });

  describe('PUT /api/confirms/:id', function() {
    var updatedConfirm;

    beforeEach(function(done) {
      request(app)
        .put(`/api/confirms/${newConfirm._id}`)
        .send({
          name: 'Updated Confirm',
          info: 'This is the updated confirm!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedConfirm = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedConfirm = {};
    });

    it('should respond with the original confirm', function() {
      expect(updatedConfirm.name).to.equal('New Confirm');
      expect(updatedConfirm.info).to.equal('This is the brand new confirm!!!');
    });

    it('should respond with the updated confirm on a subsequent GET', function(done) {
      request(app)
        .get(`/api/confirms/${newConfirm._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let confirm = res.body;

          expect(confirm.name).to.equal('Updated Confirm');
          expect(confirm.info).to.equal('This is the updated confirm!!!');

          done();
        });
    });
  });

  describe('PATCH /api/confirms/:id', function() {
    var patchedConfirm;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/confirms/${newConfirm._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Confirm' },
          { op: 'replace', path: '/info', value: 'This is the patched confirm!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedConfirm = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedConfirm = {};
    });

    it('should respond with the patched confirm', function() {
      expect(patchedConfirm.name).to.equal('Patched Confirm');
      expect(patchedConfirm.info).to.equal('This is the patched confirm!!!');
    });
  });

  describe('DELETE /api/confirms/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/confirms/${newConfirm._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when confirm does not exist', function(done) {
      request(app)
        .delete(`/api/confirms/${newConfirm._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
