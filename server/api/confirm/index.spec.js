'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var confirmCtrlStub = {
  index: 'confirmCtrl.index',
  show: 'confirmCtrl.show',
  create: 'confirmCtrl.create',
  upsert: 'confirmCtrl.upsert',
  patch: 'confirmCtrl.patch',
  destroy: 'confirmCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var confirmIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './confirm.controller': confirmCtrlStub
});

describe('Confirm API Router:', function() {
  it('should return an express router instance', function() {
    expect(confirmIndex).to.equal(routerStub);
  });

  describe('GET /api/confirms', function() {
    it('should route to confirm.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'confirmCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/confirms/:id', function() {
    it('should route to confirm.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'confirmCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/confirms', function() {
    it('should route to confirm.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'confirmCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/confirms/:id', function() {
    it('should route to confirm.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'confirmCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/confirms/:id', function() {
    it('should route to confirm.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'confirmCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/confirms/:id', function() {
    it('should route to confirm.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'confirmCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
