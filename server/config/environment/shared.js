'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['nad', 'free', 'beta', 'paid', 'admin']
};
