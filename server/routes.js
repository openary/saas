/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';

export default function(app) {
  // Insert routes below
  app.use('/api/tl', require('./api/tl'));
  app.use('/api/status', require('./api/status'));
  app.use('/api/notifications', require('./api/notifications'));
  app.use('/api/plan', require('./api/plan'));
  app.use('/api/networks', require('./api/networks'));
  app.use('/api/confirms', require('./api/confirm'));
  app.use('/api/payments', require('./api/payments'));
  app.use('/api/settings', require('./api/settings'));
  app.use('/api/cupons', require('./api/cupons'));
  app.use('/api/statistics', require('./api/statistics'));
  app.use('/api/blog', require('./api/blog'));
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth').default);

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    });
}
