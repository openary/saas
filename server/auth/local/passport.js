import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';

function localAuthenticate(User, email, password, done) {
  var date = new Date();
  var d = date.getTime();

  User.findOne({
    email: email.toLowerCase()
  }).exec()
    .then(user => {
      if(!user) {
        return done(null, false, {
          message: 'This email is not registered.'
        });
      }
      user.authenticate(password, function(authError, authenticated) {
        if(authError) {
          return done(authError);
        }
        if(!authenticated) {
          return done(null, false, { message: 'This password is not correct.' });
        } else {

        if(user.expireIn){
          var expiration = (user.expireIn).getTime();

          if(d > expiration){

          User.findById(user._id).exec()
            .then(userEx => {
              if(userEx) {
                userEx.expired = true;
                userEx.freeCuponUsed = true;
                userEx.role = 'nad';
                  userEx.save()
                  .then(() => {
                  //  res.status(204).end();
                });
                  //.catch(validationError(res));
              } else {
              // res.status(403).end();
              }
            });

          }
        }

          return done(null, user);
        }
      });
    })
    .catch(err => done(err));
}

import UserS from '../../api/user/user.model';

setInterval(function(){
  var date = new Date();
  var d = date.getTime();

  UserS.find().exec( (err, user) => {
  for(var i = 0; i < user.length; i++ ){


    if(user[i].expireIn){
      var expiration = (user[i].expireIn).getTime();

          if(d > expiration){
            console.log("Aqui indica que ya expiro");

            if(user[i].expired){

            }else{
            UserS.findById(user[i]._id).exec()
              .then(userEx => {
                if(userEx) {
                  userEx.expired = true;
                  userEx.freeCuponUsed = true;
                  userEx.role = 'nad';
                    userEx.save()
                    .then(() => {
                      //res.status(204).end();
                    })
                  //  .catch(validationError(res));
                } else {
                // res.status(403).end();
                }
              });
            }


          }
        }
    }
    console.log(user);
  })
}, 86400000);

export function setup(User/*, config*/) {
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password' // this is the virtual field on the model
  }, function(email, password, done) {
    return localAuthenticate(User, email, password, done);
  }));
}
